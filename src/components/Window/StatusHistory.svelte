<script lang="ts">
	import { makeDiff, cleanupSemantic } from '@sanity/diff-match-patch';

	import Container from './Window.svelte';
	import HorizontalBarSpinner from '../Spinner/HorizontalBarSpinner.svelte';
	import Status from '../Status/Container.svelte';
	import InputCheckbox from '../Global/Inputs/InputCheckbox.svelte';

	import { stGlobalWindow, type GlobalWindowStatusHistory } from '../../stores/Window';
	import { rdOptionsAppearanceBorderStatus } from '../../stores/Options';

	import { statusParser } from '../../lib/ContentParser/ContentParser';
	import { trimFirstMentions } from '../../lib/ComponentFunctions/trimFirstMentions';
	import { getStatusID } from '../../lib/ContentParser/anchorsParser';
	import { RequestsManager } from '../../lib/Managers/RequestsManager';
	import { flattenHTML } from '../../lib/ContentParser/flattenHTML';

	import type { ParsedStatus, ParsedDiff } from '../../types/contentParsed';
	import { Entity } from '../../types/mastodonEntities';

	const global_window = $stGlobalWindow as GlobalWindowStatusHistory;

	const account = global_window.account;
	const status = global_window.status;
	const visibility_private: boolean =
		status.visibility === Entity.Visibility.Private ||
		status.visibility === Entity.Visibility.Direct;
	const status_origin = new URL(status.url).origin;
	const account_origin = new URL(account.entity.url).origin;
	const is_same_domain = status_origin === account_origin;

	function mergeEdit(edit: Entity.StatusEdit): ParsedStatus {
		const { content, created_at, emojis, media_attachments, poll, sensitive, spoiler_text } =
			edit;

		const {
			account: status_account,
			application,
			bookmarked,
			card,
			emoji_reactions,
			favourited,
			favourites_count,
			id,
			in_reply_to_account_id,
			in_reply_to_id,
			language,
			mentions,
			muted,
			pinned,
			plain_content,
			quote,
			reblog,
			reblogged,
			reblogs_count,
			replies_count,
			tags,
			uri,
			url,
			visibility,
			tootdeck,
		} = status;
		const filtered = {
			application,
			bookmarked,
			card,
			emoji_reactions,
			favourited,
			favourites_count,
			id,
			in_reply_to_account_id,
			in_reply_to_id,
			language,
			mentions,
			muted,
			pinned,
			plain_content,
			quote,
			reblog,
			reblogged,
			reblogs_count,
			replies_count,
			tags,
			uri,
			url,
			visibility,
			tootdeck,
		};

		const clone = structuredClone(filtered);

		/**
		 * Workaround
		 * Sometimes accounts aren't tagged
		 */
		let text = content;
		if (
			mentions.length &&
			!content.split(' ').find((x) => x.includes(`class="u-url mention"`))
		) {
			text = trimFirstMentions(mentions, content.replace(/<p[^>]*>|<\/p>|/g, ''));
		}

		const merged = {
			account: status_account,
			...clone,
			content: text,
			created_at,
			emojis: emojis.length ? emojis : status.emojis,
			media_attachments,
			poll,
			sensitive,
			spoiler_text,
			edited_at: null,
			animateIn: false,
			animateOut: false,
		};

		// @ts-ignore
		return statusParser(account, merged);
	}

	let edit_count: number = 0;

	async function localFetch() {
		return account.mirrorAPI.Statuses.getHistory(status.id);
	}

	async function remoteFetch(
		origin: string,
		id: string
	): Promise<Array<Entity.StatusEdit> | null> {
		const request_url = `${origin}/api/v1/statuses/${id}/history`;

		const cache = RequestsManager.get<Array<Entity.StatusEdit>>(request_url);
		if (cache) {
			RequestsManager.increaseExpiry(request_url, 30000);
			return cache;
		}

		return RequestsManager.await<Array<Entity.StatusEdit> | null>(
			request_url,
			() =>
				fetch(request_url)
					.then(async (r) => {
						if (r.status !== 200) {
							throw r;
						}

						const json = await r.json();

						if (!Array.isArray(json)) {
							throw r;
						}

						return json;
					})
					.catch((e) => {
						console.error('Unable to remote fetch status history');
						console.error(e);
						return null;
					})
					.then((r) => (r ? r : localFetch())),
			30000
		);
	}

	// Gets custom emojis from both statuses and removes duplicates
	function getEmojis(statuses: Array<ParsedStatus>) {
		const all = statuses.flatMap(
			(status) =>
				status.emojis.map((emoji) => [emoji.shortcode, emoji]) as Array<
					[string, Entity.Emoji]
				>
		);
		if (!all.length) {
			return status.emojis;
		}

		return Array.from(new Map<string, Entity.Emoji>(all).values());
	}

	function generateDiff(
		value: ParsedStatus,
		index: number,
		array: Array<ParsedStatus>
	): [ParsedStatus, Array<ParsedDiff> | null] {
		if (!array[index - 1]) {
			return [value, null];
		}

		const older_status = array[index];
		const newer_status = array[index - 1];

		const emojis = getEmojis(array);

		const diff =
			// Generate status diff
			cleanupSemantic(makeDiff(newer_status.plain_content, older_status.plain_content))
				// Insert custom emojis
				.map((v) => [
					v[0],
					flattenHTML(v[1], emojis, { trim: false }),
				]) as Array<ParsedDiff>;

		// Generate alt diff
		older_status.media_attachments.forEach((older_media, i) => {
			const newer_media = newer_status.media_attachments[i];
			if (newer_media) {
				older_media.diff = cleanupSemantic(
					makeDiff(older_media.description ?? '', newer_media.description ?? '')
				);
			}
		});

		return [value, diff];
	}

	async function getHistory() {
		const status_origin = new URL(status.url).origin;

		const id = getStatusID(status.url);
		if (id && !visibility_private && !is_same_domain) {
			return remoteFetch(status_origin, id);
		}

		return localFetch();
	}

	const promise = getHistory().then((edits) => {
		if (edits?.length) {
			edit_count = edits.length - 1;
		}

		return (
			edits
				// Parse statuses
				?.map(mergeEdit)
				// Generate Diffs
				.map(generateDiff)
				// Index result
				.map(([edit, diff], id) => ({ id, edit, diff }))
		);
	});

	function title() {
		const username = status.account.username;
		const time = `time${edit_count !== 1 ? 's' : ''}`;
		const count = edit_count > 0 ? ` ${edit_count} ${time}` : '';
		return `${username} edited ${count}`;
	}

	let show: boolean = false;
</script>

<Container title={title()}>
	<div class="flex-column">
		{#if visibility_private && !is_same_domain}
			<div class="hint">
				Status isn't public and remote fetching is unavailable, history can be incomplete.
			</div>
		{/if}
		<div class="status-history" class:remote={!visibility_private && !is_same_domain}>
			{#await promise}
				<div style:margin-top={'10px'}>
					<HorizontalBarSpinner></HorizontalBarSpinner>
				</div>
			{:then statuses}
				{#if statuses && statuses.length > 1}
					<div style="margin-bottom: -15px;">
						<InputCheckbox
							text="Highlight difference with previous status"
							checked={show}
							on:click={() => (show = !show)}
						></InputCheckbox>
					</div>
				{/if}
				{#if statuses}
					{#each statuses as { id, edit, diff } (id)}
						<Status
							{account}
							raw_status={edit}
							notification={false}
							border={$rdOptionsAppearanceBorderStatus}
							history={true}
							diff={show ? diff : null}
						></Status>
					{/each}
				{/if}
			{/await}
		</div>
	</div>
</Container>

<style lang="scss">
	.flex-column {
		display: flex;
		flex-direction: column;

		.hint {
			padding: 10px 20px;
			font-size: calc(var(--font-size) - 1px);
			text-align: center;
			opacity: 0.7;
		}

		.status-history {
			width: calc(var(--column-width) * 1.5);
			max-height: 90vh;
			display: flex;
			flex-direction: column;
			gap: 25px;
			padding: 5px 10px 25px;
			overflow-x: hidden;
			overflow-y: auto;
			background-color: rgb(var(--column));
			color: rgb(var(--text));

			&.remote {
				padding: 0px 25px 25px;
			}
		}
	}
</style>
