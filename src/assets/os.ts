export const OperatingSystemsIMG = {
	Android: 'os/android.png',
	Apple: 'os/apple.png',
	Linux: 'os/linux.png',
	Windows: 'os/windows.png',
};
