export const browsersIMG = {
	Browser: 'browsers/browser.png',
	Chrome: 'browsers/chrome.png',
	Chromium: 'browsers/chromium.png',
	Edge: 'browsers/edge.png',
	Firefox: 'browsers/firefox.png',
	Opera: 'browsers/opera.png',
	Safari: 'browsers/safari.png',
};
