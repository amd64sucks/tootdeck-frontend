export interface Language {
	short: string;
	full: string;
	english: string;
}

export const languages: Array<Language> = [
	{
		short: 'aa',
		full: 'Afaraf',
		english: '(Afar)',
	},
	{
		short: 'ab',
		full: 'аҧсуа бызшәа',
		english: '(Abkhaz)',
	},
	{
		short: 'ae',
		full: 'avesta',
		english: '(Avestan)',
	},
	{
		short: 'af',
		full: 'Afrikaans',
		english: '(Afrikaans)',
	},
	{
		short: 'ak',
		full: 'Akan',
		english: '(Akan)',
	},
	{
		short: 'am',
		full: 'አማርኛ',
		english: '(Amharic)',
	},
	{
		short: 'an',
		full: 'aragonés',
		english: '(Aragonese)',
	},
	{
		short: 'ar',
		full: 'اللغة العربية',
		english: '(Arabic)',
	},
	{
		short: 'as',
		full: 'অসমীয়া',
		english: '(Assamese)',
	},
	{
		short: 'av',
		full: 'авар мацӀ',
		english: '(Avaric)',
	},
	{
		short: 'ay',
		full: 'aymar aru',
		english: '(Aymara)',
	},
	{
		short: 'az',
		full: 'azərbaycan dili',
		english: '(Azerbaijani)',
	},
	{
		short: 'ba',
		full: 'башҡорт теле',
		english: '(Bashkir)',
	},
	{
		short: 'be',
		full: 'беларуская мова',
		english: '(Belarusian)',
	},
	{
		short: 'bg',
		full: 'български език',
		english: '(Bulgarian)',
	},
	{
		short: 'bh',
		full: 'भोजपुरी',
		english: '(Bihari)',
	},
	{
		short: 'bi',
		full: 'Bislama',
		english: '(Bislama)',
	},
	{
		short: 'bm',
		full: 'bamanankan',
		english: '(Bambara)',
	},
	{
		short: 'bn',
		full: 'বাংলা',
		english: '(Bengali)',
	},
	{
		short: 'bo',
		full: 'བོད་ཡིག',
		english: '(Tibetan)',
	},
	{
		short: 'br',
		full: 'brezhoneg',
		english: '(Breton)',
	},
	{
		short: 'bs',
		full: 'bosanski jezik',
		english: '(Bosnian)',
	},
	{
		short: 'ca',
		full: 'Català',
		english: '(Catalan)',
	},
	{
		short: 'ce',
		full: 'нохчийн мотт',
		english: '(Chechen)',
	},
	{
		short: 'ch',
		full: 'Chamoru',
		english: '(Chamorro)',
	},
	{
		short: 'co',
		full: 'corsu',
		english: '(Corsican)',
	},
	{
		short: 'cr',
		full: 'ᓀᐦᐃᔭᐍᐏᐣ',
		english: '(Cree)',
	},
	{
		short: 'cs',
		full: 'čeština',
		english: '(Czech)',
	},
	{
		short: 'cu',
		full: 'ѩзыкъ словѣньскъ',
		english: '(Old Church Slavonic)',
	},
	{
		short: 'cv',
		full: 'чӑваш чӗлхи',
		english: '(Chuvash)',
	},
	{
		short: 'cy',
		full: 'Cymraeg',
		english: '(Welsh)',
	},
	{
		short: 'da',
		full: 'dansk',
		english: '(Danish)',
	},
	{
		short: 'de',
		full: 'Deutsch',
		english: '(German)',
	},
	{
		short: 'dv',
		full: 'Dhivehi',
		english: '(Divehi)',
	},
	{
		short: 'dz',
		full: 'རྫོང་ཁ',
		english: '(Dzongkha)',
	},
	{
		short: 'ee',
		full: 'Eʋegbe',
		english: '(Ewe)',
	},
	{
		short: 'el',
		full: 'Ελληνικά',
		english: '(Greek)',
	},
	{
		short: 'en',
		full: 'English',
		english: '(English)',
	},
	{
		short: 'eo',
		full: 'Esperanto',
		english: '(Esperanto)',
	},
	{
		short: 'es',
		full: 'Español',
		english: '(Spanish)',
	},
	{
		short: 'et',
		full: 'eesti',
		english: '(Estonian)',
	},
	{
		short: 'eu',
		full: 'euskara',
		english: '(Basque)',
	},
	{
		short: 'fa',
		full: 'فارسی',
		english: '(Persian)',
	},
	{
		short: 'ff',
		full: 'Fulfulde',
		english: '(Fula)',
	},
	{
		short: 'fi',
		full: 'suomi',
		english: '(Finnish)',
	},
	{
		short: 'fj',
		full: 'Vakaviti',
		english: '(Fijian)',
	},
	{
		short: 'fr',
		full: 'Français',
		english: '(French)',
	},
	{
		short: 'fo',
		full: 'føroyskt',
		english: '(Faroese)',
	},
	{
		short: 'fy',
		full: 'Frysk',
		english: '(Western Frisian)',
	},
	{
		short: 'ga',
		full: 'Gaeilge',
		english: '(Irish)',
	},
	{
		short: 'gd',
		full: 'Gàidhlig',
		english: '(Scottish Gaelic)',
	},
	{
		short: 'gl',
		full: 'galego',
		english: '(Galician)',
	},
	{
		short: 'gu',
		full: 'ગુજરાતી',
		english: '(Gujarati)',
	},
	{
		short: 'gv',
		full: 'Gaelg',
		english: '(Manx)',
	},
	{
		short: 'ha',
		full: 'هَوُسَ',
		english: '(Hausa)',
	},
	{
		short: 'he',
		full: 'עברית',
		english: '(Hebrew)',
	},
	{
		short: 'hi',
		full: 'हिन्दी',
		english: '(Hindi)',
	},
	{
		short: 'ho',
		full: 'Hiri Motu',
		english: '(Hiri Motu)',
	},
	{
		short: 'hr',
		full: 'Hrvatski',
		english: '(Croatian)',
	},
	{
		short: 'ht',
		full: 'Kreyòl ayisyen',
		english: '(Haitian)',
	},
	{
		short: 'hu',
		full: 'magyar',
		english: '(Hungarian)',
	},
	{
		short: 'hy',
		full: 'Հայերեն',
		english: '(Armenian)',
	},
	{
		short: 'hz',
		full: 'Otjiherero',
		english: '(Herero)',
	},
	{
		short: 'ia',
		full: 'Interlingua',
		english: '(Interlingua)',
	},
	{
		short: 'id',
		full: 'Bahasa Indonesia',
		english: '(Indonesian)',
	},
	{
		short: 'ie',
		full: 'Interlingue',
		english: '(Interlingue)',
	},
	{
		short: 'ig',
		full: 'Asụsụ Igbo',
		english: '(Igbo)',
	},
	{
		short: 'ii',
		full: 'ꆈꌠ꒿ Nuosuhxop',
		english: '(Nuosu)',
	},
	{
		short: 'ik',
		full: 'Iñupiaq',
		english: '(Inupiaq)',
	},
	{
		short: 'io',
		full: 'Ido',
		english: '(Ido)',
	},
	{
		short: 'is',
		full: 'Íslenska',
		english: '(Icelandic)',
	},
	{
		short: 'it',
		full: 'Italiano',
		english: '(Italian)',
	},
	{
		short: 'iu',
		full: 'ᐃᓄᒃᑎᑐᑦ',
		english: '(Inuktitut)',
	},
	{
		short: 'ja',
		full: '日本語',
		english: '(Japanese)',
	},
	{
		short: 'jv',
		full: 'basa Jawa',
		english: '(Javanese)',
	},
	{
		short: 'ka',
		full: 'ქართული',
		english: '(Georgian)',
	},
	{
		short: 'kg',
		full: 'Kikongo',
		english: '(Kongo)',
	},
	{
		short: 'ki',
		full: 'Gĩkũyũ',
		english: '(Kikuyu)',
	},
	{
		short: 'kj',
		full: 'Kuanyama',
		english: '(Kwanyama)',
	},
	{
		short: 'kk',
		full: 'қазақ тілі',
		english: '(Kazakh)',
	},
	{
		short: 'kl',
		full: 'kalaallisut',
		english: '(Kalaallisut)',
	},
	{
		short: 'km',
		full: 'ខេមរភាសា',
		english: '(Khmer)',
	},
	{
		short: 'kn',
		full: 'ಕನ್ನಡ',
		english: '(Kannada)',
	},
	{
		short: 'ko',
		full: '한국어',
		english: '(Korean)',
	},
	{
		short: 'kr',
		full: 'Kanuri',
		english: '(Kanuri)',
	},
	{
		short: 'ks',
		full: 'कश्मीरी',
		english: '(Kashmiri)',
	},
	{
		short: 'ku',
		full: 'Kurmancî',
		english: '(Kurmanji (Kurdish))',
	},
	{
		short: 'kv',
		full: 'коми кыв',
		english: '(Komi)',
	},
	{
		short: 'kw',
		full: 'Kernewek',
		english: '(Cornish)',
	},
	{
		short: 'ky',
		full: 'Кыргызча',
		english: '(Kyrgyz)',
	},
	{
		short: 'la',
		full: 'latine',
		english: '(Latin)',
	},
	{
		short: 'lb',
		full: 'Lëtzebuergesch',
		english: '(Luxembourgish)',
	},
	{
		short: 'lg',
		full: 'Luganda',
		english: '(Ganda)',
	},
	{
		short: 'li',
		full: 'Limburgs',
		english: '(Limburgish)',
	},
	{
		short: 'ln',
		full: 'Lingála',
		english: '(Lingala)',
	},
	{
		short: 'lo',
		full: 'ລາວ',
		english: '(Lao)',
	},
	{
		short: 'lt',
		full: 'lietuvių kalba',
		english: '(Lithuanian)',
	},
	{
		short: 'lu',
		full: 'Tshiluba',
		english: '(Luba-Katanga)',
	},
	{
		short: 'lv',
		full: 'latviešu valoda',
		english: '(Latvian)',
	},
	{
		short: 'mg',
		full: 'fiteny malagasy',
		english: '(Malagasy)',
	},
	{
		short: 'mh',
		full: 'Kajin M̧ajeļ',
		english: '(Marshallese)',
	},
	{
		short: 'mi',
		full: 'te reo Māori',
		english: '(Māori)',
	},
	{
		short: 'mk',
		full: 'македонски јазик',
		english: '(Macedonian)',
	},
	{
		short: 'ml',
		full: 'മലയാളം',
		english: '(Malayalam)',
	},
	{
		short: 'mn',
		full: 'Монгол хэл',
		english: '(Mongolian)',
	},
	{
		short: 'mr',
		full: 'मराठी',
		english: '(Marathi)',
	},
	{
		short: 'ms',
		full: 'Bahasa Melayu',
		english: '(Malay)',
	},
	{
		short: 'mt',
		full: 'Malti',
		english: '(Maltese)',
	},
	{
		short: 'my',
		full: 'ဗမာစာ',
		english: '(Burmese)',
	},
	{
		short: 'na',
		full: 'Ekakairũ Naoero',
		english: '(Nauru)',
	},
	{
		short: 'nb',
		full: 'Norsk bokmål',
		english: '(Norwegian Bokmål)',
	},
	{
		short: 'nd',
		full: 'isiNdebele',
		english: '(Northern Ndebele)',
	},
	{
		short: 'ne',
		full: 'नेपाली',
		english: '(Nepali)',
	},
	{
		short: 'ng',
		full: 'Owambo',
		english: '(Ndonga)',
	},
	{
		short: 'nl',
		full: 'Nederlands',
		english: '(Dutch)',
	},
	{
		short: 'nn',
		full: 'Norsk Nynorsk',
		english: '(Norwegian Nynorsk)',
	},
	{
		short: 'no',
		full: 'Norsk',
		english: '(Norwegian)',
	},
	{
		short: 'nr',
		full: 'isiNdebele',
		english: '(Southern Ndebele)',
	},
	{
		short: 'nv',
		full: 'Diné bizaad',
		english: '(Navajo)',
	},
	{
		short: 'ny',
		full: 'chiCheŵa',
		english: '(Chichewa)',
	},
	{
		short: 'oc',
		full: 'occitan',
		english: '(Occitan)',
	},
	{
		short: 'oj',
		full: 'ᐊᓂᔑᓈᐯᒧᐎᓐ',
		english: '(Ojibwe)',
	},
	{
		short: 'om',
		full: 'Afaan Oromoo',
		english: '(Oromo)',
	},
	{
		short: 'or',
		full: 'ଓଡ଼ିଆ',
		english: '(Oriya)',
	},
	{
		short: 'os',
		full: 'ирон æвзаг',
		english: '(Ossetian)',
	},
	{
		short: 'pa',
		full: 'ਪੰਜਾਬੀ',
		english: '(Panjabi)',
	},
	{
		short: 'pi',
		full: 'पाऴि',
		english: '(Pāli)',
	},
	{
		short: 'pl',
		full: 'Polski',
		english: '(Polish)',
	},
	{
		short: 'ps',
		full: 'پښتو',
		english: '(Pashto)',
	},
	{
		short: 'pt',
		full: 'Português',
		english: '(Portuguese)',
	},
	{
		short: 'qu',
		full: 'Runa Simi',
		english: '(Quechua)',
	},
	{
		short: 'rm',
		full: 'rumantsch grischun',
		english: '(Romansh)',
	},
	{
		short: 'rn',
		full: 'Ikirundi',
		english: '(Kirundi)',
	},
	{
		short: 'ro',
		full: 'Română',
		english: '(Romanian)',
	},
	{
		short: 'ru',
		full: 'Русский',
		english: '(Russian)',
	},
	{
		short: 'rw',
		full: 'Ikinyarwanda',
		english: '(Kinyarwanda)',
	},
	{
		short: 'sa',
		full: 'संस्कृतम्',
		english: '(Sanskrit)',
	},
	{
		short: 'sc',
		full: 'sardu',
		english: '(Sardinian)',
	},
	{
		short: 'sd',
		full: 'सिन्धी',
		english: '(Sindhi)',
	},
	{
		short: 'se',
		full: 'Davvisámegiella',
		english: '(Northern Sami)',
	},
	{
		short: 'sg',
		full: 'yângâ tî sängö',
		english: '(Sango)',
	},
	{
		short: 'si',
		full: 'සිංහල',
		english: '(Sinhala)',
	},
	{
		short: 'sk',
		full: 'slovenčina',
		english: '(Slovak)',
	},
	{
		short: 'sl',
		full: 'slovenščina',
		english: '(Slovenian)',
	},
	{
		short: 'sn',
		full: 'chiShona',
		english: '(Shona)',
	},
	{
		short: 'so',
		full: 'Soomaaliga',
		english: '(Somali)',
	},
	{
		short: 'sq',
		full: 'Shqip',
		english: '(Albanian)',
	},
	{
		short: 'sr',
		full: 'српски језик',
		english: '(Serbian)',
	},
	{
		short: 'ss',
		full: 'SiSwati',
		english: '(Swati)',
	},
	{
		short: 'st',
		full: 'Sesotho',
		english: '(Southern Sotho)',
	},
	{
		short: 'su',
		full: 'Basa Sunda',
		english: '(Sundanese)',
	},
	{
		short: 'sv',
		full: 'Svenska',
		english: '(Swedish)',
	},
	{
		short: 'sw',
		full: 'Kiswahili',
		english: '(Swahili)',
	},
	{
		short: 'ta',
		full: 'தமிழ்',
		english: '(Tamil)',
	},
	{
		short: 'te',
		full: 'తెలుగు',
		english: '(Telugu)',
	},
	{
		short: 'tg',
		full: 'тоҷикӣ',
		english: '(Tajik)',
	},
	{
		short: 'th',
		full: 'ไทย',
		english: '(Thai)',
	},
	{
		short: 'ti',
		full: 'ትግርኛ',
		english: '(Tigrinya)',
	},
	{
		short: 'tk',
		full: 'Türkmen',
		english: '(Turkmen)',
	},
	{
		short: 'tl',
		full: 'Wikang Tagalog',
		english: '(Tagalog)',
	},
	{
		short: 'tn',
		full: 'Setswana',
		english: '(Tswana)',
	},
	{
		short: 'to',
		full: 'faka Tonga',
		english: '(Tonga)',
	},
	{
		short: 'tr',
		full: 'Türkçe',
		english: '(Turkish)',
	},
	{
		short: 'ts',
		full: 'Xitsonga',
		english: '(Tsonga)',
	},
	{
		short: 'tt',
		full: 'татар теле',
		english: '(Tatar)',
	},
	{
		short: 'tw',
		full: 'Twi',
		english: '(Twi)',
	},
	{
		short: 'ty',
		full: 'Reo Tahiti',
		english: '(Tahitian)',
	},
	{
		short: 'ug',
		full: 'ئۇيغۇرچە&lrm;',
		english: '(Uyghur)',
	},
	{
		short: 'uk',
		full: 'Українська',
		english: '(Ukrainian)',
	},
	{
		short: 'ur',
		full: 'اردو',
		english: '(Urdu)',
	},
	{
		short: 'uz',
		full: 'Ўзбек',
		english: '(Uzbek)',
	},
	{
		short: 've',
		full: 'Tshivenḓa',
		english: '(Venda)',
	},
	{
		short: 'vi',
		full: 'Tiếng Việt',
		english: '(Vietnamese)',
	},
	{
		short: 'vo',
		full: 'Volapük',
		english: '(Volapük)',
	},
	{
		short: 'wa',
		full: 'walon',
		english: '(Walloon)',
	},
	{
		short: 'wo',
		full: 'Wollof',
		english: '(Wolof)',
	},
	{
		short: 'xh',
		full: 'isiXhosa',
		english: '(Xhosa)',
	},
	{
		short: 'yi',
		full: 'ייִדיש',
		english: '(Yiddish)',
	},
	{
		short: 'yo',
		full: 'Yorùbá',
		english: '(Yoruba)',
	},
	{
		short: 'za',
		full: 'Saɯ cueŋƅ',
		english: '(Zhuang)',
	},
	{
		short: 'zh',
		full: '中文',
		english: '(Chinese)',
	},
	{
		short: 'zu',
		full: 'isiZulu',
		english: '(Zulu)',
	},
	{
		short: 'ast',
		full: 'Asturianu',
		english: '(Asturian)',
	},
	{
		short: 'ckb',
		full: 'سۆرانی',
		english: '(Sorani (Kurdish))',
	},
	{
		short: 'cnr',
		full: 'crnogorski',
		english: '(Montenegrin)',
	},
	{
		short: 'jbo',
		full: 'la .lojban.',
		english: '(Lojban)',
	},
	{
		short: 'kab',
		full: 'Taqbaylit',
		english: '(Kabyle)',
	},
	{
		short: 'kmr',
		full: 'Kurmancî',
		english: '(Kurmanji (Kurdish))',
	},
	{
		short: 'ldn',
		full: 'Láadan',
		english: '(Láadan)',
	},
	{
		short: 'lfn',
		full: 'lingua franca nova',
		english: '(Lingua Franca Nova)',
	},
	{
		short: 'sco',
		full: 'Scots',
		english: '(Scots)',
	},
	{
		short: 'sma',
		full: 'Åarjelsaemien Gïele',
		english: '(Southern Sami)',
	},
	{
		short: 'smj',
		full: 'Julevsámegiella',
		english: '(Lule Sami)',
	},
	{
		short: 'szl',
		full: 'ślůnsko godka',
		english: '(Silesian)',
	},
	{
		short: 'tai',
		full: 'ภาษาไท or ภาษาไต',
		english: '(Tai)',
	},
	{
		short: 'tok',
		full: 'toki pona',
		english: '(Toki Pona)',
	},
	{
		short: 'zba',
		full: 'باليبلن',
		english: '(Balaibalan)',
	},
	{
		short: 'zgh',
		full: 'ⵜⴰⵎⴰⵣⵉⵖⵜ',
		english: '(Standard Moroccan Tamazight)',
	},
];
