import { writable, type Writable } from 'svelte/store';

import type { Search } from '../types/search';

export let stSearchCurrent: Writable<Search | null> = writable(null);
export let stSearchHistoryUpdate: Writable<number> = writable(1);
