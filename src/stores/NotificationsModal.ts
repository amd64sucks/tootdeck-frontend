import { writable, type Writable } from 'svelte/store';
import { tick } from 'svelte';

import NotificationLoading from '../components/NotificationsModal/NotificationLoading.svelte';

/**
 * Types
 */
export interface NotificationModalEntity {
	id: number;
	type: NotificationModalType;
	text: string;
	dismissable: boolean;
	callback?: Function;
	component?: NotificationLoading;
}

export enum NotificationModalType {
	Error = 0,
	Warn = 1,
	Info = 2,
	Loading = 3,
}

/**
 * Stores
 */
export const stNotificationsModal: Writable<Array<NotificationModalEntity>> = writable([]);

/**
 * Functions
 */
let notification_id = 1;

export class notificationModal {
	static async createLoading(text: string, max: number) {
		await tick();

		const props = {
			id: notification_id++,
			type: NotificationModalType.Loading,
			text,
			dismissable: true,
		};
		const component = new NotificationLoading({
			props: { max, modal: props },
			target: document.querySelector('.notification-container')!,
		});

		stNotificationsModal.update((value) => {
			value.push({
				id: props.id,
				type: NotificationModalType.Loading,
				text,
				dismissable: true,
				component,
			});
			return value;
		});

		await tick();

		return component;
	}

	static create(
		type: NotificationModalType.Info | NotificationModalType.Warn | NotificationModalType.Error,
		text: string,
		dismissable: boolean,
		callback?: Function
	) {
		const id = notification_id++;

		stNotificationsModal.update((value) => {
			value.push({
				id,
				type,
				text,
				dismissable,
				callback,
			});
			return value;
		});

		return id;
	}

	static delete(id: number) {
		stNotificationsModal.update((value) => {
			value = value.filter((v) => {
				if (v.id === id && v.type === NotificationModalType.Loading) {
					v.component?.$destroy();
				}

				return v.id !== id;
			});
			return value;
		});
	}
}
