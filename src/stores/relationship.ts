import { writable, type Writable } from 'svelte/store';

export let stRelationshipUpdate: Writable<number> = writable(0);
