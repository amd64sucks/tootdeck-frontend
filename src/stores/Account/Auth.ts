import { writable, type Writable } from 'svelte/store';

import { TootDeckAPI } from '../../lib/APIs/TootDeckAPI/TootDeckAPI';
import { WebsocketSubscriptionsManager } from '../../lib/Websocket/WebsocketManager';

export type DisconnectReason = 'expired' | 'disconnect';

export const stLogged: Writable<boolean> = writable(false);
export const stAdmin: Writable<boolean> = writable(false);

export async function loginState() {
	let account = await TootDeckAPI.Auth.me();
	if (typeof account !== 'object') {
		return false;
	}

	stLogged.set(!!account);

	stAdmin.set(account?.admin ?? false);

	return !!account;
}

export async function logout(expired: boolean) {
	await TootDeckAPI.Auth.disconnect();
	WebsocketSubscriptionsManager.destroy();

	window.localStorage.setItem('disconnect_reason', expired ? 'expired' : 'disconnect');

	window.location.reload();
}
