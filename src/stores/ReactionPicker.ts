import { writable, type Writable } from 'svelte/store';

import type { TootDeckAccount } from './Account/Accounts';

import type { ParsedStatus } from '../types/contentParsed';

export interface ReactionPickerContext {
	account: TootDeckAccount;
	status: ParsedStatus;
	position: { x: number; y: number };
}

export let stReactionPicker: Writable<ReactionPickerContext | null> = writable(null);

export class ReactionPicker {
	static create(context: ReactionPickerContext) {
		stReactionPicker.update(() => context);
	}

	static close() {
		stReactionPicker.update(() => null);
	}
}
