import { writable, type Writable } from 'svelte/store';

import type { TootDeckAccount } from './Account/Accounts';

import type { ParsedAccount } from '../types/contentParsed';
import type { Composer } from '../types/composer';

/**
 * Types
 */
export type ExpandedPanelAllowed =
	| ExpandedPanelComposer
	| ExpandedPanelComposerWithData
	| ExpandedPanelSearch
	| ExpandedPanelProfile
	| ExpandedPanelAccount;

export enum ExpandedPanelState {
	Composer = 0,
	ComposerWithData = 1,
	Profile = 2,
	Search = 3,
	Account = 4,
}

export interface ExpandedPanelContent {
	state: ExpandedPanelState;
}

export interface ExpandedPanelComposer extends ExpandedPanelContent {
	state: ExpandedPanelState.Composer;
}

export interface ExpandedPanelComposerWithData extends ExpandedPanelContent {
	state: ExpandedPanelState.ComposerWithData;
	composer: Composer;
}

export interface ExpandedPanelSearch extends ExpandedPanelContent {
	state: ExpandedPanelState.Search;
}

export interface ExpandedPanelProfile extends ExpandedPanelContent {
	state: ExpandedPanelState.Profile;
	account: TootDeckAccount;
	status_account: ParsedAccount | (() => Promise<ParsedAccount | null>);
}

export interface ExpandedPanelAccount extends ExpandedPanelContent {
	state: ExpandedPanelState.Account;
}

/**
 * Stores
 */
export let stExpandedPanel: Writable<Array<ExpandedPanelAllowed>> = writable([]);

/**
 * Functions
 */
class ExpandedPanelInner {
	static readonly instance = new ExpandedPanelInner();

	private isComposer(new_state: ExpandedPanelAllowed): boolean {
		return (
			new_state.state === ExpandedPanelState.Composer ||
			new_state.state === ExpandedPanelState.ComposerWithData
		);
	}

	private add(exp: Array<ExpandedPanelAllowed>, new_state: ExpandedPanelAllowed) {
		exp.push(new_state);
		return exp;
	}

	private remove(exp: Array<ExpandedPanelAllowed>) {
		exp.pop();
		return exp;
	}

	private moveToForeground(exp: Array<ExpandedPanelAllowed>, new_state: ExpandedPanelAllowed) {
		const index = this.isComposer(new_state)
			? exp.findIndex((v) => this.isComposer(v))
			: exp.findIndex((v) => v.state === new_state.state);
		if (index !== -1) {
			exp.splice(index, 1);
			exp.push(new_state);
			return exp;
		}
	}

	update(new_state: ExpandedPanelAllowed) {
		if (!new_state) {
			return;
		}

		stExpandedPanel.update((exp) => {
			let previous = exp.at(-1);
			if (!previous) {
				return this.add(exp, new_state);
			}

			if (
				previous.state === ExpandedPanelState.Profile &&
				new_state.state === ExpandedPanelState.Profile
			) {
				if (
					Object.is(
						new_state.status_account,
						(previous as ExpandedPanelProfile).status_account
					)
				) {
					return exp;
				}

				return this.add(exp, new_state);
			}

			if (
				(this.isComposer(previous) &&
					this.isComposer(new_state) &&
					new_state.state === ExpandedPanelState.Composer) ||
				(previous.state === new_state.state &&
					new_state.state !== ExpandedPanelState.ComposerWithData)
			) {
				return this.remove(exp);
			}

			const ret = this.moveToForeground(exp, new_state);
			if (ret) {
				return ret;
			}

			return this.add(exp, new_state);
		});
	}

	previous() {
		stExpandedPanel.update((exp) => {
			return this.remove(exp);
		});
	}

	close() {
		stExpandedPanel.set([]);
	}
}

export const ExpandedPanel = ExpandedPanelInner.instance;
