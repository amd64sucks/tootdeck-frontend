import { writable, type Writable, get } from 'svelte/store';

import { stAccounts, type TootDeckAccount, Account } from '../Account/Accounts';
import { NotificationModalType, notificationModal } from '../NotificationsModal';
import { stOptionsReadOnly } from '../Options';
import { loadingUpdateState } from '../loading';
import { AppInit } from '../../AppInit';

import { columnFactory } from '../../lib/Columns/ColumnFactory';
import { EventsManager } from '../../lib/Events/EventsManager';
import { TootDeckEvents } from '../../lib/Events/TootDeckEvents';
import { errorModal } from '../../lib/errorModal';
import type { IndexedArray } from '../../lib/IndexedArray';
import { TootDeckConfig, type ConfigResponse } from '../../lib/TootDeckConfig';
import { TootDeckColumnIdentifier } from '../../lib/Columns/TootDeckColumnTypes';
import type { IColumn } from '../../lib/Columns/types/interfaces';
import type {
	StandardColumnOptions,
	StandardColumnParams,
	StandardNotificationColumnParams,
} from '../../lib/Columns/types/options';

import type { Entity } from '../../types/mastodonEntities';

export interface LayoutColumn<T, K> {
	icon: string;
	title: string;
	identifier: TootDeckColumnIdentifier | string;
	options: Writable<StandardColumnOptions>;
	using_links: boolean;
	account: TootDeckAccount;
	content: Writable<IndexedArray<K>>;
	instance: T;
	performance_mode: Writable<boolean>;
}

export interface LayoutColumnList<T, K> extends LayoutColumn<T, K> {
	list: Entity.List;
}

export interface IndexedLayoutColumn<T, K> extends LayoutColumn<T, K> {
	id: number;
}

export let stLayout: Writable<Array<IColumn>> = writable([]);

export class Layout {
	static add(column: IColumn, index?: number) {
		stLayout.update((value) => {
			if (index !== undefined) {
				value.splice(index, 0, column);
			} else {
				value.push(column);
			}

			return value;
		});
	}

	static delete(id: number) {
		stLayout.update((value) => {
			value.find((v) => v.id === id)?.destroy();
			return value.filter((v) => v.id !== id);
		});
	}

	static reset() {
		stLayout.set([]);
	}
}

export async function initColumnWithData(
	account: TootDeckAccount,
	identifier: string,
	params?: Record<string, any>
) {
	switch (identifier) {
		case TootDeckColumnIdentifier.Federated:
			{
				loadingUpdateState(
					'Loading Federated column for ' + account.mirrorAPI.handle_string
				);
				const request = await account.mirrorAPI.Timeline.getPublic();
				EventsManager.sendEvent(TootDeckEvents.FederatedTimeline, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
		case TootDeckColumnIdentifier.Local:
			{
				loadingUpdateState('Loading Local column for ' + account.mirrorAPI.handle_string);
				const request = await account.mirrorAPI.Timeline.getLocal();
				EventsManager.sendEvent(TootDeckEvents.LocalTimeline, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
		case TootDeckColumnIdentifier.Home:
			{
				loadingUpdateState('Loading Home column for ' + account.mirrorAPI.handle_string);
				const request = await account.mirrorAPI.Timeline.getHome();
				EventsManager.sendEvent(TootDeckEvents.ColumnHome, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
		case TootDeckColumnIdentifier.User:
			{
				loadingUpdateState('Loading User column for ' + account.mirrorAPI.handle_string);
				const request = await account.mirrorAPI.Accounts.getStatuses(account.entity.id);
				EventsManager.sendEvent(TootDeckEvents.ColumnUser, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
		case TootDeckColumnIdentifier.Notifications:
			{
				loadingUpdateState(
					'Loading Notifications column for ' + account.mirrorAPI.handle_string
				);
				const request = await account.mirrorAPI.Notifications.getAll({
					types: params?.options.types,
				});
				EventsManager.sendEvent(TootDeckEvents.Notification, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((n) => {
								if (n.status) {
									n.status = { ...n.status, animateIn: false, animateOut: true };
								}
								return { ...n, notify: false, animateIn: false, animateOut: true };
							}) ?? [],
				});
			}
			break;
		// case TootDeckColumnIdentifier.Search: request = account.mirrorAPI.Timeline.getHome(); break
		case TootDeckColumnIdentifier.List:
			{
				loadingUpdateState('Loading List column for ' + account.mirrorAPI.handle_string);
				const request = await account.mirrorAPI.Timeline.getList(params!.list.id);
				EventsManager.sendEvent(TootDeckEvents.List, {
					account,
					list_id: params!.list.id,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
		// case TootDeckColumnIdentifier.Trending: request = account.mirrorAPI.Timeline.getHome(); break
		case TootDeckColumnIdentifier.Favourites:
			{
				loadingUpdateState(
					'Loading Favourites column for ' + account.mirrorAPI.handle_string
				);
				const request = await account.mirrorAPI.Accounts.getFavourites();
				EventsManager.sendEvent(TootDeckEvents.Favourites, {
					account: account,
					data:
						structuredClone(request)
							?.data?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
					links: request?.links,
				});
			}
			break;
		// case TootDeckColumnIdentifier.Messages: request = account.mirrorAPI.Timeline.Conversations.get(); break
		// case TootDeckColumnIdentifier.Mentions: request = account.mirrorAPI.Timeline.getHome(); break
		case TootDeckColumnIdentifier.Bookmarks:
			{
				loadingUpdateState(
					'Loading Bookmarks column for ' + account.mirrorAPI.handle_string
				);
				const request = await account.mirrorAPI.Accounts.getBookmarks();
				EventsManager.sendEvent(TootDeckEvents.Bookmarks, {
					account: account,
					data:
						structuredClone(request)
							?.data?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
					links: request?.links,
				});
			}
			break;
		case TootDeckColumnIdentifier.Hashtag:
			{
				loadingUpdateState('Loading Hashtag column for ' + account.mirrorAPI.handle_string);
				const request = await account.mirrorAPI.Timeline.getTag(params!.hashtag);
				EventsManager.sendEvent(TootDeckEvents.HashtagColumn, {
					account: account,
					data:
						structuredClone(request)
							?.reverse()
							.map((s) => ({ ...s, animateIn: false, animateOut: true })) ?? [],
				});
			}
			break;
	}
}

let readonly: boolean = false;
AppInit.Promise.then(() => {
	stOptionsReadOnly.subscribe((v) => (readonly = v));
});

export async function init(config: ConfigResponse) {
	loadingUpdateState('Loading columns');

	const accounts = get(stAccounts);

	Layout.reset();

	const layout_config = config.value?.columns;
	if (layout_config && layout_config.length) {
		// let missing_account: Array<string> = [];

		for (const column of layout_config) {
			const account = Account.get(column.params.account, false);
			if (!account) {
				// missing_account.push(stringifyHandle(column.params.account));
				continue;
			}

			try {
				Layout.add(columnFactory.create(column.identifier, column.params));
			} catch (e) {
				errorModal(
					'Error when importing config: Unknown column type ' + column.identifier,
					undefined,
					true
				);
				throw e;
			}

			initColumnWithData(account, column.identifier, column.params).catch((e) => {
				notificationModal.create(
					NotificationModalType.Warn,
					'Failed to fetch initial column data.',
					true
				);
				console.error(e);
			});
		}
	} else {
		let default_layout: Array<IColumn> = [];

		let iter = accounts.values();
		let value: TootDeckAccount | undefined;
		while ((value = iter.next().value)) {
			loadingUpdateState('Loading columns for ' + value.mirrorAPI.handle_string);

			try {
				const params: StandardColumnParams = {
					account: {
						username: value.entity.username,
						domain: value.mirrorAPI.domain,
					},
					options: {
						collapsed: false,
						sound_uuid: null,
						override_title: {
							toggle: false,
							value: null,
						},
					},
				};
				const column = columnFactory.create(TootDeckColumnIdentifier.Home, params);

				default_layout.push(column);

				initColumnWithData(value, column.identifier, column.params).catch((e) => {
					notificationModal.create(
						NotificationModalType.Warn,
						'Failed to fetch initial column data.',
						true
					);
					console.error(e);
				});
			} catch (e: any) {
				errorModal(`Unable to create default column Home`, ['columnFactory: ', e], true);
				continue;
			}

			try {
				const params: StandardNotificationColumnParams = {
					account: {
						username: value.entity.username,
						domain: value.mirrorAPI.domain,
					},
					options: {
						collapsed: false,
						sound_uuid: null,
						override_title: {
							toggle: false,
							value: null,
						},
						group: false,
						types: [],
					},
				};
				const column = columnFactory.create(TootDeckColumnIdentifier.Notifications, params);

				default_layout.push(column);

				initColumnWithData(value, column.identifier, column.params).catch((e) => {
					notificationModal.create(
						NotificationModalType.Warn,
						'Failed to fetch initial column data.',
						true
					);
					console.error(e);
				});
			} catch (e: any) {
				errorModal(
					`Unable to create default column Notifications`,
					['columnFactory: ', e],
					true
				);
				continue;
			}
		}

		stLayout.set(default_layout);
	}

	initialized = true;

	loadingUpdateState('Finalizing');
}

/**
 * Config
 */
let initialized: boolean = false;

export function save() {
	if (!initialized || readonly) {
		return;
	}

	TootDeckConfig.saveLayout();
}

stLayout.subscribe(() => save());
