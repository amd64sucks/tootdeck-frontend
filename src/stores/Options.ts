import { derived, writable, type Readable, type Writable } from 'svelte/store';
import { type EmojiStyle, type TopRow } from 'emoji-mart-svelte';

import { changeTheme, type ThemeAvailable } from '../lib/ComponentFunctions/options/theme';
import { Account, type TootDeckAccount } from './Account/Accounts';
import { loadingUpdateState } from './loading';

import { type ConfigResponse } from '../lib/TootDeckConfig';
import { setfontSize } from '../lib/ComponentFunctions/options/fontSize';
import { setAccentColor } from '../lib/ComponentFunctions/options/accentColor';
import { type RGB } from '../lib/Utils/convertRGBA';
import { setFavicon } from '../lib/ComponentFunctions/options/setFavicon';
import { MOYAI, MOYAI_URL, type CustomMoyai } from '../lib/ComponentFunctions/moyai';

import { ActiveTab } from '../types/profile';

/**
 * Types
 */
export enum ComposerFromView {
	ProfilePicture = 0,
	ProfilePictureSmall = 1,
	Full = 2,
}

export interface InstanceOverride {
	[instance: string]: {
		emoji_reaction: boolean | null;
		poll_allow_media: boolean | null;
		poll_max_options: number | null;
		poll_max_characters_per_option: number | null;
		poll_min_expiration: number | null;
		poll_max_expiration: number | null;
	};
}

export interface Options {
	appearance: {
		// Single value
		accentColor: RGB;
		accentScrollbar: boolean;
		fontSize: string;
		theme: ThemeAvailable | undefined;
		customCSS: string;

		// objects
		animation: {
			animateGif: {
				avatar: boolean;
				emoji: boolean;
				notification: boolean;
				status: boolean;
			};
			column: {
				content: boolean;
			};
			mediaViewer: {
				mediaChange: boolean;
			};
			speedFactor: number;
		};
		column: {
			header: {
				avatar: boolean;
				btn: {
					clear: boolean;
					collapse: boolean;
					delete: boolean;
				};
				icon: boolean;
			};
			width: string;
		};
		composer: {
			fromView: ComposerFromView;
		};
		/**
		 * TODO: Change with The Config Update ™
		 */
		customValue: {
			publishBtn: string;
			reblog: string;
			reblogBtn: string;
			reblogs: string;
		};
		background: {
			image: string;
			opacity: {
				value: number;
				composer: boolean;
				headers: boolean;
				sidebar: boolean;
			};
		};
		border: {
			status: boolean;
			column: boolean;
		};
		emoji: {
			style: EmojiStyle;
			reaction: {
				value: boolean;
				inQuote: boolean;
				max: number;
			};
		};
		extra: {
			snow: boolean;
			cherryBlossomPetals: boolean;
			mutualIndicator: boolean;
			removePrefixHandle: {
				value: boolean;
				items: Array<string>;
			};
			prideLogo: {
				toggle: boolean;
				scheduled: boolean;
				random: boolean;
				src: string;
				userDefined: string;
			};
		};
		status: {
			interaction: {
				compactMode: boolean;
				showOnHover: boolean;
			};
			metadata: {
				language: boolean;
				visibility: boolean;
			};
		};
	};
	behavior: {
		column: {
			collapseReadMessage: boolean; // Soon™
			maxStatuses: number;
			pauseOnHover: boolean; // Soon™
			hideDMs: boolean;
			thread: {
				insertStatusBeforeSelected: boolean;
				openInAnotherColumn: boolean;
			};
		};
		composer: {
			reverseMediaOrder: boolean;
			stayOpen: boolean;
			favouriteLanguages: Array<string>;
			simultaneousUpload: boolean;
		};
		emoji: {
			zoomOnHover: boolean;
		};
		emojiPicker: {
			toggleWideEmoji: boolean;
			topRow: TopRow;
		};
		expandedPanel: {};
		instance: InstanceOverride;
		navbar: {
			expanded: boolean;
		};
		notification: {
			highlightDuration: number;
			highlightNew: boolean;
			playCustomSound: {
				value: boolean;
				volume: number;
				limit: number;
				sounds: Array<CustomMoyai>;
			};
		};
		notificationModal: {
			dismissAfter: number;
		};
		profile: {
			defaultTab: ActiveTab;
			showMediaAsGrid: boolean;
		};
		search: {
			toggleAdvancedView: boolean;
			querySearchInHistory: boolean;
		};
		status: {
			hideThreshold: number;
			quote: boolean;
			showInteractionsOnQuote: boolean;
			twitterEmbed: {
				value: boolean;
				exclude: Array<{
					id: string;
					acct: string;
				}>;
			};
			youtubeEmbed: {
				value: boolean;
				piped: {
					value: boolean;
					hostname: string;
				};
			};
			DailymotionEmbed: boolean;
		};
		sync: {
			autoSync: boolean;
		};
		unreadInTitle: {
			message: boolean; // Soon™
			notification: boolean; // Soon™
		};
		showChangelog: boolean;
	};
}

export const default_options: Options = {
	appearance: {
		// Single value
		accentColor: '99, 100, 255',
		accentScrollbar: true,
		fontSize: '14px',
		theme: undefined,
		customCSS: '',

		// objects
		animation: {
			animateGif: {
				avatar: true,
				emoji: true,
				notification: true,
				status: true,
			},
			column: {
				content: true,
			},
			mediaViewer: {
				mediaChange: true,
			},
			speedFactor: 1,
		},
		column: {
			header: {
				avatar: false,
				btn: {
					clear: false,
					collapse: true,
					delete: false,
				},
				icon: true,
			},
			width: '370px',
		},
		composer: {
			fromView: ComposerFromView.ProfilePicture,
		},
		customValue: {
			publishBtn: 'Publish',
			reblog: 'Reblogged',
			reblogBtn: 'Reblog',
			reblogs: 'Reblogs',
		},
		border: {
			status: false,
			column: false,
		},
		background: {
			image: '',
			opacity: {
				value: 1,
				composer: false,
				headers: false,
				sidebar: false,
			},
		},
		emoji: {
			style: 'twitter',
			reaction: {
				value: true,
				inQuote: false,
				max: 6,
			},
		},
		extra: {
			snow: false,
			cherryBlossomPetals: false,
			mutualIndicator: true,
			removePrefixHandle: {
				value: false,
				items: [
					'mstdn',
					'fedi',
					'social',
					'akko(?:ma)*',
					'misskey',
					'mk',
					'sharkey',
					'shork',
				],
			},
			prideLogo: {
				toggle: true,
				scheduled: true,
				random: true,
				src: 'mastodon',
				userDefined: '',
			},
		},
		status: {
			interaction: {
				compactMode: false,
				showOnHover: false,
			},
			metadata: {
				language: false,
				visibility: true,
			},
		},
	},
	behavior: {
		column: {
			collapseReadMessage: false, // Soon™
			maxStatuses: 40,
			pauseOnHover: false, // Soon™
			hideDMs: false,
			thread: {
				insertStatusBeforeSelected: true,
				openInAnotherColumn: false,
			},
		},
		composer: {
			reverseMediaOrder: false,
			stayOpen: false,
			favouriteLanguages: [],
			simultaneousUpload: true,
		},
		emoji: {
			zoomOnHover: true,
		},
		emojiPicker: {
			toggleWideEmoji: true,
			topRow: 'recently',
		},
		expandedPanel: {},
		instance: {},
		navbar: {
			expanded: true,
		},
		notification: {
			highlightDuration: 3,
			highlightNew: true,
			playCustomSound: {
				value: false,
				volume: 0.5,
				limit: 10,
				sounds: [
					{
						name: 'Moyai',
						src: MOYAI_URL,
						find: MOYAI,
					},
				],
			},
		},
		notificationModal: {
			dismissAfter: 0,
		},
		profile: {
			defaultTab: ActiveTab.Posts,
			showMediaAsGrid: true,
		},
		status: {
			hideThreshold: 200,
			quote: true,
			showInteractionsOnQuote: true,
			twitterEmbed: {
				value: true,
				exclude: [],
			},
			youtubeEmbed: {
				value: true,
				piped: {
					value: true,
					hostname: 'https://piped.video',
				},
			},
			DailymotionEmbed: true,
		},
		search: {
			toggleAdvancedView: false,
			querySearchInHistory: false,
		},
		sync: {
			autoSync: false,
		},
		unreadInTitle: {
			notification: true, // Soon™
			message: true, // Soon™
		},
		showChangelog: true,
	},
};

/**
 * Stores
 */
export let stOptions: Writable<Options> = writable(default_options);
export let stDebug: Writable<boolean> = writable(false);
export let stDebugLeaks: Writable<boolean> = writable(false);
export let stDebugComposer: Writable<boolean> = writable(false);
export let stDebugSearch: Writable<boolean> = writable(false);
export let stOptionsReadOnly: Writable<boolean> = writable(false);

/**
 * Appearance
 */

// Single value
export let rdOptionsAppearanceAccentColor: Readable<RGB> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.accentColor
);

export let rdOptionsAppearanceAccentScrollbar: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.accentScrollbar
);

export let rdOptionsAppearanceFontSize: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.fontSize
);

export let rdOptionsAppearanceTheme: Readable<ThemeAvailable | undefined> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.theme
);

rdOptionsAppearanceTheme.subscribe((v) => changeTheme(v));

export let rdOptionsAppearanceCustomCSS: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.customCSS
);

// Animation
export let rdOptionsAppearanceAnimationAnimateGifAvatar: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.animateGif.avatar
);

export let rdOptionsAppearanceAnimationAnimateGifEmoji: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.animateGif.emoji
);

export let rdOptionsAppearanceAnimationAnimateGifNotification: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.animateGif.notification
);

export let rdOptionsAppearanceAnimationAnimateGifStatus: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.animateGif.status
);

export let rdOptionsAppearanceAnimationColumnContent: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.column.content
);

export let rdOptionsAppearanceAnimationMediaViewerMediaChange: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.mediaViewer.mediaChange
);

export let rdOptionsAppearanceAnimationSpeedFactor: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.animation.speedFactor
);

// Column
export let rdOptionsAppearanceColumnHeaderAvatar: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.header.avatar
);

export let rdOptionsAppearanceColumnHeaderBtnClear: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.header.btn.clear
);

export let rdOptionsAppearanceColumnHeaderBtnCollapse: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.header.btn.collapse
);

export let rdOptionsAppearanceColumnHeaderBtnDelete: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.header.btn.delete
);

export let rdOptionsAppearanceColumnHeaderIcon: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.header.icon
);

export let rdOptionsAppearanceColumnWidth: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.column.width
);

// Composer
export let rdOptionsAppearanceComposerFromView: Readable<ComposerFromView> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.composer.fromView
);

// Custom value
export let rdOptionsAppearanceCustomValuePublishBtn: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.customValue.publishBtn
);

export let rdOptionsAppearanceCustomValueReblog: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.customValue.reblog
);

export let rdOptionsAppearanceCustomValueReblogBtn: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.customValue.reblogBtn
);

export let rdOptionsAppearanceCustomValueReblogs: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.customValue.reblogs
);

// Background
export let rdOptionsAppearanceBackgroundImage: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.background.image
);

export let rdOptionsAppearanceBackgroundOpacityValue: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.background.opacity.value
);

export let rdOptionsAppearanceBackgroundOpacityComposer: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.background.opacity.composer
);

export let rdOptionsAppearanceBackgroundOpacityHeaders: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.background.opacity.headers
);

export let rdOptionsAppearanceBackgroundOpacitySidebar: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.background.opacity.sidebar
);

// Border
export let rdOptionsAppearanceBorderStatus: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.border.status
);

export let rdOptionsAppearanceBorderColumn: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.border.column
);

// Emoji
export let rdOptionsAppearanceEmojiStyle: Readable<EmojiStyle> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.emoji.style
);

export let rdOptionsAppearanceEmojiReaction: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.emoji.reaction.value
);

export let rdOptionsAppearanceEmojiReactionInQuote: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.emoji.reaction.inQuote
);

export let rdOptionsAppearanceEmojiReactionMax: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.emoji.reaction.max
);

// Status
export let rdOptionsAppearanceStatusInteractionCompactMode: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.status.interaction.compactMode
);

export let rdOptionsAppearanceStatusInteractionShowOnHover: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.status.interaction.showOnHover
);

export let rdOptionsAppearanceStatusMetadataLanguage: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.status.metadata.language
);

export let rdOptionsAppearanceStatusMetadataVisibilty: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.status.metadata.visibility
);

// Extra
export let rdOptionsAppearanceExtraSnow: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.snow
);

export let rdOptionsAppearanceExtraCherryBlossomPetals: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.cherryBlossomPetals
);

export let rdOptionsAppearanceExtraMutualIndicator: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.mutualIndicator
);

export let rdOptionsAppearanceExtraRemovePrefixHandle: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.removePrefixHandle.value
);

export let rdOptionsAppearanceExtraRemovePrefixHandleItems: Readable<Array<string>> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.removePrefixHandle.items
);

export let rdOptionsAppearanceExtraPrideLogoToggle: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.prideLogo.toggle
);

export let rdOptionsAppearanceExtraPrideLogoScheduled: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.prideLogo.scheduled
);

export let rdOptionsAppearanceExtraPrideLogoRandom: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.prideLogo.random
);

export let rdOptionsAppearanceExtraPrideLogoSrc: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.prideLogo.src
);

export let rdOptionsAppearanceExtraPrideLogoUserDefined: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.appearance.extra.prideLogo.userDefined
);

/**
 * Behavior
 */
// Column
export let rdOptionsBehaviorColumnCollapseReadMessage: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.collapseReadMessage
);

export let rdOptionsBehaviorColumnMaxStatuses: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.maxStatuses
);

export let rdOptionsBehaviorColumnPauseOnHover: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.pauseOnHover
);

export let rdOptionsBehaviorColumnHideDMs: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.hideDMs
);

export let rdOptionsBehaviorColumnThreadInsertStatusBeforeSelected: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.thread.insertStatusBeforeSelected
);

export let rdOptionsBehaviorColumnThreadOpenInAnotherColumn: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.column.thread.openInAnotherColumn
);

// Composer
export let rdOptionsBehaviorComposerReverseMediaOrder: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.composer.reverseMediaOrder
);

export let rdOptionsBehaviorComposerStayOpen: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.composer.stayOpen
);

export let rdOptionsBehaviorComposerFavouriteLanguages: Readable<Array<string>> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.composer.favouriteLanguages
);

export let rdOptionsBehaviorComposerSimultaneousUpload: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.composer.simultaneousUpload
);

// Emoji
export let rdOptionsBehaviorEmojiZoomOnHover: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.emoji.zoomOnHover
);

// Emoji picker
export let rdOptionsBehaviorEmojiPickerToggleWideEmoji: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.emojiPicker.toggleWideEmoji
);

export let rdOptionsBehaviorEmojiPickerTopRow: Readable<TopRow> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.emojiPicker.topRow
);

// Expanded panel

// Instance
export let rdOptionsBehaviorInstance: Readable<InstanceOverride> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.instance
);

// Navbar
export let rdOptionsBehaviorNavbarExpanded: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.navbar.expanded
);

// Notification
export let rdOptionsBehaviorNotificationHighlightDuration: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.highlightDuration
);

export let rdOptionsBehaviorNotificationHighlightNew: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.highlightNew
);

// Custom sound
export let rdOptionsBehaviorNotificationPlayCustomSound: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.playCustomSound.value
);

export let rdOptionsBehaviorNotificationPlayCustomSoundLimit: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.playCustomSound.limit
);

export let rdOptionsBehaviorNotificationPlayCustomSoundVolume: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.playCustomSound.volume
);

export let rdOptionsBehaviorNotificationPlayCustomSounds: Readable<Array<CustomMoyai>> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notification.playCustomSound.sounds
);

// Notification Modal
export let rdOptionsBehaviorNotificationModalDismissAfter: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.notificationModal.dismissAfter
);

// Profile
export let rdOptionsBehaviorProfileDefaultTab: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.profile.defaultTab
);

export let rdOptionsBehaviorProfileShowMediaAsGrid: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.profile.showMediaAsGrid
);

// Status
export let rdOptionsBehaviorStatusHideThreshold: Readable<number> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.hideThreshold
);

export let rdOptionsBehaviorStatusQuote: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.quote
);

export let rdOptionsBehaviorStatusShowInteractionsOnQuote: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.showInteractionsOnQuote
);

export let rdOptionsBehaviorStatusTwitterEmbed: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.twitterEmbed.value
);

export let rdOptionsBehaviorStatusTwitterEmbedExcluded: Readable<
	Array<{
		id: string;
		acct: string;
	}>
> = derived(stOptions, ($stOptions) => $stOptions.behavior.status.twitterEmbed.exclude);

export let rdOptionsBehaviorStatusYoutubeEmbed: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.youtubeEmbed.value
);

export let rdOptionsBehaviorStatusYoutubeEmbedPiped: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.youtubeEmbed.piped.value
);

export let rdOptionsBehaviorStatusYoutubeEmbedPipedHostname: Readable<string> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.youtubeEmbed.piped.hostname
);

export let rdOptionsBehaviorStatusDailymotionEmbed: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.status.DailymotionEmbed
);

// Search
export let rdOptionsBehaviorSearchToggleAdvancedView: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.search.toggleAdvancedView
);

export let rdOptionsBehaviorSearchQuerySearchInHistory: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.search.querySearchInHistory
);

// Sync
export let rdOptionsBehaviorSyncAutoSync: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.sync.autoSync
);

// Unread in title
export let rdOptionsBehaviorUnreadInTitleMessage: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.unreadInTitle.message
);

export let rdOptionsBehaviorUnreadInTitleNotification: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.unreadInTitle.notification
);

// Changelog
export let rdOptionsBehaviorShowChangelog: Readable<boolean> = derived(
	stOptions,
	($stOptions) => $stOptions.behavior.showChangelog
);

/**
 * Functions
 */

// prettier-ignore
function setInstanceOverrides(options: Options, account: TootDeckAccount) {
	const instance_override = options.behavior.instance[account.mirrorAPI.domain];
	if (!instance_override) {
		return
	}

	const polls = account.instance.configuration.polls

	instance_override.poll_allow_media					= polls?.allow_media ?? null;
	instance_override.poll_max_options					= polls?.max_options ?? null;
	instance_override.poll_max_characters_per_option	= polls?.max_characters_per_option ?? null;
	instance_override.poll_min_expiration				= polls?.min_expiration ?? null;
	instance_override.poll_max_expiration				= polls?.max_expiration ?? null;
}

export function init(config: ConfigResponse) {
	loadingUpdateState('Retrieving configuration');

	const options = config?.value!.options;
	if (options) {
		stOptions.set(options);
		setfontSize(options.appearance.fontSize);
		setAccentColor(options.appearance.accentColor);
		setFavicon(options.appearance.extra.prideLogo);
	}

	const accounts_options = config?.value!.accounts_options;
	if (accounts_options) {
		accounts_options.forEach((account_opts) => {
			const account = Account.get(account_opts.handle, false);
			if (account) {
				account.options.set(account_opts.value);
				setInstanceOverrides(options, account);
			}
		});
	}
}
