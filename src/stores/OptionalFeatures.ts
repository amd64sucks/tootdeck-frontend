import { derived, writable, type Writable } from 'svelte/store';
import type { Readable } from 'svelte/motion';

import { loadingUpdateState } from './loading';

import { TootDeckAPI } from '../lib/APIs/TootDeckAPI/TootDeckAPI';
import {
	GITLAB_REPO_BACKEND,
	GITLAB_REPO_FRONTEND,
	GITLAB_REPO_GLOBAL,
} from '../lib/Constants/Repositories';

/**
 * Types
 */
export interface OptionalFeatures {
	CACHE_STATUS_INTERACTIONS: boolean;
	CACHE_STATUS_REACTIONS: boolean;
	CACHE_TWITTER_EMBED: boolean;
	WORKER: boolean;
	CHANGELOG: boolean;
}

const default_params: OptionalFeatures = {
	CACHE_STATUS_INTERACTIONS: false,
	CACHE_STATUS_REACTIONS: false,
	CACHE_TWITTER_EMBED: false,
	WORKER: false,
	CHANGELOG: false,
};

/**
 * Stores
 */
let stOptionalFeatures: Writable<OptionalFeatures> = writable(default_params);

export let rdOptionalFeaturesCacheStatusInteractions: Readable<boolean> = derived(
	stOptionalFeatures,
	($stOptionalFeatures) =>
		$stOptionalFeatures.CACHE_STATUS_INTERACTIONS && $stOptionalFeatures.WORKER
);

export let rdOptionalFeaturesCacheStatusReactions: Readable<boolean> = derived(
	stOptionalFeatures,
	($stOptionalFeatures) =>
		$stOptionalFeatures.CACHE_STATUS_REACTIONS && $stOptionalFeatures.WORKER
);

export let rdOptionalFeaturesCacheTwitterEmbed: Readable<boolean> = derived(
	stOptionalFeatures,
	($stOptionalFeatures) => $stOptionalFeatures.CACHE_TWITTER_EMBED && $stOptionalFeatures.WORKER
);

export let rdOptionalFeaturesWorker: Readable<boolean> = derived(
	stOptionalFeatures,
	($stOptionalFeatures) => $stOptionalFeatures.WORKER
);

export let rdOptionalFeaturesChangelog: Readable<boolean> = derived(
	stOptionalFeatures,
	($stOptionalFeatures) =>
		$stOptionalFeatures.CHANGELOG &&
		!!GITLAB_REPO_GLOBAL &&
		!!GITLAB_REPO_BACKEND &&
		!!GITLAB_REPO_FRONTEND
);

/**
 * Functions
 */
export async function init() {
	loadingUpdateState('Getting optional features');

	const response = await TootDeckAPI.Others.optionalFeatures();
	if (!response) {
		return;
	}

	stOptionalFeatures.set(response);
}
