export enum InstanceType {
	Mastodon = 'mastodon',
	Pleroma = 'pleroma',
	Misskey = 'misskey',
	// Friendica = 'friendica',
}
