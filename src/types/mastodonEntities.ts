export namespace Entity {
	export interface Account {
		acct: string;
		avatar_static: string;
		avatar: string;
		bot: boolean | null;
		created_at: string;
		discoverable: undefined | boolean;
		display_name: string;
		emojis: Array<Emoji>;
		fields: Array<Field>;
		followers_count: number;
		following_count: number;
		group: boolean | null;
		header_static: string;
		header: string;
		id: string;
		limited: boolean | null;
		locked: boolean;
		moved: Account | null;
		mute_expires_at: undefined | string;
		noindex: boolean | null;
		note: string;
		role: undefined | Role;
		source: undefined | Source;
		statuses_count: number;
		suspended: boolean | null;
		url: string;
		username: string;
	}

	export interface Activity {
		logins: string;
		registrations: string;
		statuses: string;
		week: string;
	}

	export interface AnnouncementAccount {
		acct: string;
		id: string;
		url: string;
		username: string;
	}

	export interface AnnouncementStatus {
		id: string;
		url: string;
	}

	export interface AnnouncementReaction {
		count: number;
		me: boolean | null;
		name: string;
		static_url: string | null;
		url: string | null;
	}

	export interface Announcement {
		all_day: boolean;
		content: string;
		emojis: Array<Emoji>;
		ends_at: string | null;
		id: string;
		mentions: Array<AnnouncementAccount>;
		published_at: string;
		published: boolean;
		reactions: Array<AnnouncementReaction>;
		read: boolean | null;
		starts_at: string | null;
		statuses: Array<AnnouncementStatus>;
		tags: Array<StatusTag>;
		updated_at: string | null;
	}

	export interface Application {
		name: string;
		vapid_key: string | null;
		website: string | null;
	}

	export enum AttachmentType {
		Unknown = 'unknown',
		Image = 'image',
		Gifv = 'gifv',
		Video = 'video',
		Audio = 'audio',
	}

	export interface AsyncAttachment {
		blurhash: string | null;
		description: string | null;
		id: string;
		meta: Meta | null;
		preview_url: string;
		remote_url: string | null;
		text_url: string | null;
		type: AttachmentType;
		url: string | null;
	}

	export interface Attachment {
		blurhash: string | null;
		description: string | null;
		id: string;
		meta: Meta | null;
		preview_url: string | null;
		remote_url: string | null;
		text_url: string | null;
		type: AttachmentType;
		url: string;
	}

	export interface Sub {
		// For Image, Gifv, and Video
		aspect?: number;
		height?: number;
		size?: string;
		width?: number;

		// For Gifv and Video
		frame_rate?: string;

		// For Audio, Gifv, and Video
		bitrate?: number;
		duration?: number;
	}

	export interface Focus {
		x: number;
		y: number;
	}

	export interface Meta {
		aspect?: number;
		audio_bitrate?: string;
		audio_channel?: string;
		audio_encode?: string;
		duration?: number;
		focus?: Focus;
		fps?: number;
		height?: number;
		length?: string;
		original?: Sub;
		size?: string;
		small?: Sub;
		width?: number;
	}

	export enum CardType {
		Link = 'link',
		Photo = 'photo',
		Video = 'video',
		Rich = 'rich',
	}

	export interface Card {
		author_name: string | null;
		author_url: string | null;
		blurhash: string | null;
		description: string;
		embed_url: string | null;
		height: number | null;
		html: string | null;
		image: string | null;
		provider_name: string | null;
		provider_url: string | null;
		title: string;
		type: CardType;
		url: string;
		width: number | null;
	}

	export interface Context {
		ancestors: Array<Status>;
		descendants: Array<Status>;
	}

	export interface Conversation {
		accounts: Array<Account>;
		id: string;
		last_status: Status | null;
		unread: boolean;
	}

	export interface Emoji {
		category?: string;
		shortcode: string;
		static_url: string;
		url: string;
		visible_in_picker: boolean;
	}

	export interface FeaturedTag {
		id: string;
		last_status_at: string;
		name: string;
		statuses_count: number;
	}

	export interface Field {
		name: string;
		value: string;
		verified_at: string | null;
	}

	export interface Filter {
		context: Array<string>;
		expires_at: string | null;
		id: string;
		irreversible: boolean;
		phrase: string;
		whole_word: boolean;
	}

	export interface FollowRequest {
		acct: string;
		avatar_static: string;
		avatar: string;
		bot: boolean;
		created_at: string;
		discoverable?: boolean;
		display_name: string;
		emojis: Array<Emoji>;
		fields: Array<Field>;
		followers_count: number;
		following_count: number;
		group: boolean;
		header_static: string;
		header: string;
		id: number;
		locked: boolean;
		note: string;
		statuses_count: number;
		url: string;
		username: string;
	}

	export interface History {
		accounts: string;
		day: string;
		uses: string;
	}

	export interface IdentityProof {
		profile_url: string;
		proof_url: string;
		provider_username: string;
		provider: string;
		updated_at: string;
	}

	export interface Instance {
		approval_required: boolean;
		configuration: {
			features?: Array<string>;
			media_attachments?: {
				supported_mime_types: Array<string>;
				image_size_limit: number;
				image_matrix_limit: number;
				video_size_limit: number;
				video_frame_limit: number;
				video_matrix_limit: number;
			};
			polls?: {
				allow_media?: boolean;
				max_characters_per_option: number;
				max_expiration: number;
				max_options: number;
				min_expiration: number;
			};
			reactions?: {
				max_reactions: 1;
			};
			statuses: {
				characters_reserved_per_url?: number;
				max_characters: number;
				max_media_attachments?: number;
			};
		};
		contact_account?: Account;
		description: string;
		email: string;
		invites_enabled?: boolean;
		languages: Array<string>;
		registrations: boolean;
		rules?: Array<InstanceRule>;
		stats: Stats;
		thumbnail: string | null;
		title: string;
		uri: string;
		urls: URLs | null;
		version: string;

		// Extended
		support_emoji_reactions: boolean;
	}

	export interface InstanceRule {
		id: string;
		text: string;
	}

	export enum RepliesPolicy {
		Followed = 'followed',
		List = 'list',
		None = 'none',
	}

	export interface List {
		id: string;
		replies_policy: RepliesPolicy | null;
		exclusive: boolean;
		title: string;
	}

	export interface Marker {
		home?: {
			last_read_id: string;
			updated_at: string;
			version: number;
		};
		notifications?: {
			last_read_id: string;
			unread_count?: number;
			updated_at: string;
			version: number;
		};
	}

	export interface Mention {
		acct: string;
		id: string;
		url: string;
		username: string;
	}

	export interface Notification {
		account: Account;
		animateIn: boolean;
		animateOut: boolean;
		created_at: string;
		emoji?: string;
		id: string;
		status?: Status;
		target?: Account;
		type: NotificationType;
	}

	export enum NotificationType {
		Mention = 'mention',
		Status = 'status',
		Reblog = 'reblog',
		Follow = 'follow',
		Follow_request = 'follow_request',
		Favourite = 'favourite',
		Poll = 'poll',
		Update = 'update',
		AdminSignUp = 'admin.sign_up',
		AdminReport = 'admin.report',

		// Extended
		PollExpired = 'poll_expired',
		FollowRequest = 'follow_request',
		ReactionChuckya = 'reaction',
		ReactionPleroma = 'emoji_reaction',
	}

	export interface Poll {
		expired: boolean;
		expires_at: string | null;
		id: string;
		multiple: boolean;
		options: Array<PollOption>;
		own_votes: Array<number>;
		voted: boolean;
		votes_count: number;
		voters_count: number;
	}

	export interface PollOption {
		title: string;
		votes_count: number | null;
	}

	export enum PreferencesMedia {
		Default = 'default',
		ShowAll = 'show_all',
		HideAll = 'hide_all',
	}

	export interface Preferences {
		'posting:default:language': string | null;
		'posting:default:sensitive': boolean;
		'posting:default:visibility': Visibility;
		'reading:expand:media': PreferencesMedia;
		'reading:expand:spoilers': boolean;
	}

	export interface Alerts {
		favourite: boolean;
		follow: boolean;
		mention: boolean;
		poll: boolean;
		reblog: boolean;
	}

	export interface PushSubscription {
		alerts: Alerts;
		endpoint: string;
		id: string;
		server_key: string;
	}

	export interface Reaction {
		accounts: Array<Account> | undefined;
		count: number;
		me: boolean;
		name: string;
		static_url: string | undefined;
		url: string | undefined;
	}

	export interface Relationship {
		blocked_by: boolean;
		blocking: boolean;
		domain_blocking: boolean;
		endorsed: boolean;
		followed_by: boolean;
		following: boolean;
		id: string;
		muting_notifications: boolean;
		muting: boolean;
		note: string | null;
		notifying: boolean;
		requested: boolean;
		requested_by: boolean | null;
		showing_reblogs: boolean;
	}

	export interface Report {
		action_taken_at: string | null;
		action_taken: boolean;
		id: string;
		rule_ids: Array<string> | null;
		status_ids: Array<string> | null;

		// These parameters don't exist in Pleroma
		category: ReportCategory | null;
		comment: string | null;
		forwarded: boolean | null;
		target_account: Account | null;
	}

	export enum ReportCategory {
		Spam = 'spam',
		Violation = 'violation',
		Other = 'other',
	}

	export interface Response<T> {
		data: T;
		status: number;
		statusText: string;
		headers: any;
	}

	export interface Results {
		accounts: Array<Account>;
		hashtags: Array<Tag>;
		statuses: Array<Status>;
	}

	export interface Role {
		name: string;
	}

	export interface ScheduledStatus {
		id: string;
		media_attachments: Array<Attachment> | null;
		params: StatusParams;
		scheduled_at: string;
	}

	export interface Source {
		fields: Array<Field>;
		language: string | null;
		note: string;
		privacy: string | null;
		sensitive: boolean | null;
	}

	export interface Stats {
		domain_count: number;
		status_count: number;
		user_count: number;
	}

	export interface Status {
		account: Account;
		animateIn: boolean;
		animateOut: boolean;
		application: Application | null;
		bookmarked: boolean;
		card: Card | null;
		content: string;
		created_at: string;
		edited_at: string | null;
		emoji_reactions: Array<Reaction>;
		emojis: Array<Emoji>;
		favourited: boolean | null;
		favourites_count: number;
		id: string;
		in_reply_to_account_id: string | null;
		in_reply_to_id: string | null;
		language: string | null;
		media_attachments: Array<Attachment>;
		mentions: Array<Mention>;
		muted: boolean | null;
		pinned: boolean | null;
		plain_content: string | null;
		poll: Poll | null;
		quote: boolean;
		reblog: Status | null;
		reblogged: boolean | null;
		reblogs_count: number;
		replies_count: number;
		sensitive: boolean;
		spoiler_text: string;
		tags: Array<StatusTag>;
		uri: string;
		url: string;
		visibility: Visibility;
	}

	export enum Visibility {
		Public = 'public',
		Unlisted = 'unlisted',
		Private = 'private',
		Direct = 'direct',
	}

	export interface StatusTag {
		name: string;
		url: string;
	}

	export interface StatusEdit {
		account: Account;
		content: string;
		created_at: string;
		emojis: Array<Emoji>;
		media_attachments: Array<Attachment>;
		poll: Poll | null;
		sensitive: boolean;
		spoiler_text: string;
	}

	export interface StatusParams {
		application_id: number | null;
		in_reply_to_id: string | null;
		media_ids: Array<string> | null;
		scheduled_at: string | null;
		sensitive: boolean | null;
		spoiler_text: string | null;
		text: string;
		visibility: Visibility | null;
	}

	export interface StatusSource {
		id: string;
		spoiler_text: string;
		text: string;
	}

	export interface Tag {
		following?: boolean;
		history?: Array<History>;
		name: string;
		url: string;
	}

	export interface Token {
		access_token: string;
		created_at: number;
		scope: string;
		token_type: string;
	}

	export interface URLs {
		streaming_api: string;
	}
}
