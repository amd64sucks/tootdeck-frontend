export interface CustomEmoji {
	id: string;
	name: string;
	keywords: Array<string>;
	skins: Array<{ src: string; static_src: string }>;
}

export interface CustomEmojiCatergory {
	id: string;
	name: string;
	emojis: Array<CustomEmoji>;
}

export interface EmojiPicked {
	aliases: Array<string>;
	id: string;
	keywords: Array<string>;
	name: string;
	native: string;
	shortcodes: string;
	skin: 1;
	unified: string;
}
