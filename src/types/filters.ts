export enum FilterState {
	None = 0,
	Words = 1,
	Muted = 2,
	BlockedUsers = 3,
	BlockedDomains = 4,
}
