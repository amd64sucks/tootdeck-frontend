import type { RemoteInteractions } from './remoteInteractions';

export namespace Dashboard {
	export enum State {
		Home = 0,
		RedisInteractions = 1,
		RedisInstances = 2,
		RedisExcludedDomains = 3,
		RedisTwitterEmbed = 4,
	}

	export enum StateConvert {
		Home = 'home',
		RedisInteractions = 'redis_interactions',
		RedisInstances = 'redis_instances',
		RedisExcludedDomains = 'redis_excluded_domains',
		RedisTwitterEmbed = 'redis_twitter_embed',
	}

	export interface Worker {
		uuid: string;
		started_at: string;
		memory: number;
		times: {
			count: number;
			average: number;
			last: number;
		};
		concurrency: number;
	}

	export interface Jobs {
		processed: number;
		failed: number;
	}

	export interface Data {
		jobs: Jobs;
		requests_made: number;
		queue_size: number;
		max_concurrency: number;
		workers: Array<Dashboard.Worker>;
	}

	export interface WebsocketData extends Omit<Dashboard.Data, 'workers'> {
		worker: Worker;
	}

	export interface ChartsData {
		created_at: string;
		merge_type: Dashboard.Stats.WorkerMerge;
		jobs_failed: number;
		jobs_processed: number;
		requests_made: number;
		worker_job_time_average: number;
		worker_job_time_max: number;
		worker_job_time_min: number;
		worker_memory_max: number;
		worker_queue_max: number;
		cache_size_max: number;
		cache_size_dead: number;
	}

	export namespace Stats {
		// prettier-ignore
		export enum WorkerMerge {
			NotMerged = 0,	//  1min between records =>  60 x 24       => 1440 records a day
			Daily = 1,		//  5min between records =>  (60 / 5) x 24 =>  720 records/days
			Weekly = 2		//    1h between records =>         1 x 24 =>   24 records/days
			/**
			 * 24 records/days => 8 760 records/years
			 * 10 years => 87 600 records
			 */
		}

		export interface Account {
			handle: string;
			jobs_count: number;
			status: boolean;
		}

		export interface Socket {
			total: number;
			instances: number;
			frontend: number;
			dashboard: number;
		}

		export interface User {
			main: Dashboard.Stats.Account;
			secondary: Array<Omit<Dashboard.Stats.Account, 'status'>>;
		}

		export interface Users {
			sockets: Dashboard.Stats.Socket;
			users: Array<Dashboard.Stats.User>;
		}
	}

	export namespace Redis {
		export enum State {
			Ok = 'OK',
			Retry = 'RETRY',
			Dead = 'DEAD',
		}

		export interface Data<T> {
			key: string;
			value: T;
			ttl?: number;
			selected: boolean;
			expired: boolean;
			fetched_at: Date;
		}

		export interface ExcludedDomain {
			state: Dashboard.Redis.State.Retry | Dashboard.Redis.State.Dead;
			value: string;
		}

		export interface InstanceCache {
			state: Dashboard.Redis.State.Ok | Dashboard.Redis.State.Dead;
			value: string;
		}

		export interface Instances extends Dashboard.Redis.Data<InstanceCache> {
			ttl: -1;
		}

		export interface ExcludedDomains extends Dashboard.Redis.Data<ExcludedDomain> {
			ttl: number;
		}

		/**
		 * Twitter
		 */
		// #region

		export interface TwitterEmbedOK {
			state: Dashboard.Redis.State.Ok;
			value: string;
		}

		export interface TwitterEmbedDead {
			state: Dashboard.Redis.State.Dead;
			value: string;
		}

		export interface TwitterEmbed {
			key: string;
			value: TwitterEmbedOK | TwitterEmbedDead;
		}

		// #endregion

		/**
		 * Interactions
		 */
		// #region

		export interface InteractionTemplate {
			state: Dashboard.Redis.State;
			value: RemoteInteractions | string;
		}

		export interface InteractionRetry extends InteractionTemplate {
			state: Dashboard.Redis.State.Retry;
			value: string;
		}

		export interface InteractionDead extends InteractionTemplate {
			state: Dashboard.Redis.State.Dead;
			value: string;
		}

		export interface InteractionOK extends InteractionTemplate {
			state: Dashboard.Redis.State.Ok;
			value: RemoteInteractions;
		}

		export type Interaction = InteractionRetry | InteractionDead | InteractionOK;

		export interface Interactions extends Dashboard.Redis.Data<Interaction> {
			ttl: number;
		}

		// #endregion
	}
}
