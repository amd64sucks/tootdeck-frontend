export enum SettingsState {
	Appearance = 0,
	Behavior = 1,
	Instance = 2,
	CustomCSS = 3,
	Sync = 4,
	SoundsLibrary = 5,
	Shortcuts = 6,
	Sessions = 7,
	About = 8,
}
