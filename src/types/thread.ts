import type { Writable } from 'svelte/store';

import type { IndexedArray } from '../lib/IndexedArray';
import type { StandardColumn } from '../lib/Columns/StandardColumn';

import type { ParsedStatus } from './contentParsed';

export interface ThreadLines {
	up: boolean;
	down: boolean;
}

export interface PartialThreadStatus<T> {
	status: T;
	lines: ThreadLines;
}

export interface ThreadStatus extends PartialThreadStatus<ParsedStatus> {
	id: string;
	selected: boolean;
}

export interface ThreadStack {
	id: number;
	loading?: boolean;
	thread: Writable<IndexedArray<ThreadStatus>> | null;
	column_instance: StandardColumn<any>;
}
