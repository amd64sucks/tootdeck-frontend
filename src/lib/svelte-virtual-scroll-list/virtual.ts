/**
 * virtual list core calculating center
 */
export interface VirtualScrollProps {
	slotHeaderSize: number;
	slotFooterSize: number;
	estimateSize: number;
	uniqueIds: Array<any>;
}

export type rangeEventHandler = () => void;

const DIRECTION_TYPE = {
	FRONT: 'FRONT', // scroll up or left
	BEHIND: 'BEHIND', // scroll down or right
};
const CALC_TYPE = {
	INIT: 'INIT',
	FIXED: 'FIXED',
	DYNAMIC: 'DYNAMIC',
};

export default class {
	private param: VirtualScrollProps;
	private lastCalcIndex: number = 0;
	private fixedSizeValue: number = 0;
	private calcType: string = CALC_TYPE.INIT;
	private direction: string = '';
	private offset: number = 0;
	sizes: Map<number, number> = new Map<number, number>();

	constructor(param: VirtualScrollProps) {
		// param data
		this.param = param;

		// size data
		this.sizes = new Map();
	}

	destroy() {
		// param data
		// @ts-ignore
		this.param = null;
		// @ts-ignore
		this.callUpdate = null;

		// size data
		this.sizes = new Map();
		this.fixedSizeValue = 0;
		this.calcType = CALC_TYPE.INIT;

		// scroll data
		this.offset = 0;
		this.direction = '';
	}

	isBehind() {
		return this.direction === DIRECTION_TYPE.BEHIND;
	}

	isFront() {
		return this.direction === DIRECTION_TYPE.FRONT;
	}

	// return start index offset
	getOffset(start: number) {
		return (start < 1 ? 0 : this.getIndexOffset(start)) + this.param.slotHeaderSize;
	}

	updateParam(key: string, value: any) {
		if (this.param && key in this.param) {
			// if uniqueIds change, find out deleted id and remove from size map
			if (key === 'uniqueIds') {
				this.sizes.forEach((v, key) => {
					if (!value.includes(key)) {
						this.sizes.delete(key);
					}
				});
			}

			// @ts-ignore
			this.param[key] = value;
		}
	}

	// save each size map by id
	saveSize(id: string, size: number) {
		this.sizes.set(+id, size);

		// we assume size type is fixed at the beginning and remember first size value
		// if there is no size value different from this at next coming saving
		// we think it's a fixed size list, otherwise is dynamic size list
		if (this.calcType === CALC_TYPE.INIT) {
			this.fixedSizeValue = size;
			this.calcType = CALC_TYPE.FIXED;
		} else if (this.calcType === CALC_TYPE.FIXED && this.fixedSizeValue !== size) {
			this.calcType = CALC_TYPE.DYNAMIC;
			// it's no use at all
			this.fixedSizeValue = 0;
		}
	}

	handleScroll(offset: number) {
		this.direction = offset < this.offset ? DIRECTION_TYPE.FRONT : DIRECTION_TYPE.BEHIND;
		this.offset = offset;
	}

	// ----------- public method end -----------

	// return a scroll offset from given index, can efficiency be improved more here?
	// although the call frequency is very high, its only a superposition of numbers
	getIndexOffset(givenIndex: number) {
		if (!givenIndex) {
			return 0;
		}

		let offset = 0;
		let indexSize = 0;
		for (let index = 0; index < givenIndex; index++) {
			// this.__getIndexOffsetCalls++
			indexSize = this.sizes.get(this.param.uniqueIds[index])!;
			offset = offset + indexSize;
		}

		// remember last calculate index
		this.lastCalcIndex = Math.max(this.lastCalcIndex, givenIndex - 1);
		this.lastCalcIndex = Math.min(this.lastCalcIndex, this.getLastIndex());

		return offset;
	}

	// is fixed size type
	isFixedType() {
		return this.calcType === CALC_TYPE.FIXED;
	}

	// return the real last index
	getLastIndex() {
		return this.param.uniqueIds.length - 1;
	}

	// return end base on start
	getEndByStart() {
		return this.getLastIndex();
	}
}
