import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { StandardColumn } from '../Columns/StandardColumn';
import { ContentManager } from './ContentManager';
import { debugLogger } from '../debugLogger';

import type { ParsedStatus } from '../../types/contentParsed';

type Domain = string;
type URL = string;
type DomainMap = Map<URL, QuoteCache>;
export type QuoteCache = ParsedStatus | 'DEAD';

class QuoteManagerInnner {
	static readonly instance = new QuoteManagerInnner();

	private readonly domains = new Map<Domain, DomainMap>();

	private async fetch(
		that: StandardColumn<any>,
		account: TootDeckAccount,
		url: string
	): Promise<ParsedStatus | undefined> {
		const response = await account.mirrorAPI.Search.statuses(url, {
			limit: 1,
			resolve: true,
		});
		if (!response || !response[0]) {
			return;
		}

		const status = response[0];
		const parsed = ContentManager.Status.store(that, {
			...status,
			animateIn: false,
			animateOut: false,
		});

		return parsed;
	}

	private getID(account: TootDeckAccount, url: string) {
		return url + '@' + account.mirrorAPI.handle_string;
	}

	async get(that: StandardColumn<any>, url: string, in_status_id: string): Promise<QuoteCache> {
		const account = that.account;
		const domain = account.mirrorAPI.domain;
		const id = this.getID(account, url);

		let inner = this.domains.get(domain);
		if (inner) {
			const cache = inner.get(id);
			if (cache) {
				if (cache !== 'DEAD') {
					that.addQuoteOwership(cache.id);
				}
				debugLogger('QuoteManager: get: ' + id + ' found in cache');

				return cache;
			}
		}

		let status: ParsedStatus | 'DEAD' | undefined = ContentManager.Status.find(
			account.mirrorAPI.handle,
			url
		);
		if (!status) {
			status = (await this.fetch(that, account, url)) ?? 'DEAD';
		}

		if (status !== 'DEAD') {
			if (!status.tootdeck.in_status.find((x) => x.id === in_status_id)) {
				status.tootdeck.in_status.push({
					id: in_status_id,
					quote: true,
				});
			}
			that.addQuoteOwership(status.id);
		}

		if (!inner) {
			inner = new Map<URL, QuoteCache>();
			this.domains.set(domain, inner);
		}

		inner.set(id, status);
		debugLogger(
			'QuoteManager: get: ' +
				id +
				' set in cache, ' +
				(typeof status !== 'string' ? status.id : status)
		);

		return status;
	}

	getSync(that: StandardColumn<any>, url: string): QuoteCache | undefined {
		const account = that.account;
		const domain = account.mirrorAPI.domain;
		const id = this.getID(account, url);

		let inner = this.domains.get(domain);
		if (inner) {
			const cache = inner.get(id);
			if (cache) {
				if (cache !== 'DEAD') {
					that.addQuoteOwership(cache.id);
				}

				debugLogger('QuoteManager: getSync: ' + id + ' found in cache');
				return cache;
			}
		}

		return undefined;
	}

	store(that: StandardColumn<any>, url: string, in_status_id: string, dead: 'DEAD'): 'DEAD';
	store(
		that: StandardColumn<any>,
		url: string,
		in_status_id: string,
		status: ParsedStatus
	): ParsedStatus;
	store(
		that: StandardColumn<any>,
		url: string,
		in_status_id: string,
		status_or_dead: ParsedStatus | 'DEAD'
	): QuoteCache;
	store(
		that: StandardColumn<any>,
		url: string,
		in_status_id: string,
		status_or_dead: ParsedStatus | 'DEAD'
	): QuoteCache {
		const account = that.account;
		const domain = account.mirrorAPI.domain;
		const id = this.getID(account, url);

		let inner = this.domains.get(domain);
		if (inner) {
			const cache = inner.get(id);
			if (cache) {
				if (cache !== 'DEAD') {
					that.addQuoteOwership(cache.id);
				}
				debugLogger('QuoteManager: get: ' + id + ' found in cache');

				return cache;
			}
		}

		if (!inner) {
			inner = new Map<URL, QuoteCache>();
			this.domains.set(domain, inner);
		}

		const status = status_or_dead;
		if (status === 'DEAD') {
			inner.set(id, 'DEAD');
		} else {
			if (!status.tootdeck.in_status.find((x) => x.id === in_status_id)) {
				status.tootdeck.in_status.push({
					id: in_status_id,
					quote: true,
				});
			}
			that.addQuoteOwership(status.id);

			inner.set(id, status);
		}

		debugLogger(
			'QuoteManager: store: ' +
				id +
				' set in cache, ' +
				(typeof status !== 'string' ? status.id : status)
		);

		return status;
	}

	delete(that: StandardColumn<any>, status_id: string): void {
		that.removeQuote(status_id);

		const domain = that.account.mirrorAPI.domain;
		const inner = this.domains.get(domain);
		if (!inner) {
			return;
		}

		const cache = Array.from(inner).find(
			([_, value]) => typeof value !== 'string' && value.id === status_id
		);
		if (!cache) {
			return;
		}

		const [id, status] = cache;
		if (typeof status !== 'string') {
			status.tootdeck.in_status.forEach((link) => {
				ContentManager.Status.forceUpdate(that.account.mirrorAPI.handle, link.id);
			});
		}

		inner.set(id, 'DEAD');
		debugLogger('QuoteManager: delete: ' + id + ' removed');
	}
}

export const QuoteManager = QuoteManagerInnner.instance;
