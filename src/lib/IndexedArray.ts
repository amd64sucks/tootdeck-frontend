type Item<T> = T & { id: string };

export class IndexedArray<T> {
	private readonly array: Array<Item<T>> = [];
	private readonly indexes: Array<string> = [];

	constructor(...items: Array<Item<T>>) {
		items.forEach((item) => {
			this.pushBottom(item);
		});
	}

	clear() {
		this.array.splice(0, this.array.length);
		this.indexes.splice(0, this.indexes.length);
	}

	delete(key: string, index?: number) {
		if (index === undefined) {
			index = this.findIndex(key);
		}

		if (index === -1) {
			return;
		}

		this.array.splice(index, 1);
		this.indexes.splice(index, 1);
	}

	deleteFirst() {
		const value = this.array.splice(0, 1);
		this.indexes.splice(0, 1);
		return value[0];
	}

	deleteLast() {
		const value = this.array.splice(this.array.length - 1, 1);
		this.indexes.splice(this.array.length - 1, 1);
		return value[0];
	}

	findIndex(key: string) {
		return this.indexes.findIndex((x) => x === key);
	}

	getFromIndex(key: string, index?: number) {
		if (index === undefined) {
			index = this.findIndex(key);
		}

		return this.array[index];
	}

	get() {
		return this.array;
	}

	has(key: string) {
		return this.findIndex(key) !== -1;
	}

	get length() {
		return this.indexes.length;
	}

	first() {
		return this.array[0];
	}

	last() {
		return this.array.at(-1);
	}

	private pushTop(item: Item<T>) {
		this.array.unshift(item);
		this.indexes.unshift(item.id);
	}

	private pushBottom(item: Item<T>) {
		this.array.push(item);
		this.indexes.push(item.id);
	}

	/**
	 * Create entry at the top if not exist
	 *
	 * Update entry otherwise
	 *
	 * Returns true if an entry is created
	 */
	update(item: Item<T>, index: number) {
		if (this.array[index]?.id === item.id) {
			this.array[index] = item;
			return false;
		}

		this.setTop(item);
	}

	/**
	 * Create entry at specified index if not exist
	 *
	 * Update entry otherwise
	 *
	 * Returns true if an entry is created
	 */
	setAt(item: Item<T>, index: number): boolean {
		const id = item.id;

		if (this.array[index]?.id === item.id) {
			this.array[index] = item;
			return false;
		}

		this.array.splice(index, 0, item);
		this.indexes.splice(index, 0, id);
		return true;
	}

	/**
	 * Create entry at the top if not exist
	 *
	 * Update entry otherwise
	 *
	 * Returns true if an entry is created
	 */
	setTop(item: Item<T>, index?: number): boolean {
		const id = item.id;

		if (index === undefined) {
			index = this.findIndex(id);
		}

		if (index === -1) {
			this.pushTop(item);
			return true;
		}

		this.array[index] = item;
		return false;
	}

	/**
	 * Create entry at the bottom if not exist
	 *
	 * Update entry otherwise
	 *
	 * Returns true if an entry is created
	 */
	setBottom(item: Item<T>, index?: number): boolean {
		const id = item.id;

		if (index === undefined) {
			index = this.findIndex(id);
		}

		if (index === -1) {
			this.pushBottom(item);
			return true;
		}

		this.array[index] = item;
		return false;
	}

	slice(start?: number, end?: number): Array<T> {
		return this.array.slice(start, end);
	}

	forEach(callbackfn: (value: T, index: number, array: Array<T>) => void) {
		this.array.forEach(callbackfn);
	}

	map<U>(callbackfn: (value: T, index: number, array: Array<T>) => U): Array<U> {
		return this.array.map(callbackfn);
	}

	filter(predicate: (value: string) => boolean): Array<Item<T>> {
		const filtered: Array<Item<T>> = [];
		this.indexes.filter((v, i) => (predicate(v) ? filtered.push(this.array[i]) : -1));
		return filtered;
	}

	[Symbol.iterator]() {
		return this.array.values();
	}
}
