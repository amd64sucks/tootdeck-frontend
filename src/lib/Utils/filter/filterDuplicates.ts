export function filterDuplicate<T extends Array<Record<string, any>>>(array: T, property: string) {
	return array.filter(
		(element, index) => array.findIndex((x) => x[property] === element[property]) === index
	) as T;
}

export function filterDuplicateStringOnly<T extends Array<string>>(array: T) {
	return Array.from(new Set(array));
}
