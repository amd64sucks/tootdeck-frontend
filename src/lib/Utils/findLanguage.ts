import { languages } from '../../assets/languages';

export function findLanguage(lang: string) {
	const first_half = lang.split('-')[0];
	return languages.find((x) => x.short === first_half);
}
