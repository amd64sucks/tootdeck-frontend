import type VirtualScroll from '../svelte-virtual-scroll-list/VirtualScroll.svelte';

/**
 * Modified from https://github.com/mastodon/mastodon/blob/51b83ed19536b06ce3f57b260400ecec2d1dd187/app/javascript/mastodon/scroll.ts
 * This file is licensed under AGPLv3
 */
const easingOutQuint = (x: number, t: number, b: number, c: number, d: number) =>
	c * ((t = t / d - 1) * t * t * t * t + 1) + b;

function scroll(node: Element, key: 'scrollTop' | 'scrollLeft', target: number, duration: number) {
	const startTime = Date.now();
	const offset = node[key];
	const gap = target - offset;
	let interrupt = false;

	const step = () => {
		const elapsed = Date.now() - startTime;
		const percentage = elapsed / duration;

		if (percentage > 1 || interrupt) {
			return;
		}

		// console.log(offset, gap, easingOutQuint(0, elapsed, offset, gap, duration));

		node[key] = easingOutQuint(0, elapsed, offset, gap, duration);
		requestAnimationFrame(step);
	};

	step();

	return () => {
		interrupt = true;
	};
}

export const scrollRight = (node: Element, position: number, duration: number = 1000) =>
	scroll(node, 'scrollLeft', position, duration);

export const scrollTop = (node: Element, top: number = 0, duration: number = 1000) =>
	scroll(node, 'scrollTop', top, duration);

export function scrollTopVirtualList(
	virtual_list: VirtualScroll,
	top: number = 0,
	duration: number = 1000
) {
	const startTime = Date.now();
	const offset = virtual_list.getOffset();
	const gap = top - offset;
	let interrupt = false;

	const step = () => {
		const elapsed = Date.now() - startTime;
		const percentage = elapsed / duration;

		if (percentage > 1 || interrupt) {
			return;
		}

		virtual_list.scrollToOffset(easingOutQuint(0, elapsed, offset, gap, duration));
		requestAnimationFrame(step);
	};

	step();

	return () => {
		interrupt = true;
	};
}
