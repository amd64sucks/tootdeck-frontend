import { MacOS } from './OS';

export function ctrlKey(e: KeyboardEvent | MouseEvent) {
	return (!MacOS && e.ctrlKey) || (MacOS && e.metaKey);
}
