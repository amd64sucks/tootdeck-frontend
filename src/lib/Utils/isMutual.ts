import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { getHandleFromAccount } from './handle/getHandleFromAccount';

import type { ParsedAccount } from '../../types/contentParsed';

export function isMutual(account: TootDeckAccount, status_account: ParsedAccount) {
	const relationship =
		account.relationship.get(status_account.id) ||
		account.relationship.get(getHandleFromAccount(status_account, true));
	return (relationship?.followed_by && relationship?.following) ?? false;
}
