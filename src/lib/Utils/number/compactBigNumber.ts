function toFixed(value: number, toFixed: number | undefined): string {
	const str = value.toString();

	return str.indexOf('.') === 1 ? value.toFixed(1) : value.toFixed(toFixed);
}

export function compactBigNumber(
	n: number,
	args: { space_suffix?: boolean; fractionDigits?: number } = {
		space_suffix: false,
		fractionDigits: 1,
	}
): string {
	const { space_suffix, fractionDigits } = args;

	if (n < 1000) {
		return n.toString();
	}

	if (n >= 1000000000) {
		return toFixed(n / 1000000000, fractionDigits) + (space_suffix ? ' B' : 'B');
	}

	if (n >= 1000000) {
		return toFixed(n / 1000000, fractionDigits) + (space_suffix ? ' M' : 'M');
	}

	if (n >= 1000) {
		return toFixed(n / 1000, fractionDigits) + (space_suffix ? ' K' : 'K');
	}

	return n.toString();
}
