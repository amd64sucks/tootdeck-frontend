export function addThousandSeparator(nbr: number, separator: string = ' ') {
	const chars = nbr.toString().split('').reverse();
	let result = '';
	for (let i = 0; i < chars.length; i++) {
		if (i && i % 3 === 0) {
			result += separator;
		}
		result += chars[i];
	}
	return result.split('').reverse().join('');
}
