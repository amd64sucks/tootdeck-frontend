export const isCheckboxChecked = (e: Event) => (e.target as HTMLInputElement).checked;
