export function updateObject(obj: Record<string, any>, new_obj: Record<string, any>) {
	Object.keys(new_obj).forEach((key) => {
		obj[key] = new_obj[key];
	});
}
