export const isScrollbarVisible = (element: HTMLElement) =>
	element.scrollHeight > element.clientHeight;
