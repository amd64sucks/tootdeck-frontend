export function removeFediPrefixFromHandle(items: Array<string>, handle: string): string {
	// const handle_reg = /(?<=.+@)((?:mstdn|fedi|social|akko(?:ma)*|misskey|mk|sharkey|shork)\.)(?=.+(?:\..+)+)/g;
	const remove_reg = items.join('|');
	const handle_reg = new RegExp(`(?<=.+@)((?:${remove_reg})\\.)(?=.+(?:\\..+)+)`, 'g');

	return handle.replaceAll(handle_reg, '');
}
