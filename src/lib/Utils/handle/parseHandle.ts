import { isURL } from '../url/isURL';

export function parseHandle(handle: string): {
	username: string;
	domain: string;
} | null {
	if (!handle) {
		return null;
	}

	if ((handle.match(/@/g) || []).length > 2) {
		return null;
	}

	const parse = handle
		.split('@')
		.map((x) => x.trim())
		.filter((x) => x);

	if (parse.length !== 2) {
		return null;
	}

	let raw_domain = parse[1]!;

	if (raw_domain.search('http://') !== -1 || raw_domain.search('https://') !== -1) {
		return null;
	}

	if (!isURL(raw_domain, true)) {
		return null;
	}

	if (raw_domain.split('/').length !== 1) {
		return null;
	}

	return { username: parse[0]!, domain: raw_domain };
}
