let result: '12' | '24' | undefined = undefined;

export function is12hor24h() {
	if (result) {
		return result;
	}

	const test = !!new Date()
		.toLocaleString()
		.split(' ')
		.filter((x) => x === 'AM' || x === 'PM').length;

	if (test) {
		result = '12';
	} else {
		result = '24';
	}
	return result;
}

export const is12h = () => is12hor24h() === '12';
export const is24h = () => is12hor24h() === '24';
