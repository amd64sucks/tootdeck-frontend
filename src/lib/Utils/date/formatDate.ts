import { format } from 'date-fns';

import { is12h } from './is12hor24h';

function formatTimeDiff(created_at: string, relative_time: number) {
	let t = {
		ss: Math.floor(relative_time % 60),
		mm: Math.floor((relative_time / 60) % 60),
		hh: Math.floor((relative_time / 60 / 60) % 24),
		d: Math.floor(relative_time / 60 / 60 / 24),
	};

	if (relative_time >= -10 && relative_time <= 0) {
		return 'just now';
	}

	if (relative_time < 0) {
		return format(new Date(created_at), 'MMM dd yyyy');
	}

	if (t.d > 7) {
		const f = format(new Date(created_at), 'MMM dd yyyy');
		const year = new Date().getFullYear();
		return f.replace(year.toString(), '');
	}

	if (t.d) {
		return t.d + 'd';
	}

	if (t.hh) {
		return t.hh + 'h';
	}

	if (t.mm) {
		return t.mm + 'm';
	}

	if (t.ss) {
		if (t.ss < 10) {
			return 'just now';
		} else {
			return t.ss + 's';
		}
	}

	return format(new Date(created_at), 'MMM dd yyyy');
}

export function formatDate(created_at: string) {
	const offset = (new Date().valueOf() - new Date(created_at).valueOf()) / 1000;
	const relative = new Date(offset).valueOf();

	return formatTimeDiff(created_at, relative);
}

/**
 *
 * @param order `big` => dd MMM yyyy at hh:mm
 * @param order `little` => hh:mm · dd MMM yyyy
 */
export function formatAbsoluteDate(created_at: string | Date, order: 'big' | 'little') {
	const date = typeof created_at === 'string' ? new Date(created_at) : created_at;

	if (order === 'big') {
		return is12h()
			? format(date, 'dd MMM yyyy · hh:mm a').replace('·', 'at')
			: format(date, 'dd MMM yyyy · hh:mm').replace('·', 'at');
	}

	return is12h()
		? format(new Date(date), 'hh:mm a · dd MMM yyyy')
		: format(new Date(date), 'HH:mm · dd MMM yyyy');
}
