import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { ContentManager } from '../Managers/ContentManager';
import { getDomain } from './url/getDomain';

import type { ParsedAccount, ParsedStatus } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

function checkDomain(account: TootDeckAccount, status_account: ParsedAccount | Entity.Account) {
	return (
		!status_account.acct.includes('@') &&
		account.mirrorAPI.domain === getDomain(status_account.url)
	);
}

export function isSameDomain(
	account: TootDeckAccount,
	status_or_account?: ParsedStatus | Entity.Status | ParsedAccount | Entity.Account
) {
	if (!status_or_account) {
		return false;
	}

	const status = status_or_account as ParsedStatus | Entity.Status;
	if (status?.account) {
		if (checkDomain(account, status.account)) {
			return true;
		}

		return !!ContentManager.Status.find(account.mirrorAPI.handle, status.url);
	}

	const status_account = status_or_account as ParsedAccount | Entity.Account;
	return checkDomain(account, status_account);
}
