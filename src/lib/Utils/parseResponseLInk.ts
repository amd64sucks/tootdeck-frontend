import type { Entity } from '../../types/mastodonEntities';

interface Link {
	min_id?: string;
	max_id?: string;
	since_id?: string;
}

export interface Links {
	prev?: Link;
	next?: Link;
}

export type ResponseLinks<T> = { data: T; links: Links | 'NONE' };

export function parseResponseLink<T>(response: Entity.Response<T>): Links | 'NONE' {
	const link = response.headers?.link as string;
	if (!link) {
		return 'NONE';
	}

	const link_split = link.split(',');

	const response_link: { [key: string]: Link } = {};

	for (const raw of link_split) {
		const raw_split = raw.split(';');

		for (let i = 0; i < raw_split.length; i++) {
			const obj_name = raw_split[i + 1].match('(?:")(.+)(?:")')?.[1];
			if (!obj_name) {
				return {};
			}
			const link_url = raw_split[i].replace('<', '').replace('>', '');
			const url = new URL(link_url);

			response_link[obj_name] = {
				min_id: url.searchParams.get('min_id') ?? undefined,
				max_id: url.searchParams.get('max_id') ?? undefined,
				since_id: url.searchParams.get('since_id') ?? undefined,
			};

			i++;
		}
	}

	return response_link;
}
