export function truncateLink(href: string, max_size: number) {
	let trunc_anchor_text = href;
	let truncated = false;
	try {
		const url = new URL(href!);
		const remove_www = url.hostname.replace('www.', '');
		if (url.pathname.replace('/', '').length === 0) {
			trunc_anchor_text = remove_www;
		} else {
			const link = remove_www + url.pathname + url.search;
			trunc_anchor_text = link.substring(0, max_size);
			truncated = link.length > max_size;
		}
	} catch (e) {}

	return { trunc_anchor_text, truncated };
}
