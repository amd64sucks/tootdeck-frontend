import { notificationModal, NotificationModalType } from '../stores/NotificationsModal';

export function queryMessage() {
	const query = new URLSearchParams(window.location.search);
	const message = query.get('message');
	if (message) {
		let type: NotificationModalType = NotificationModalType.Error;
		switch (+(query.get('message_type') || 'NaN')) {
			case NotificationModalType.Error:
				type = NotificationModalType.Error;
				break;
			case NotificationModalType.Warn:
				type = NotificationModalType.Warn;
				break;
			case NotificationModalType.Info:
				type = NotificationModalType.Info;
				break;
		}

		window.history.replaceState({}, '', '/');

		notificationModal.create(NotificationModalType.Error, message.replaceAll('"', ''), true);
	}
}
