export enum TootDeckEvents {
	/**
	 * Status update
	 */
	Status = 'TootDeck:Status',
	/**
	 * Status in the federated timeline
	 */
	FederatedTimeline = 'TootDeck:FederatedTimeline',
	/**
	 * Status in the local timeline
	 */
	LocalTimeline = 'TootDeck:LocalTimeline',
	/**
	 * Status for home column initialization
	 */
	ColumnHome = 'TootDeck:Home',
	/**
	 * Notification
	 */
	Notification = 'TootDeck:Notification',
	/**
	 * @deprecated Unused for now
	 */
	Message = 'TootDeck:Message',
	/**
	 * Status in a list timeline
	 */
	List = 'TootDeck:List',
	/**
	 * Status in a hashtag timeline
	 */
	Hashtag = 'TootDeck:Hashtag',
	/**
	 * Status for an hashtag column initialization
	 */
	HashtagColumn = 'TootDeck:ColumnHashtag',
	/**
	 * Status from user channel in websocket
	 */
	User = 'TootDeck:User',
	/**
	 * Status for user column initialization
	 */
	ColumnUser = 'TootDeck:ColumnUser',
	/**
	 * Status for favorites column initialization and when a status is favourited/unfavourited
	 */
	Favourites = 'TootDeck:Favourites',
	/**
	 * Status for bookmarks column initialization and when a status is bookmarked/unbookmarked
	 */
	Bookmarks = 'TootDeck:Bookmarks',
	/**
	 * Status for user profile
	 */
	Profile = 'TootDeck:Profile',
}
