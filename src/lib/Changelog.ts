import { formatQuery, getBody } from './APIs/fetchAPI';
import {
	GITLAB_REPO_BACKEND,
	GITLAB_REPO_FRONTEND,
	GITLAB_REPO_GLOBAL,
} from './Constants/Repositories';
import { filterDuplicate } from './Utils/filter/filterDuplicates';

/**
 * Commits
 */
// #region

type URLCommit = `${string}/repository/commits`;

interface CommitStats {
	additions: number;
	deletions: number;
}

interface CommitResponse {
	id: string;
	short_id: string;
	created_at: string;
	parent_ids: Array<string>;
	title: string;
	message: string;
	author_name: string;
	author_email: string;
	authored_date: string;
	committer_name: string;
	committer_email: string;
	committed_date: string;
	trailers: {};
	extended_trailers: {};
	web_url: string;
	stats?: CommitStats;
}

interface CommitResponseWithStats extends CommitResponse {
	stats: CommitStats;
}

interface CommitQuery {
	/**
	 * The name of a repository branch, tag, or revision range, or the default branch if not provided
	 */
	ref_name?: string;
	/**
	 * Only commits after or on this date are returned (ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ)
	 */
	since?: string;
	/**
	 * Only commits before or on this date are returned (ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ)
	 */
	until?: string;
	/**
	 * The file path to filter commits
	 */
	path?: string;
	/**
	 * Search commits by commit author
	 */
	author?: string;
	/**
	 * Retrieve every commit from the repository
	 */
	all?: boolean;
	/**
	 * Include stats about each commit in the response
	 */
	with_stats?: boolean;
	/**
	 * Follow only the first parent commit when encountering a merge commit
	 */
	first_parent?: boolean;
	/**
	 * Order of commits (default is 'default', reverse chronological order)
	 *
	 * Git topo documentation https://git-scm.com/docs/git-log#Documentation/git-log.txt---topo-order
	 */
	order?: 'default' | 'topo';
	/**
	 * Parse and include Git trailers for every commit
	 *
	 * Git trailers documentation https://git-scm.com/docs/git-interpret-trailers
	 */
	trailers?: boolean;
}

interface CommitShortInfo {
	commit: string;
	created_at: string;
}

interface CommitStorage {
	global?: CommitShortInfo;
	backend?: CommitShortInfo;
	frontend?: CommitShortInfo;
}

interface ParsedChunk {
	text: string;
	url: string | null;
}

type ParsedContent = string | Array<ParsedChunk>;

export interface ParsedCommit {
	id: string;
	created_at: string;
	content: Array<ParsedContent>;
	url: string;
}

interface CommitScroll {
	found_commit: boolean;
	commits: Array<CommitResponse>;
}

export interface ParsedCommitScroll {
	found_commit: boolean;
	commits: Array<ParsedCommit>;
}

export interface ParsedChangelog {
	global?: ParsedCommitScroll;
	backend?: ParsedCommitScroll;
	frontend?: ParsedCommitScroll;
}

// #endregion

/**
 * Release
 */
// #region

type URLRelease = `${string}/releases`;

interface ReleasesQuery {
	/**
	 * The field to use as order. Either released_at (default) or created_at.
	 */
	order_by?: string;
	/**
	 * The direction of the order. Either desc (default) for descending order or asc for ascending order.
	 */
	sort?: string;
	/**
	 * If true, a response includes HTML rendered Markdown of the release description.
	 */
	include_html_description?: boolean;
}

interface ReleaseQuery {
	/**
	 * If true, a response includes HTML rendered Markdown of the release description.
	 */
	include_html_description?: boolean;
}

interface ReleaseReponse {
	name: string;
	tag_name: string;
	description: string;
	description_html?: string;
	created_at: string;
	released_at: string;
	upcoming_release: boolean;
	author: {
		id: number;
		username: string;
		name: string;
		state: string;
		locked: false;
		avatar_url: string;
		web_url: string;
	};
	commit: CommitResponse;
	commit_path: string;
	tag_path: string;
	assets: {
		count: number;
		sources: Array<{
			format: string;
			url: string;
		}>;
		links: Array<string>;
	};
	evidences: Array<{
		sha: string;
		filepath: string;
		collected_at: string;
	}>;
	_links: {
		closed_issues_url: string;
		closed_merge_requests_url: string;
		merged_merge_requests_url: string;
		opened_issues_url: string;
		opened_merge_requests_url: string;
		self: string;
	};
}

interface ReleaseReponseWithHTML extends ReleaseReponse {
	description_html: string;
}

// #endregion

class ChangelogInner {
	static readonly instance = new ChangelogInner();
	private readonly urls = {
		api: 'https://gitlab.com/api/v4/projects/',
		global: this.getRepositoryString(GITLAB_REPO_GLOBAL),
		backend: this.getRepositoryString(GITLAB_REPO_BACKEND),
		frontend: this.getRepositoryString(GITLAB_REPO_FRONTEND),
	};
	private readonly _storage_key: string = 'commit';
	private readonly _commits_suffix = '/repository/commits';
	private readonly _releases_suffix = '/releases';
	private readonly _local_state: {
		nightly?: ParsedChangelog;
		release?: string;
	} = {};

	private getRepositoryString(url: string) {
		if (!url) {
			return '';
		}

		const pathname = new URL(url).pathname;
		const [_, project, repository] = pathname.split('/');

		return encodeURIComponent(project + '/' + repository);
	}

	private getGitlabURL(repository: string, suffix: string): string {
		return `${this.urls.api}${repository}${suffix}`;
	}

	private getGlobalURL(release: true): URLRelease;
	private getGlobalURL(release: false): URLCommit;
	private getGlobalURL(release: boolean): string {
		return this.getGitlabURL(
			this.urls.global,
			release ? this._releases_suffix : this._commits_suffix
		);
	}

	private getBackendURL(_: false): URLCommit {
		return this.getGitlabURL(this.urls.backend, this._commits_suffix) as URLCommit;
	}

	private getFrontendURL(_: false): URLCommit {
		return this.getGitlabURL(this.urls.frontend, this._commits_suffix) as URLCommit;
	}

	private async fetch<T>(url: string, query_options?: Object): Promise<T | null> {
		const query = formatQuery(query_options);

		return fetch(url + query, {
			method: 'GET',
		}).then(async (res) => {
			if (res.status !== 200) {
				return null;
			}

			const { is_json, body } = await getBody(res);
			if (!is_json) {
				return null;
			}

			return body;
		});
	}

	/**
	 * Commits
	 */
	// #region

	/**
	 * Fetchers
	 */
	// #region

	private async fetchRepositoryCommits(
		url: URLCommit,
		query_options: CommitQuery & { with_stats: true }
	): Promise<Array<CommitResponseWithStats> | null>;
	private async fetchRepositoryCommits(
		url: URLCommit,
		query_options?: CommitQuery
	): Promise<Array<CommitResponse> | null>;
	private async fetchRepositoryCommits(
		url: URLCommit,
		query_options?: CommitQuery
	): Promise<Array<CommitResponse> | null> {
		return this.fetch<Array<CommitResponse>>(url, query_options);
	}

	private async fetchAllCommits(query_options?: {
		global?: CommitQuery;
		backend?: CommitQuery;
		frontend?: CommitQuery;
	}) {
		return Promise.all([
			this.fetchRepositoryCommits(this.getGlobalURL(false), query_options?.global),
			this.fetchRepositoryCommits(this.getBackendURL(false), query_options?.backend),
			this.fetchRepositoryCommits(this.getFrontendURL(false), query_options?.frontend),
		]);
	}

	private async scrollCommits(
		url: URLCommit,
		target_commit: string,
		created_at: string,
		commits: Array<CommitResponse> = []
	): Promise<CommitScroll> {
		const response = await this.fetchRepositoryCommits(url, {
			until: commits.length !== 0 ? created_at : undefined,
		});
		if (!response || !response.length) {
			return { found_commit: false, commits: commits.slice(0, 1) };
		}

		const last_created_at = response.at(-1)!.created_at;

		commits.push(...response);

		const index = commits.findIndex(({ id }) => id === target_commit);
		if (index !== -1) {
			return { found_commit: true, commits: commits.slice(0, index) };
		}
		/**
		 * Abort if the target commit isn't found after 4 requests (80 commits)
		 */
		if (commits.length >= 80) {
			return { found_commit: false, commits: commits.slice(0, 1) };
		}

		return this.scrollCommits(url, target_commit, last_created_at, commits);
	}

	// #endregion

	/**
	 * LocalStorage
	 */
	// #region

	private getLocal(): CommitStorage | null {
		try {
			return JSON.parse(window.localStorage.getItem(this._storage_key)!);
		} catch (_) {
			return null;
		}
	}

	private saveLocal(value: CommitStorage) {
		window.localStorage.setItem(this._storage_key, JSON.stringify(value));
	}

	private async initLocal(): Promise<
		[CommitResponse | null, CommitResponse | null, CommitResponse | null]
	> {
		const [global, backend, frontend] = await this.fetchAllCommits();
		const local: CommitStorage = {};
		if (global) {
			local.global = {
				commit: global[0].id,
				created_at: global[0].created_at,
			};
		}
		if (backend) {
			local.backend = {
				commit: backend[0].id,
				created_at: backend[0].created_at,
			};
		}
		if (frontend) {
			local.frontend = {
				commit: frontend[0].id,
				created_at: frontend[0].created_at,
			};
		}

		this.saveLocal(local);

		return [global?.[0] ?? null, backend?.[0] ?? null, frontend?.[0] ?? null];
	}

	// #endregion

	private parseResponse(res: CommitResponse): ParsedCommit {
		const message = res.message.split('\n');
		const content: Array<ParsedContent> = [];

		const mergeRequestURL = (repository: string) =>
			`https://gitlab.com/${repository}/-/merge_requests/`;
		const issueURL = (repository: string) => `https://gitlab.com/${repository}/-/issues/`;
		/**
		 * Gitlab isseue (#number)
		 */
		const match_issue = '#\\d+';
		/**
		 *  Gitlab merge request (!number)
		 */
		const match_merge_request = '!\\d+';
		/**
		 * Match tootdeck/tootdeck-frontend
		 */
		const match_sub_repo = '\\w+\\/\\w+-\\w+';
		/**
		 * Match tootdeck/tootdeck
		 */
		const match_global_repo = '\\w+\\/\\w+';
		/**
		 * Search in string: repository + `to_match` in middle or at the end of the string
		 *
		 * @param to_match regex to match
		 */
		const regexStringContain = (to_match: string) =>
			`(${match_sub_repo}|${match_global_repo})(${to_match})(?: +|$)`;
		const regex = new RegExp(regexStringContain(match_issue + '|' + match_merge_request), 'g');

		let found: boolean = false;
		for (const text of message) {
			found = false;
			for (const [_, repo, match] of text.matchAll(regex)) {
				found = true;
				const id = match.substring(1);
				const exclude = text.split(repo).map((chunk) => {
					const ret: ParsedChunk = {
						text: chunk,
						url: null,
					};

					if (chunk.includes('#')) {
						ret.url = issueURL(repo) + id;
					} else if (chunk.includes('!')) {
						ret.url = mergeRequestURL(repo) + id;
					}

					return ret;
				});
				content.push(exclude);
			}

			if (!found) {
				content.push(text);
			}
		}

		return {
			id: res.id,
			created_at: res.created_at,
			content,
			url: res.web_url,
		};
	}

	async getNightly(): Promise<ParsedChangelog> {
		let local = this.getLocal();
		if (!local) {
			const [global, backend, frontend] = await this.initLocal();
			const ret = {
				global: global
					? { found_commit: true, commits: [this.parseResponse(global)] }
					: undefined,
				backend: backend
					? { found_commit: true, commits: [this.parseResponse(backend)] }
					: undefined,
				frontend: frontend
					? { found_commit: true, commits: [this.parseResponse(frontend)] }
					: undefined,
			};

			this._local_state.nightly = ret;

			return ret;
		}

		const promises: [
			Promise<ParsedCommitScroll> | undefined,
			Promise<ParsedCommitScroll> | undefined,
			Promise<ParsedCommitScroll> | undefined,
		] = [undefined, undefined, undefined];

		const parse = ({ found_commit, commits }: CommitScroll) => ({
			found_commit,
			commits: filterDuplicate(
				commits.map((commit) => this.parseResponse(commit)),
				'id'
			),
		});
		if (local.global) {
			promises[0] = this.scrollCommits(
				this.getGlobalURL(false),
				local.global.commit,
				local.global.created_at
			).then(parse.bind(this));
		}
		if (local.backend) {
			promises[1] = this.scrollCommits(
				this.getBackendURL(false),
				local.backend.commit,
				local.backend.created_at
			).then(parse.bind(this));
		}
		if (local.frontend) {
			promises[2] = this.scrollCommits(
				this.getFrontendURL(false),
				local.frontend.commit,
				local.frontend.created_at
			).then(parse.bind(this));
		}

		const [global, backend, frontend] = await Promise.all(promises);
		const ret = {
			global,
			backend,
			frontend,
		};

		this._local_state.nightly = ret;

		return ret;
	}

	// #endregion

	/**
	 * Releases
	 */
	// #region

	/**
	 * Fetchers
	 */
	// #region

	private async fetchRepositoryReleases(
		query_options?: ReleasesQuery & { include_html_description: true }
	): Promise<Array<ReleaseReponseWithHTML> | null>;
	private async fetchRepositoryReleases(
		query_options?: ReleasesQuery
	): Promise<Array<ReleaseReponse> | null>;
	private async fetchRepositoryReleases(
		query_options?: ReleasesQuery
	): Promise<Array<ReleaseReponse> | null> {
		return this.fetch<Array<ReleaseReponse>>(this.getGlobalURL(true), query_options);
	}

	private async fetchRepositoryRelease(
		tag: string,
		query_options?: ReleaseQuery & { include_html_description: true }
	): Promise<ReleaseReponseWithHTML | null>;
	private async fetchRepositoryRelease(
		tag: string,
		query_options?: ReleaseQuery
	): Promise<ReleaseReponse | null>;
	private async fetchRepositoryRelease(
		tag: string,
		query_options?: ReleaseQuery
	): Promise<ReleaseReponse | null> {
		return this.fetch<ReleaseReponse>(this.getGlobalURL(true) + '/' + tag, query_options);
	}

	// #endregion

	async getRelease(tag: string) {
		const description = await this.fetchRepositoryRelease(tag).then((res) => {
			if (!res) {
				return '';
			}

			const changelog = 'Changelog\n\n';
			const index = res.description.indexOf(changelog);
			if (index !== -1) {
				return res.description.slice(index + changelog.length);
			}

			return res.description;
		});

		this._local_state.release = description;

		return description;
	}

	// #endregion

	get local_state() {
		return this._local_state;
	}
}

export const Changelog = ChangelogInner.instance;
