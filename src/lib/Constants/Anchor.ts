export const ANCHOR_ARGS: {
	target: string;
	referrerpolicy: ReferrerPolicy;
	rel: string;
} = {
	target: '_blank',
	referrerpolicy: 'no-referrer',
	rel: 'nofollow noopener noreferrer',
};
