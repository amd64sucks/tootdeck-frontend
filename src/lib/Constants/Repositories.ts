import { envParser } from '../envParser';

export const TOOTDECK_REPOSITORY = 'https://gitlab.com/tootdeck/tootdeck';
export const GITLAB_REPO_GLOBAL: string = envParser(import.meta.env.VITE_GITLAB_REPO_GLOBAL);
export const GITLAB_REPO_BACKEND: string = envParser(import.meta.env.VITE_GITLAB_REPO_BACKEND);
export const GITLAB_REPO_FRONTEND: string = envParser(import.meta.env.VITE_GITLAB_REPO_FRONTEND);
