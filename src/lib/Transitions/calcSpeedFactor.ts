import { get } from 'svelte/store';

import { rdOptionsAppearanceAnimationSpeedFactor } from '../../stores/Options';

export const calcSpeedFactor = (nbr: number) => nbr * get(rdOptionsAppearanceAnimationSpeedFactor);
