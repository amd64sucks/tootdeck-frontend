import { writable, type Readable, type Writable } from 'svelte/store';
import { fileToBase64 } from './Utils/js/fileToBase64';
import { TootDeckConfig } from './TootDeckConfig';

export interface Sound {
	uuid: string;
	name: string;
	value: string;
	default: boolean;
}

class SoundsLibraryInner {
	static readonly instance = new SoundsLibraryInner();

	private readonly library: Array<Sound> = [];
	private readonly store: Writable<Array<Sound>> = writable(this.library);

	import(sounds: Array<Sound>) {
		this.library.splice(0, this.library.length);

		sounds.forEach((sound) => {
			this.library.push(sound);
		});

		this.store.update((v) => v);
	}

	get(uuid: string): Sound | undefined {
		return this.library.find((x) => x.uuid === uuid);
	}

	getAll(): Array<Sound> {
		return this.library;
	}

	getReadable(): Readable<Array<Sound>> {
		return this.store;
	}

	async add(file: File, name: string): Promise<boolean> {
		if (this.library.length === 10) {
			return false;
		}

		try {
			const sound = {
				uuid: window.crypto.randomUUID(),
				name,
				value: await fileToBase64(file),
				default: !this.library.length,
			};

			this.library.push(sound);

			this.store.update((v) => v);

			TootDeckConfig.save();

			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}

	setDefault(uuid: string, save_config: boolean = true): boolean {
		const found = this.library.find((x) => x.uuid === uuid);
		if (!found) {
			return false;
		}

		this.library.forEach((x) => (x.default = false));
		found.default = true;

		this.store.update((v) => v);

		if (save_config) {
			TootDeckConfig.save();
		}

		return true;
	}

	rename(uuid: string, name: string): boolean {
		const index = this.library.findIndex((x) => x.uuid === uuid);
		if (index === -1) {
			return false;
		}

		this.library[index].name = name;

		this.store.update((v) => v);

		TootDeckConfig.save();

		return true;
	}

	remove(uuid: string): boolean {
		const index = this.library.findIndex((x) => x.uuid === uuid);
		if (index === -1) {
			return false;
		}

		const removed = this.library.splice(index, 1);

		// If the removed sound was the default, set the first in library to default
		if (removed[0]?.default && this.library[0]) {
			this.library[0].default = true;
		}

		this.store.update((v) => v);

		TootDeckConfig.save();

		return true;
	}
}

export const SoundsLibrary = SoundsLibraryInner.instance;
