import EventEmitter from 'events';

import { ReconnectWebSocket } from './ReconnectWebsocket';

import { WebsocketDashboard } from './types';

export class DashboardWebsocket extends EventEmitter {
	private readonly websocket = new ReconnectWebSocket('dashboard');

	constructor() {
		super();
		this.websocket.addListener('reconnect', () => this.emit('reconnect'));
		this.websocket.addListener('error', (e) => this.emit('error', e));
		this.websocket.addListener('message', this.handleEvent.bind(this));
	}

	private handleEvent(data: WebsocketDashboard.AvailableResponse) {
		switch (data.type) {
			case WebsocketDashboard.ResponseType.Data:
				this.emit('data', data.data!);
				break;
			case WebsocketDashboard.ResponseType.User:
				this.emit('user', data.data!);
				break;
			case WebsocketDashboard.ResponseType.Status:
				this.emit('status', data.data!);
				break;
		}
	}

	start() {
		if (!this.websocket.started) {
			this.websocket.start();
		}
	}

	stop() {
		if (this.websocket.started) {
			this.websocket.stop();
		}
	}
}
