/**
 * Modified from https://github.com/h3poteto/megalodon/blob/v6.1.5/megalodon/src/mastodon/web_socket.ts
 * This file is licensed under MIT license
 */
import { EventEmitter } from 'events';

export interface WebSocketInterface {
	start(): void;
	stop(): void;
	send(data: string): void;
	// EventEmitter
	on(event: string | symbol, listener: (...args: Array<any>) => void): this;
	once(event: string | symbol, listener: (...args: Array<any>) => void): this;
	removeListener(event: string | symbol, listener: (...args: Array<any>) => void): this;
	removeAllListeners(event?: string | symbol): this;
}

export class ReconnectWebSocket extends EventEmitter implements WebSocketInterface {
	private _reconnectInterval: number = 3000;
	private _reconnectMaxAttempts: number = Infinity;
	private _reconnectCurrentAttempts: number = 0;
	private _connectionClosed: boolean = false;
	private _client: WebSocket | null = null;
	private _heartbeatInterval: number = 40000;
	private _heartbeat: NodeJS.Timeout | undefined = undefined;
	private _started: boolean = false;

	constructor(private readonly _channel: string = '') {
		super();
	}

	get started() {
		return this._started;
	}

	/**
	 * Start websocket connection.
	 */
	start() {
		this._started = true;
		this._connectionClosed = false;
		this._resetRetryParams();
		this._startWebSocketConnection();
		this._setHeartbeat();
	}

	/**
	 * Reset connection and start new websocket connection.
	 */
	private _startWebSocketConnection() {
		this._resetConnection();
		this._client = this._connect();
		this._bindSocket(this._client);
	}

	/**
	 * Stop current connection.
	 */
	stop() {
		this._started = false;
		this._connectionClosed = true;
		this._resetConnection();
		this._resetRetryParams();
		this._clearHeartbeat();
	}

	/**
	 * Clean up current connection, and listeners.
	 */
	private _resetConnection() {
		if (this._client) {
			this._client.close(1000);
			this._clearBinding();
			this._client = null;
		}
	}

	/**
	 * Resets the parameters used in reconnect.
	 */
	private _resetRetryParams() {
		this._reconnectCurrentAttempts = 0;
	}

	/**
	 * Connect to the endpoint.
	 */
	private _connect(): WebSocket {
		const query = this._channel ? '?' + this._channel : '';
		return new WebSocket(`wss://${window.location.hostname}/api/streaming${query}`);
	}

	/**
	 * Reconnects to the same endpoint.
	 */
	private _reconnect() {
		setTimeout(() => {
			// Skip reconnect when client is connecting.
			// https://github.com/websockets/ws/blob/7.2.1/lib/websocket.js#L365
			if (this._client && this._client.readyState === this._client.CONNECTING) {
				return;
			}

			if (this._reconnectCurrentAttempts < this._reconnectMaxAttempts) {
				this._reconnectCurrentAttempts++;
				this._clearBinding();
				if (this._client) {
					// In reconnect, we want to close the connection immediately,
					// because recoonect is necessary when some problems occur.
					this._client.close();
				}
				// Call connect methods
				this._client = this._connect();
				this._bindSocket(this._client);
				this.emit('reconnect', {});
			}
		}, this._reconnectInterval);
	}

	/**
	 * Clear binding event for websocket client.
	 */
	private _clearBinding() {
		if (this._client) {
			this._client.onclose = null;
			this._client.onopen = null;
			this._client.onmessage = null;
			this._client.onerror = null;
		}
	}

	/**
	 * Bind event for web socket client.
	 * @param client A WebSocket instance.
	 */
	private _bindSocket(client: WebSocket) {
		client.onclose = (ev: CloseEvent) => {
			this.emit('close', {});
			console.log(`Closed connection with ${ev.code}`);
			if (!this._connectionClosed) {
				this._reconnect();
			}
		};
		client.onopen = () => {
			this.emit('connect', {});
		};
		client.onmessage = (ev: MessageEvent<any>) => {
			this.emit('message', JSON.parse(ev.data));
		};
		client.onerror = (ev: Event) => {
			this.emit('error', ev);
		};
	}

	private _clearHeartbeat() {
		clearInterval(this._heartbeat);
	}

	private _setHeartbeat() {
		this._heartbeat = setInterval(() => {
			this.send('');
		}, this._heartbeatInterval);
	}

	send(data: string) {
		if (this._client?.readyState === this._client?.OPEN) {
			this._client?.send(data);
		}
	}
}
