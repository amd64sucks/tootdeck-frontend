import type { StandardColumn } from '../Columns/StandardColumn';

import type { RemoteInteractions } from '../../types/remoteInteractions';
import type { Dashboard } from '../../types/dashboard';
import type { Entity } from '../../types/mastodonEntities';

/**
 * Types
 */
export type OptionalParams = { list_id?: string | undefined; hashtag?: string | undefined };

/**
 * Subscriptions
 */
export enum CommandType {
	Subscribe = 'subscribe',
	Unsubscribe = 'unsubscribe',
}

export enum AvailableSubscription {
	User = 'user',
	Notification = 'user:notification',
	Public = 'public',
	Local = 'public:local',
	Hashtag = 'hashtag',
	List = 'list',
	Direct = 'direct',
}

export interface ReferenceCountedSubscription {
	used_in: Array<StandardColumn<any>>;
	stream: AvailableSubscription;
	list?: string;
	tag?: string;
}

export interface SubscribeEvent {
	handle: string;
	type: CommandType;
	stream: AvailableSubscription;
	list?: string;
	tag?: string;
}

/**
 * Responses
 */
export enum ResponseType {
	Update = 'update',
	Notification = 'notification',
	Conversation = 'conversation',
	Delete = 'delete',
	Status_update = 'status_update',
	API = 'API',
}

export interface WebsocketResponse {
	type: ResponseType;
	data: any;
}

export interface MirrorData<T> {
	stream: Array<string>;
	content: T;
}

export interface WebsocketMirrorResponse extends WebsocketResponse {
	handle: string;
	data: MirrorData<any>;
}

export interface ResponseStatus extends WebsocketMirrorResponse {
	type: ResponseType.Update;
	data: MirrorData<Entity.Status>;
}

export interface ResponseEdit extends WebsocketMirrorResponse {
	type: ResponseType.Status_update;
	data: MirrorData<Entity.Status>;
}

export interface ResponseNotification extends WebsocketMirrorResponse {
	type: ResponseType.Notification;
	data: MirrorData<Entity.Notification>;
}

export interface ResponseConversation extends WebsocketMirrorResponse {
	type: ResponseType.Conversation;
	data: MirrorData<Entity.Conversation>;
}

export interface ResponseDelete extends WebsocketResponse {
	type: ResponseType.Delete;
	data: string;
}

export interface ResponseAPI extends WebsocketResponse {
	type: ResponseType.API;
	data: ResponseAPIData;
	handle?: string;
	// For config sync
	uuid?: string;
	// For Interactions
	interactions?: RemoteInteractions;
	// For Dashboard
	dashboard?: Dashboard.WebsocketData;
	// For broadcast message
	message?: string;
}

export enum ResponseAPIData {
	Refresh = 'Refresh',
	InternalServerError = 'InternalServerError',
	AppExpired = 'AppExpired',
	NewSession = 'NewSession',
	ConfigUpdate = 'ConfigUpdate',
	AwaitingSubscription = 'AwaitingSubscription',
	Reconnected = 'Reconnected',
	CouldNotConnect = 'CouldNotConnect',
	Interactions = 'Interactions',
	BroadcastMessage = 'BroadcastMessage',
}

export namespace WebsocketDashboard {
	export enum ResponseType {
		Data = 0,
		User = 1,
		Status = 2,
	}

	export type AvailableResponse = Data | User | Status;

	export interface Response {
		type: ResponseType;
		data: any;
	}

	export interface Data extends Response {
		type: ResponseType.Data;
		data: Dashboard.WebsocketData;
	}

	export interface User extends Response {
		type: ResponseType.User;
		data: {
			sockets: Dashboard.Stats.Socket;
			account: Omit<Dashboard.Stats.Account, 'status'>;
		};
	}

	export interface Status extends Response {
		type: ResponseType.Status;
		data: {
			sockets: Dashboard.Stats.Socket;
			account: Omit<Dashboard.Stats.Account, 'jobs_count'>;
		};
	}
}
