import { get } from 'svelte/store';
import PersistentStorage from 'persistent-storage';

import {
	stOptions,
	type Options,
	default_options,
	rdOptionsBehaviorSyncAutoSync,
	stOptionsReadOnly,
	type InstanceOverride,
} from '../stores/Options';
import {
	stAccounts,
	type TootDeckAccountOptions,
	Account,
	initializedState as initializedStateAccountsOptions,
} from '../stores/Account/Accounts';
import { stLayout } from '../stores/Columns/Layout';

import type { StandardColumnParams } from './Columns/types/options';
import { TootDeckColumnIdentifier } from './Columns/TootDeckColumnTypes';
import type { IStandardColumn } from './Columns/StandardColumn';
import { TootDeckAPI } from './APIs/TootDeckAPI/TootDeckAPI';
import { errorModal } from './errorModal';
import { SoundsLibrary, type Sound } from './SoundsLibrary';
import { hexToRGB, type RGB } from './Utils/convertRGBA';
import { setFavicon } from './ComponentFunctions/options/setFavicon';

export interface ConfigColumn {
	identifier: string;
	params: StandardColumnParams;
}

export interface ConfigAccount {
	handle: string;
	value: TootDeckAccountOptions;
}

export interface ConfigDump {
	accounts_options: Array<ConfigAccount>;
	columns: Array<ConfigColumn>;
	options: Options;
	sounds_library: Array<Sound>;
}

export interface ConfigResponse {
	value: ConfigDump | null;
	updated_at: string;
}

type ConfigKey =
	| `options.${string}`
	| `columns.${string}`
	| `accounts_options.${string}`
	| `sounds_library.${string}`;

class TootDeckConfigInner {
	static readonly instance = new TootDeckConfigInner();
	private readonly uuid = window.crypto.randomUUID();
	private readonly config_key: string = 'config_';
	private readonly dump_key: string = '';
	private readonly storage = new PersistentStorage({
		keyPrefix: this.config_key,
		useCompression: false,
		useCache: false,
		storageBackend: window.localStorage,
	});

	private readonly localStorageListener = window.addEventListener('storage', (e) => {
		if (e.key !== this.config_key + this.getHandleKey()) {
			return;
		}

		const config = JSON.parse(e.newValue ?? '{}') as ConfigResponse;
		if (Object.keys(config).length === 0) {
			return;
		}

		this.updateOptions(config);
	});

	private upload_timeout: NodeJS.Timeout = setTimeout(() => {});

	private getHandleKey() {
		return Account.getLoginAccount().mirrorAPI.handle_string;
	}

	private getCurrentLayout(): Array<ConfigColumn> {
		const column_data: Array<ConfigColumn> = [];
		for (const column of get(stLayout).values()) {
			const identifier = column.identifier;
			if (
				identifier === TootDeckColumnIdentifier.Thread ||
				identifier === TootDeckColumnIdentifier.Profile
			) {
				continue;
			}
			const { options } = column.params;

			column_data.push({
				identifier,
				params: {
					...column.params,
					options: get(options),
				},
			});
		}

		return column_data;
	}

	private getCurrentAccountOptions(): Array<ConfigAccount> {
		const accounts_options: Array<ConfigAccount> = [];

		for (const account of get(stAccounts).values()) {
			accounts_options.push({
				handle: account.mirrorAPI.handle_string,
				value: get(account.options),
			});
		}

		return accounts_options;
	}

	private getCurrent(): ConfigDump {
		const column_data: Array<ConfigColumn> = this.getCurrentLayout();
		const accounts_options: Array<ConfigAccount> = this.getCurrentAccountOptions();
		const sounds_library = SoundsLibrary.getAll();
		const options = get(stOptions);

		return {
			accounts_options,
			columns: column_data,
			options,
			sounds_library,
		};
	}

	private localUpToDate(current: number, remote: number) {
		return current >= remote;
	}

	async isUptoDate() {
		if (get(stOptionsReadOnly)) {
			return true;
		}

		const remote_config = await this._download();
		if (!remote_config) {
			return true;
		}

		const local_config = this.storage.getItem(this.getHandleKey()) as ConfigResponse;
		if (!local_config) {
			return false;
		}

		const remote = new Date(remote_config.updated_at).valueOf();
		const current = new Date(local_config.updated_at).valueOf();

		return this.localUpToDate(current, remote);
	}

	async import() {
		let local_config = this.storage.getItem(this.getHandleKey()) as ConfigResponse;

		let config: ConfigResponse | null = null;
		if (local_config) {
			config = local_config;

			if (!get(stOptionsReadOnly) && config.value?.options.behavior.sync.autoSync) {
				config = (await this.sync())!;
			}
		} else if (!get(stOptionsReadOnly)) {
			config = (await this._download()) ?? null;
			if (config) {
				this.saveLocal(config);
			}
		}

		// No config yet, returning the default one
		if (!config || !config.value?.options) {
			return {
				value: { options: this.verifyOptions(default_options) },
			};
		}

		config.value.options = this.verifyOptions(config.value.options);

		return config;
	}

	async sync() {
		if (get(stOptionsReadOnly)) {
			return;
		}

		const remote_config = await this._download();
		const local_config = this.storage.getItem(this.getHandleKey()) as ConfigResponse;
		if (!remote_config && !local_config) {
			return;
		}

		// No config on server
		if (!remote_config) {
			if (!local_config.value) {
				local_config.value = this.getCurrent();
			}

			await this._upload(local_config.value);
			return local_config;
		}

		const remote = new Date(remote_config.updated_at).valueOf();
		const current = new Date(local_config.updated_at).valueOf();

		// Local config is up to date
		if (remote === current) {
			return local_config;
		}

		// Local config is newer
		if (this.localUpToDate(current, remote)) {
			await this._upload(local_config.value!);
			return local_config;
		}

		// Local config is outdated
		this.updateOptions(remote_config);
		this.updateAccountsOptions(remote_config);
		this.updateSoundsLibrary(remote_config);

		this.saveLocal(remote_config);
		return remote_config;
	}

	save(config?: ConfigResponse) {
		if (get(stOptionsReadOnly)) {
			return;
		}

		this.saveLocal(config);

		if (get(stOptions).behavior.sync.autoSync) {
			clearTimeout(this.upload_timeout);
			this.upload_timeout = setTimeout(this.upload.bind(this), 5000);
		}
	}

	/**
	 * Only save one value
	 *
	 * You can only replace the final value,
	 * attempting to edit the object will throw
	 *
	 * @param key dot separated path to key
	 * @param value type should match replaced value
	 */
	saveOne(key: ConfigKey, value: any, value_is_object: boolean = false): void {
		if (get(stOptionsReadOnly)) {
			return;
		}

		const key_array = key.split('.');
		const local = this.importLocal();
		if (!local.value || !local.value.options) {
			local.value = this.getCurrent();
		}

		/**
		 * key: options.behavior.navbar.expanded
		 * value: false
		 *
		 * object_at[options][behavior][navbar][expanded]
		 * object_at[expanded]
		 *
		 * check if object_at[expanded] exist
		 * check type of object_at[expanded] and value
		 * Set object_at[expanded] with value
		 *
		 * Save to localStorage
		 * Sync config if the option is enabled
		 */

		let object_at: any = local.value;
		let current_key: any;
		while (key_array.length) {
			current_key = key_array.shift()!;
			if (key_array.length === 0) {
				break;
			}

			object_at = object_at[current_key];
		}

		const current_value = object_at[current_key];
		if (
			current_value === undefined ||
			(!value_is_object && typeof current_value === 'object')
		) {
			throw new Error(
				`Could not find value associated with ${key} in options, found value is "${current_value}"`
			);
		}
		if (typeof current_value !== typeof value) {
			throw new Error(
				`Could not set the provided value "${value}" to ${key} because ${typeof current_value} and ${typeof value} are different types`
			);
		}

		object_at[current_key] = value;

		const handle = this.getHandleKey();
		this.storage.setItem(handle, {
			value: local.value,
			updated_at: new Date().toISOString(),
		});

		if (get(rdOptionsBehaviorSyncAutoSync)) {
			if (this.upload_timeout) {
				clearTimeout(this.upload_timeout);
			}

			this.upload_timeout = setTimeout(() => this.sync(), 5000);
		}
	}

	saveLayout(): void {
		if (get(stOptionsReadOnly)) {
			return;
		}

		const local = this.importLocal();
		if (!local.value) {
			local.value = this.getCurrent();
		} else {
			local.value.columns = this.getCurrentLayout();
		}

		const handle = this.getHandleKey();
		this.storage.setItem(handle, {
			value: local.value,
			updated_at: new Date().toISOString(),
		});

		if (get(rdOptionsBehaviorSyncAutoSync)) {
			if (this.upload_timeout) {
				clearTimeout(this.upload_timeout);
			}

			this.upload_timeout = setTimeout(() => this.sync(), 5000);
		}
	}

	/**
	 * Download
	 */
	// #region

	private uncompress(raw: string): ConfigDump {
		const tmp = 'config_uncompress_tmp';
		const storage = new PersistentStorage({
			keyPrefix: tmp,
			useCompression: true,
			useCache: false,
			storageBackend: window.localStorage,
		});

		// Set compressed string
		window.localStorage.setItem(tmp, raw);
		// Get uncompressed config
		const uncompressed = storage.getItem(this.dump_key)!;
		// Remove tmp
		window.localStorage.removeItem(tmp);

		return uncompressed;
	}

	private async _download(disable_cache: boolean = false): Promise<ConfigResponse | null> {
		const response = await TootDeckAPI.User.getConfig(disable_cache);
		if (!response) {
			errorModal('Error when importing config from server.', undefined, true);
			return null;
		}

		const uncompressed = this.uncompress(response.value as any);
		return {
			value: uncompressed,
			updated_at: response.updated_at,
		};
	}

	// #endregion

	/**
	 * Upload
	 */
	// #region

	private compress(config: ConfigDump): string {
		const tmp = 'config_compress_tmp';
		const storage = new PersistentStorage({
			keyPrefix: tmp,
			useCompression: true,
			useCache: false,
			storageBackend: window.localStorage,
		});

		// Compress config
		storage.setItem(this.dump_key, config);
		// Get compressed string
		const compressed = window.localStorage.getItem(tmp)!;
		// Remove tmp
		window.localStorage.removeItem(tmp);

		return compressed;
	}

	private async _upload(current: ConfigDump): Promise<boolean> {
		const compressed = this.compress(current);
		const upload = await TootDeckAPI.User.addConfig(compressed, this.uuid);
		if (!upload) {
			errorModal('Error happened when uploading config to server.', undefined, true);
			return false;
		}

		return true;
	}

	// #endregion

	/**
	 * Verify
	 */
	// #region

	private correctOption(input: any, correction: any) {
		return input ?? correction;
	}

	// prettier-ignore
	private verifyOptions(options: Options | undefined) {
		function convertAccentColor(color: string): RGB {
			return color.includes("#") ? hexToRGB(color) : color as RGB
		}

		const verif: Options = {
			appearance: {
				// Single value
				accentColor: convertAccentColor(this.correctOption(options?.appearance?.accentColor, default_options.appearance.accentColor)),
				accentScrollbar: this.correctOption(options?.appearance?.accentScrollbar, default_options.appearance.accentScrollbar),
				fontSize: this.correctOption(options?.appearance?.fontSize, default_options.appearance.fontSize),
				theme: this.correctOption(options?.appearance?.theme, default_options.appearance.theme),
				customCSS: this.correctOption(options?.appearance?.customCSS, default_options.appearance.customCSS),

				// objects
				animation: {
					animateGif: {
						avatar: this.correctOption(options?.appearance?.animation?.animateGif?.avatar, default_options.appearance.animation.animateGif.avatar),
						emoji: this.correctOption(options?.appearance?.animation?.animateGif?.emoji, default_options.appearance.animation.animateGif.emoji),
						notification: this.correctOption(options?.appearance?.animation?.animateGif?.notification, default_options.appearance.animation.animateGif.notification),
						status: this.correctOption(options?.appearance?.animation?.animateGif?.status, default_options.appearance.animation.animateGif.status),
					},
					column: {
						content: this.correctOption(options?.appearance?.animation?.column?.content, default_options.appearance.animation.column.content),
					},
					mediaViewer: {
						mediaChange: this.correctOption(options?.appearance?.animation?.mediaViewer?.mediaChange, default_options.appearance.animation.mediaViewer.mediaChange),
					},
					speedFactor: this.correctOption(options?.appearance?.animation?.speedFactor, default_options.appearance.animation.speedFactor),
				},
				column: {
					header: {
						avatar: this.correctOption(options?.appearance?.column?.header?.avatar, default_options.appearance.column.header.avatar),
						btn: {
							clear: this.correctOption(options?.appearance?.column?.header?.btn?.clear, default_options.appearance.column.header.btn.clear),
							collapse: this.correctOption(options?.appearance?.column?.header?.btn?.collapse, default_options.appearance.column.header.btn.collapse),
							delete: this.correctOption(options?.appearance?.column?.header?.btn?.delete, default_options.appearance.column.header.btn.delete),
						},
						icon: this.correctOption(options?.appearance?.column?.header?.icon, default_options.appearance.column.header.icon),
					},
					width: this.correctOption(options?.appearance?.column?.width, default_options.appearance.column.width),
				},
				composer: {
					fromView: this.correctOption(options?.appearance?.composer?.fromView, default_options.appearance.composer.fromView),
				},
				customValue: {
					publishBtn: this.correctOption(options?.appearance?.customValue?.publishBtn, default_options.appearance.customValue.publishBtn),
					reblog: this.correctOption(options?.appearance?.customValue?.reblog, default_options.appearance.customValue.reblog),
					reblogBtn: this.correctOption(options?.appearance?.customValue?.reblogBtn, default_options.appearance.customValue.reblogBtn),
					reblogs: this.correctOption(options?.appearance?.customValue?.reblogs, default_options.appearance.customValue.reblogs),
				},
				background: {
					image: this.correctOption(options?.appearance?.background?.image, default_options.appearance.background.image),
					opacity: {
						value: this.correctOption(options?.appearance?.background?.opacity?.value, default_options.appearance.background.opacity.value),
						composer: this.correctOption(options?.appearance?.background?.opacity?.composer, default_options.appearance.background.opacity.composer),
						headers: this.correctOption(options?.appearance?.background?.opacity?.headers, default_options.appearance.background.opacity.headers),
						sidebar: this.correctOption(options?.appearance?.background?.opacity?.sidebar, default_options.appearance.background.opacity.sidebar),
					}
				},
				border: {
					status: this.correctOption(options?.appearance?.border?.status, default_options.appearance.border.status),
					column: this.correctOption(options?.appearance?.border?.column, default_options.appearance.border.column),
				},
				emoji: {
					style: this.correctOption(options?.appearance?.emoji?.style, default_options.appearance.emoji.style),
					reaction: {
						value: this.correctOption(options?.appearance?.emoji?.reaction?.value, default_options.appearance.emoji.reaction.value),
						inQuote: this.correctOption(options?.appearance?.emoji?.reaction?.inQuote, default_options.appearance.emoji.reaction.inQuote),
						max: this.correctOption(options?.appearance?.emoji?.reaction?.max, default_options.appearance.emoji.reaction.max),
					},
				},
				status: {
					interaction: {
						compactMode: this.correctOption(options?.appearance?.status?.interaction?.compactMode, default_options.appearance.status.interaction.compactMode),
						showOnHover: this.correctOption(options?.appearance?.status?.interaction?.showOnHover, default_options.appearance.status.interaction.showOnHover),
					},
					metadata: {
						language: this.correctOption(options?.appearance?.status?.metadata?.language, default_options.appearance.status.metadata.language),
						visibility: this.correctOption(options?.appearance?.status?.metadata?.visibility, default_options.appearance.status.metadata.visibility),
					},
				},
				extra: {
					snow: this.correctOption(options?.appearance?.extra?.snow, default_options.appearance.extra.snow),
					cherryBlossomPetals: this.correctOption(options?.appearance?.extra?.cherryBlossomPetals, default_options.appearance.extra.cherryBlossomPetals),
					mutualIndicator: this.correctOption(options?.appearance?.extra?.mutualIndicator, default_options.appearance.extra.mutualIndicator),
					removePrefixHandle: {
						value: this.correctOption(options?.appearance?.extra?.removePrefixHandle?.value, default_options.appearance.extra.removePrefixHandle.value),
						items: this.correctOption(options?.appearance?.extra?.removePrefixHandle?.items, default_options.appearance.extra.removePrefixHandle.items)
					},
					prideLogo: {
						toggle: this.correctOption(options?.appearance?.extra?.prideLogo?.toggle, default_options.appearance.extra.prideLogo.toggle),
						scheduled: this.correctOption(options?.appearance?.extra?.prideLogo?.scheduled, default_options.appearance.extra.prideLogo.scheduled),
						random: this.correctOption(options?.appearance?.extra?.prideLogo?.random, default_options.appearance.extra.prideLogo.random),
						src: this.correctOption(options?.appearance?.extra?.prideLogo?.src, default_options.appearance.extra.prideLogo.src),
						userDefined: this.correctOption(options?.appearance?.extra?.prideLogo?.userDefined, default_options.appearance.extra.prideLogo.userDefined)
					},
				},
			},
			behavior: {
				column: {
					collapseReadMessage: this.correctOption(options?.behavior?.column?.collapseReadMessage, default_options.behavior.column.collapseReadMessage), // Soon™
					maxStatuses: this.correctOption(options?.behavior?.column?.maxStatuses, default_options.behavior.column.maxStatuses),
					pauseOnHover: this.correctOption(options?.behavior?.column?.pauseOnHover, default_options.behavior.column.pauseOnHover), // Soon™
					hideDMs: this.correctOption(options?.behavior?.column?.hideDMs, default_options.behavior.column.hideDMs),
					thread: {
						insertStatusBeforeSelected: this.correctOption(options?.behavior?.column?.thread?.insertStatusBeforeSelected, default_options.behavior.column.thread.insertStatusBeforeSelected),
						openInAnotherColumn: this.correctOption(options?.behavior?.column?.thread?.openInAnotherColumn, default_options.behavior.column.thread.openInAnotherColumn),
					}
				},
				composer: {
					reverseMediaOrder: this.correctOption(options?.behavior?.composer?.reverseMediaOrder, default_options.behavior.composer.reverseMediaOrder),
					stayOpen: this.correctOption(options?.behavior?.composer?.stayOpen, default_options.behavior.composer.stayOpen),
					favouriteLanguages: this.correctOption(options?.behavior?.composer?.favouriteLanguages, default_options.behavior.composer.favouriteLanguages),
					simultaneousUpload: this.correctOption(options?.behavior?.composer?.simultaneousUpload, default_options.behavior.composer.simultaneousUpload),
				},
				emoji: {
					zoomOnHover: this.correctOption(options?.behavior?.emoji?.zoomOnHover, default_options.behavior.emoji.zoomOnHover),
				},
				emojiPicker: {
					toggleWideEmoji: this.correctOption(options?.behavior?.emojiPicker?.toggleWideEmoji, default_options.behavior.emojiPicker.toggleWideEmoji),
					topRow: this.correctOption(options?.behavior?.emojiPicker?.topRow, default_options.behavior.emojiPicker.topRow),
				},
				instance: (() => {
					if (options?.behavior.instance && Object.entries(options?.behavior?.instance ?? {}).length) {
						return options.behavior.instance
					}

					const instances = Array.from(get(stAccounts)).map(([key, value]) => value.mirrorAPI.domain)
					const override: InstanceOverride = {}

					instances.forEach((instance) => {
						override[instance] = {
							emoji_reaction: null,
							poll_allow_media: null,
							poll_max_characters_per_option: null,
							poll_max_options: null,
							poll_min_expiration: null,
							poll_max_expiration: null,
						}
					})

					return override
				})(),
				expandedPanel: {
				},
				navbar: {
					expanded: this.correctOption(options?.behavior?.navbar?.expanded, default_options.behavior.navbar.expanded),
				},
				notification: {
					highlightNew: this.correctOption(options?.behavior?.notification?.highlightNew, default_options.behavior.notification.highlightNew),
					highlightDuration: this.correctOption(options?.behavior?.notification?.highlightDuration, default_options.behavior.notification.highlightDuration),
					playCustomSound: {
						value: this.correctOption(options?.behavior?.notification?.playCustomSound?.value, default_options.behavior.notification.playCustomSound.value),
						limit: this.correctOption(options?.behavior?.notification?.playCustomSound?.limit, default_options.behavior.notification.playCustomSound.limit),
						volume: this.correctOption(options?.behavior?.notification?.playCustomSound?.volume, default_options.behavior.notification.playCustomSound.volume),
						sounds: this.correctOption(options?.behavior?.notification?.playCustomSound?.sounds, default_options.behavior.notification.playCustomSound.sounds)
					},
				},
				notificationModal: {
					dismissAfter: this.correctOption(options?.behavior?.notificationModal?.dismissAfter, default_options.behavior.notificationModal.dismissAfter),
				},
				profile: {
					defaultTab: this.correctOption(options?.behavior?.profile?.defaultTab, default_options.behavior.profile.defaultTab),
					showMediaAsGrid: this.correctOption(options?.behavior?.profile?.showMediaAsGrid, default_options.behavior.profile.showMediaAsGrid),
				},
				status: {
					hideThreshold: this.correctOption(options?.behavior?.status?.hideThreshold, default_options.behavior.status.hideThreshold),
					quote: this.correctOption(options?.behavior?.status?.quote, default_options.behavior.status.quote),
					showInteractionsOnQuote: this.correctOption(options?.behavior?.status?.showInteractionsOnQuote, default_options.behavior.status.showInteractionsOnQuote),
					twitterEmbed: {
						value: this.correctOption(options?.behavior?.status?.twitterEmbed?.value, default_options.behavior.status.twitterEmbed.value),
						exclude: this.correctOption(options?.behavior?.status?.twitterEmbed?.exclude, default_options.behavior.status.twitterEmbed.exclude)
					},
					youtubeEmbed: {
						value: this.correctOption(options?.behavior?.status?.youtubeEmbed?.value, default_options.behavior.status.youtubeEmbed.value),
						piped: {
							value: this.correctOption(options?.behavior?.status?.youtubeEmbed?.piped?.value, default_options.behavior.status.youtubeEmbed.piped.value),
							hostname: this.correctOption(options?.behavior?.status?.youtubeEmbed?.piped?.hostname, default_options.behavior.status.youtubeEmbed.piped.hostname),
						},
					},
					DailymotionEmbed: this.correctOption(options?.behavior?.status?.DailymotionEmbed, default_options.behavior.status.DailymotionEmbed),
				},
				search: {
					toggleAdvancedView: this.correctOption(options?.behavior?.search?.toggleAdvancedView, default_options.behavior.search.toggleAdvancedView),
					querySearchInHistory: this.correctOption(options?.behavior?.search?.querySearchInHistory, default_options.behavior.search.querySearchInHistory),
				},
				sync: {
					autoSync: this.correctOption(options?.behavior?.sync?.autoSync, default_options.behavior.sync.autoSync),
				},
				unreadInTitle: {
					notification: this.correctOption(options?.behavior?.unreadInTitle?.notification, default_options.behavior.unreadInTitle.notification), // Soon™
					message: this.correctOption(options?.behavior?.unreadInTitle?.message, default_options.behavior.unreadInTitle.message), // Soon™
				},
				showChangelog: this.correctOption(options?.behavior?.showChangelog, default_options.behavior.showChangelog)
			},
		};

		return verif;
	}

	// #endregion

	/**
	 * Local
	 */
	// #region

	saveLocal(config?: ConfigResponse) {
		if (get(stOptionsReadOnly)) {
			return;
		}

		const handle = this.getHandleKey();

		if (config) {
			this.storage.setItem(handle, config);
			return;
		}

		const local = this.getCurrent();
		this.storage.setItem(handle, {
			value: local,
			updated_at: new Date().toISOString(),
		});
	}

	deleteLocal() {
		if (get(stOptionsReadOnly)) {
			return;
		}

		this.storage.removeItem(this.getHandleKey());
	}

	importLocal(): ConfigResponse {
		return (
			this.storage.getItem(this.getHandleKey()) ?? {
				value: this.getCurrent(),
				updated_at: new Date(),
			}
		);
	}

	async upload(config?: ConfigDump) {
		if (get(stOptionsReadOnly)) {
			return;
		}

		if (!config) {
			config = this.getCurrent();
		}
		await this._upload(config);
	}

	// #endregion

	async deleteRemote() {
		if (get(stOptionsReadOnly)) {
			return;
		}

		await TootDeckAPI.User.deleteConfig();
	}

	async download(uuid?: string) {
		if (get(stOptionsReadOnly) || uuid === this.uuid) {
			return;
		}

		const remote_config = await this._download(true);
		if (!remote_config || !remote_config.value) {
			return;
		}

		this.updateOptions(remote_config);
		this.updateAccountsOptions(remote_config);
		this.updateSoundsLibrary(remote_config);

		this.saveLocal(remote_config);
	}

	/**
	 * Update
	 */
	// #region

	private updateOptions(remote_config: ConfigResponse) {
		const verif = this.verifyOptions(remote_config.value?.options);
		stOptions.set(verif);
		setFavicon(verif.appearance.extra.prideLogo);
	}

	private updateAccountsOptions(remote_config: ConfigResponse) {
		initializedStateAccountsOptions(false);

		const accounts_options = remote_config.value!.accounts_options;
		if (accounts_options) {
			accounts_options.forEach((account_opts) => {
				const account = Account.get(account_opts.handle, false);
				if (account) {
					account.options.set(account_opts.value);
				}
			});
		}

		initializedStateAccountsOptions(true);
	}

	private updateSoundsLibrary(remote_config: ConfigResponse) {
		const remote_sounds_library = remote_config.value!.sounds_library;
		// No sound library yet
		if (!remote_sounds_library) {
			return;
		}

		// Empty sound library
		if (!remote_sounds_library.length) {
			SoundsLibrary.import(remote_sounds_library);
			return;
		}

		// No sound library yet
		const local_sounds_library = SoundsLibrary.getAll();
		if (!local_sounds_library.length) {
			SoundsLibrary.import(remote_sounds_library);
			return;
		}

		const local_sound_default = local_sounds_library.find((x) => x.default)!;
		const remote_sound_default = remote_sounds_library.find((x) => x.default)!;

		SoundsLibrary.import(remote_sounds_library);

		// check if the default sound has changed
		if (local_sound_default.uuid === remote_sound_default.uuid) {
			return;
		}

		// Update the default sound on all columns using it
		get(stLayout).forEach((column) => {
			const instance = column as IStandardColumn;
			const column_sound = instance.sound;

			if (column_sound === local_sound_default.uuid) {
				instance.sound = remote_sound_default.uuid;
			}
		});
	}

	// #endregion
}

export const TootDeckConfig = TootDeckConfigInner.instance;
