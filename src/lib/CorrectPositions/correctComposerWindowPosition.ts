export function correctComposerWindowPosition(
	button: HTMLButtonElement,
	element: HTMLElement,
	override?: { x?: number; y?: number; center?: boolean; overflow_wrap_left?: boolean }
) {
	const max_height = window.innerHeight;

	const button_location = button.getBoundingClientRect();
	const button_width = button.clientWidth;
	const button_height = button.clientHeight;
	const button_x = button_location.x;
	const button_y = button_location.y;

	const element_width = element.clientWidth;
	const element_height = element.clientHeight;

	// Vertical layout
	let element_x = override?.center
		? button_x - Math.floor(element_width * 0.5)
		: button_x + (override?.x || 0);
	let element_y = button_y + button_height + (override?.y || 0);

	element.style.left = element_x + 'px';
	element.style.top = element_y + 'px';

	if (element_y + element_height > max_height) {
		// Horizontal layout
		element_x = override?.overflow_wrap_left
			? button_x - element_width
			: button_x + button_width;
		element_y = button_y - Math.floor(element_height * 0.5) - 1.5;

		element.style.left = element_x + 'px';
		element.style.top = element_y + 'px';

		if (element_y + element_height > max_height) {
			element.style.top = max_height - element_height + 'px';
		}
	}

	element.focus();
}
