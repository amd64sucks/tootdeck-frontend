import { MirrorInterface } from '../Interface';

import type { SearchOptional } from '../types';
import type { Entity } from '../../../../types/mastodonEntities';

export class Search extends MirrorInterface {
	/**
	 * GET /api/v2/search
	 * https://docs.joinmastodon.org/methods/search/#v2
	 *
	 * @query `q`
	 * @query `max_id`
	 * @query `min_id`
	 * @query `limit`
	 * @returns `Array<Entity.Results>`
	 */
	async accounts(query: string, options?: SearchOptional) {
		return this.fetchAPI<Entity.Results>('GET', `/api/v2/search`, {
			query: { q: query, type: 'accounts', ...options },
		}).then((r) => r?.data?.accounts ?? null);
	}

	/**
	 * GET /api/v2/search
	 * https://docs.joinmastodon.org/methods/search/#v2
	 *
	 * @query `q`
	 * @query `max_id`
	 * @query `min_id`
	 * @query `limit`
	 * @returns `Array<Entity.Results>`
	 */
	async hashtags(query: string, options?: SearchOptional) {
		return this.fetchAPI<Entity.Results>('GET', `/api/v2/search`, {
			query: { q: query, type: 'hashtags', ...options },
		}).then((r) => r?.data?.hashtags ?? null);
	}

	/**
	 * GET /api/v2/search
	 * https://docs.joinmastodon.org/methods/search/#v2
	 *
	 * @query `q`
	 * @query `max_id`
	 * @query `min_id`
	 * @query `limit`
	 * @returns `Array<Entity.Results>`
	 */
	async statuses(query: string, options?: SearchOptional) {
		return this.fetchAPI<Entity.Results>('GET', `/api/v2/search`, {
			query: { q: query, type: 'statuses', ...options },
		}).then((r) => r?.data?.statuses ?? null);
	}

	/**
	 * GET /api/v2/search
	 * https://docs.joinmastodon.org/methods/search/#v2
	 *
	 * @query `q`
	 * @query `types`
	 * @query `max_id`
	 * @query `min_id`
	 * @query `limit`
	 * @returns `Array<Entity.Results>`
	 */
	async any(
		query: string,
		options?: SearchOptional & { type?: 'accounts' | 'hashtags' | 'statuses' }
	) {
		return this.fetchAPI<Entity.Results>('GET', `/api/v2/search`, {
			query: { q: query, ...options },
		}).then((r) => r?.data ?? null);
	}
}
