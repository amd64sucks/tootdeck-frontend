import { NotificationModalType, notificationModal } from '../../../stores/NotificationsModal';

import {
	fetchAPI,
	NormalMastodonResponseCodes,
	type FetchAPIOptionalParams,
	type MirrorEndpoint,
} from '../fetchAPI';
import { uploadAPI, type UploadAPIOptionalParams } from '../uploadAPI';
import { FetchError } from '../Error';
import { errorModal } from '../../errorModal';
import { stringifyHandle } from '../../Utils/handle/stringifyHandle';

import type { InstanceHandle } from '../../../types/handle';
import type { InstanceType } from '../../../types/megalodon';
import type { Entity } from '../../../types/mastodonEntities';

export class MirrorInterface {
	protected readonly _handle: InstanceHandle;
	protected readonly _instance_type: InstanceType;

	constructor(handle: InstanceHandle, instanceType: InstanceType) {
		this._handle = handle;
		this._instance_type = instanceType;
	}

	get domain() {
		return this._handle.domain;
	}

	get handle_string(): string {
		return stringifyHandle(this._handle);
	}

	get handle(): InstanceHandle {
		return this._handle;
	}

	get instance_type(): InstanceType {
		return this._instance_type;
	}

	private async errorHandler<T>(
		r: Entity.Response<T> | FetchError | null,
		hide_error?: boolean
	): Promise<Entity.Response<T> | null> {
		if (!r) {
			return null;
		}

		if (r instanceof FetchError) {
			if (hide_error) {
				return null;
			}

			if (r.data.mirror) {
				const message = `${r.code} ${r.data.message}\n${Object.values(r.data.detail ?? {}).join(' ')}`;

				switch (r.code) {
					case 429:
						let offset =
							(r.data.rateLimitReset!.valueOf() - new Date().valueOf()) / 1000;
						let value = offset.toString();
						if (offset > 60) {
							offset = Math.floor(offset / 60);
							value = offset <= 1 ? offset + 'min' : offset + 'mins';
						} else if (offset <= 60) {
							value = 'less than a minute';
						}

						const message_text = `You are rate limited. Try again`;

						notificationModal.create(
							NotificationModalType.Warn,
							message_text + (offset ? ` in ${value}.` : ' later.'),
							true
						);
						break;
					case 400:
					case 409:
					case 404:
					case 422:
					case 500:
						errorModal(message, [message, r.data], true);
						break;
					default:
						errorModal(
							'Something went wrong on the instance.',
							[message, r.data.error],
							true
						);
				}
			} else if (r.data.message !== 'Got unknown response from the instance') {
				errorModal(
					'Something went wrong on the backend.',
					[`${r.code} ${r.data.message}`, r.data.error],
					true
				);
			}
		} else if (NormalMastodonResponseCodes.includes(r.status)) {
			return r;
		}

		return null;
	}

	async uploadAPI<T>(
		method: string,
		endpoint: string,
		file: File,
		optional?: UploadAPIOptionalParams
	): Promise<Entity.Response<T> | null> {
		const header = {
			handle: this.handle_string,
		};

		const request = uploadAPI<T>(method, endpoint, file, {
			...optional,
			header,
			mirrorAPI: true,
		}) as Promise<Entity.Response<T> | null>;
		request.then((r) => this.errorHandler(r, optional?.hide_error));

		return request;
	}

	async fetchAPI<T>(
		method: string,
		endpoint: MirrorEndpoint,
		optional?: FetchAPIOptionalParams
	): Promise<Entity.Response<T> | null> {
		const header = {
			handle: this.handle_string,
		};

		return fetchAPI<T>(method, endpoint, { ...optional, header, mirrorAPI: true })
			.catch((e: FetchError) => e)
			.then((r) => this.errorHandler(r, optional?.hide_error));
	}
}
