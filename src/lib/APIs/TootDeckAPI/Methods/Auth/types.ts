import type { InstanceType } from '../../../../../types/megalodon';

export interface AccountResponse {
	username: string;
	domain: string;
	type: InstanceType;
	valid: boolean;
}

export interface AuthGetResponse {
	admin?: boolean;
	main: AccountResponse;
	secondary: Array<AccountResponse>;
}

export interface OpenSessionResponse {
	current: boolean;
	revoked: boolean;
	uuid: string;
	browser_name: string;
	browser_version: number;
	os: string;
	created_at: Date;
	updated_at: Date;
}
