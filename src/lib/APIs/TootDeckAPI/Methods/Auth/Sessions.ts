import { fetchAPI } from '../../../fetchAPI';
import type { FetchError } from '../../../Error';

import type { OpenSessionResponse } from './types';

export class Sessions {
	/**
	 * GET /api/auth/sessions
	 *
	 * Get all opened sessions
	 *
	 * @param token_uuid string
	 * @returns OpenSessionResponse
	 */
	async get(): Promise<Array<OpenSessionResponse> | null> {
		return fetchAPI<Array<OpenSessionResponse>>('GET', '/api/auth/sessions')
			.catch((e: FetchError) => null)
			.then((r) => r?.data ?? null);
	}

	/**
	 * DELETE /api/auth/sessions
	 *
	 * Revoke token associated to this session
	 *
	 * @param session_uuid string
	 * @returns boolean
	 */
	async revoke(session_uuid: string): Promise<boolean> {
		return fetchAPI('DELETE', '/api/auth/sessions/revoke/' + session_uuid)
			.catch((e: FetchError) => null)
			.then((result) => (result ? true : false));
	}

	/**
	 * DELETE /api/auth/sessions
	 *
	 * Delete session from history
	 *
	 * @param session_uuid string
	 * @returns boolean
	 */
	async delete(session_uuid: string): Promise<boolean> {
		return fetchAPI('DELETE', '/api/auth/sessions/delete/' + session_uuid)
			.catch((e: FetchError) => null)
			.then((result) => (result ? true : false));
	}
}
