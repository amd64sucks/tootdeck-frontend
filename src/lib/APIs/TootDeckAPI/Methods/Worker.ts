import { fetchAPI } from '../../fetchAPI';
import type { FetchError } from '../../Error';
import { isTwitterLink } from '../../../ContentParser/anchorsParser';

import type { Twitter } from '../../../../types/twitter';

export class Worker {
	/**
	 * POST /api/worker/static_emoji/:identifier
	 *
	 * Request a static emoji version
	 *
	 * @returns string
	 */
	async staticEmoji(identifier: string, cdn?: string): Promise<string | null> {
		return fetchAPI<string>('POST', '/api/worker/static_emoji/' + identifier, {
			body: { cdn },
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || result.status !== 200) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * POST /api/worker/twitter_embed
	 *
	 * Request a twitter embed
	 *
	 * @returns string
	 */
	async twitterEmbed(href: string): Promise<Twitter.Response | null> {
		if (!isTwitterLink(href)) {
			return null;
		}

		const { origin, pathname } = new URL(href);

		return fetchAPI<Twitter.Response>('POST', '/api/worker/twitter_embed', {
			body: { url: origin + pathname },
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || result.status !== 200) {
					return null;
				}

				return result.data;
			});
	}
}
