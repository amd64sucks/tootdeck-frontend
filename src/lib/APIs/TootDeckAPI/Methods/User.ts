import { fetchAPI } from '../../fetchAPI';

import type { ConfigResponse } from '../../../TootDeckConfig';
import type { FetchError } from '../../Error';

export class User {
	/**
	 * PUT /api/user/erase
	 *
	 * Generate code to completely remove user from server
	 *
	 * @returns string on success
	 * @returns null on fail
	 */
	async eraseRequest(): Promise<string | null> {
		return fetchAPI<{ code: string }>('PUT', '/api/user/erase')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || result.status !== 200) {
					return null;
				}

				return result.data.code;
			});
	}

	/**
	 * DELETE /api/user/erase
	 *
	 * Completely remove user from server
	 *
	 * @param code from EraseRequest()
	 * @returns on success `{account: "OK", token: "OK"}`
	 * @returns on failure `{account: string, token: string}`
	 */
	async eraseConfirm(code: string): Promise<{ user: string; token: string } | null> {
		return fetchAPI<{ user: string; token: string }>('DELETE', '/api/user/erase', {
			query: {
				code,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || result.status !== 200) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * DELETE /api/user/unlink
	 *
	 * Unlink given account from user
	 *
	 * @returns on success `{account: "OK", token: "OK"}`
	 * @returns on failure `{account: string, token: string}`
	 */
	async unlink(handle: string): Promise<{ account: string; token: string } | null> {
		return fetchAPI<{ account: string; token: string }>('DELETE', '/api/user/unlink', {
			query: {
				handle,
			},
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || result.status !== 200) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/user/config
	 *
	 * Retrieves compressed config from database
	 */
	async getConfig(disable_cache: boolean = false): Promise<ConfigResponse | null> {
		return fetchAPI<ConfigResponse>('GET', '/api/user/config', {
			disable_cache,
		})
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result || (result.status !== 200 && result.status !== 404)) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * POST /api/user/config
	 *
	 * Store config to database
	 */
	async addConfig(config: string, uuid?: string): Promise<boolean> {
		return fetchAPI<void>('POST', '/api/user/config', {
			body: {
				uuid,
				config,
			},
		})
			.catch((e: FetchError) => {})
			.then((result) => result?.status === 201);
	}

	/**
	 * DELETE /api/user/config
	 *
	 * Remove config from database
	 */
	async deleteConfig(): Promise<void> {
		return fetchAPI<void>('DELETE', '/api/user/config')
			.catch((e: FetchError) => {})
			.then((result) => result?.data);
	}
}
