import type { OptionalFeatures } from '../../../../stores/OptionalFeatures';

import { fetchAPI } from '../../fetchAPI';
import type { FetchError } from '../../Error';
import type { ApiResponseTime } from './types';

export class Others {
	/**
	 * GET /api/ping
	 *
	 * Check server availability
	 *
	 * @returns "PONG"
	 */
	async ping(): Promise<'PONG' | null> {
		return fetchAPI<'PONG'>('GET', '/api/ping')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/version
	 *
	 * Get application version
	 *
	 * @returns
	 */
	async version(): Promise<string | null> {
		return fetchAPI<string>('GET', '/api/version')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				if (result.status === 404) {
					return 'dev';
				}
				return result.data.trim();
			});
	}

	/**
	 * GET /api/stats
	 *
	 * Get server responses statistics
	 *
	 * @returns ApiResponseTime
	 */
	async stats(): Promise<ApiResponseTime | null> {
		return fetchAPI<ApiResponseTime>('GET', '/api/stats')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}

	/**
	 * GET /api/optional_features
	 *
	 * Get server optional features
	 *
	 * @returns Array<string>
	 */
	async optionalFeatures(): Promise<OptionalFeatures | null> {
		return fetchAPI<OptionalFeatures>('GET', '/api/optional_features')
			.catch((e: FetchError) => null)
			.then((result) => {
				if (!result) {
					return null;
				}

				return result.data;
			});
	}
}
