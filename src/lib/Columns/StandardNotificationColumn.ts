import { tick } from 'svelte';
import { get } from 'svelte/store';

import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { ContentManager } from '../Managers/ContentManager';
import { ColumnLinkType, StandardColumn } from './StandardColumn';
import type { StandardColumnType } from './TootDeckColumnTypes/StandardColumnType';

import { debugLogger } from '../debugLogger';
import { errorModal } from '../errorModal';
import type { IndexedArray } from '../IndexedArray';
import { sleep } from '../Utils/js/sleep';

import type { ParsedNotification } from '../../types/contentParsed';
import type {
	StandardNotificationColumnParams,
	StandardNotificationColumnParamsStore,
} from './types/options';
import type { NextContent } from './types/return';
import type { FindFunct } from './types/content';
import type { NotificationsOptional } from '../APIs/MirrorAPI/types';
import { Entity } from '../../types/mastodonEntities';

export abstract class StandardNotificationsColumn extends StandardColumn<ParsedNotification> {
	constructor(
		account: TootDeckAccount,
		kind: StandardColumnType,
		id: number,
		params: StandardNotificationColumnParams
	) {
		if (params.options.group === undefined) {
			params.options.group = false;
		}
		if (params.options.types === undefined) {
			params.options.types = [];
		}

		super(account, kind, id, params);
	}

	override get params(): StandardNotificationColumnParamsStore {
		return this._params as StandardNotificationColumnParamsStore;
	}

	protected get types() {
		return get(this.params.options).types;
	}

	protected get group() {
		return get(this.params.options).group;
	}

	/**
	 * return `true` when content has been removed
	 */
	limitContent(content: IndexedArray<ParsedNotification>): boolean {
		if (!this._top || content.length <= this._size_limit + 20) {
			return false;
		}

		for (let i = content.length; i > this._size_limit; i--) {
			const notification = content.deleteLast();
			ContentManager.Notification.removeOwner(this, notification.id);
		}

		if (!get(this._performance_mode)) {
			this._performance_mode.set(true);
			setTimeout(async () => {
				await tick();
				this._performance_mode.set(false);
			}, 1000);
		}

		return true;
	}

	/**
	 * Types
	 */
	// #region

	/**
	 * `type` will never be "TootDeck:btn:previous"
	 */
	protected isSubscribedType(type: Entity.NotificationType | 'TootDeck:btn:previous') {
		if (this.types.length === 0) {
			return true;
		}

		return this.types.find((t) => t === type) !== undefined;
	}

	async reload() {
		const request = await this.account.mirrorAPI.Notifications.getAll({
			types: this.types,
		}).then(
			(r) =>
				structuredClone(r)
					?.reverse()
					.map((n) => {
						if (n.status) {
							n.status = { ...n.status, animateIn: false, animateOut: true };
						}
						return { ...n, notify: false, animateIn: false, animateOut: true };
					}) ?? []
		);

		this._content.update((v) => {
			v.forEach((content) => {
				this.removeOwnership(content);
			});

			this._quote_ownership.forEach((_, id) => this.removeQuote(id));

			v.clear();

			request?.forEach((notification) => {
				v.setTop(ContentManager.Notification.store(this, notification));
			});

			if (this.group) {
				this.groupNotifications(v);
			}

			return v;
		});
	}

	changeTypes(types: Array<Entity.NotificationType>) {
		this.params.options.update((v) => {
			v.types = types;
			return v;
		});
	}

	// #endregion

	/**
	 * Group notifcations
	 */
	// #region

	private isGroupable(type: Entity.NotificationType | 'TootDeck:btn:previous') {
		return (
			type === Entity.NotificationType.Favourite ||
			type === Entity.NotificationType.Reblog ||
			type === Entity.NotificationType.ReactionChuckya ||
			type === Entity.NotificationType.ReactionPleroma
		);
	}

	protected groupNotifications(content: IndexedArray<ParsedNotification>) {
		const array = content.get();

		for (let i = content.length - 1; i >= 0; i--) {
			const current = array[i];
			if (this.isGroupable(current.type)) {
				const previous = array.findLast(
					(previous) =>
						previous.type === current.type &&
						previous.status!.id === current.status!.id &&
						previous.id !== current.id
				);
				if (previous) {
					current.accounts.forEach((account) => {
						if (!previous.accounts.find((a) => a.id === account.id)) {
							previous.accounts.push(account);
						}
					});

					content.delete(current.id);
				}
			}
		}
	}

	/**
	 * Return `true` if the notification already exist and is already merged
	 */
	protected groupSkipNotification(
		content: Array<ParsedNotification>,
		notification: Entity.Notification | ParsedNotification
	): boolean {
		return !!content.find(
			(n) =>
				n.status?.id === notification.status?.id &&
				n.type === notification.type &&
				n.accounts.length !== 1 &&
				n.accounts.find(
					(a) =>
						a.id ===
						((notification as Entity.Notification).account?.id ??
							(notification as ParsedNotification).accounts[0].id)
				)
		);
	}

	// #endregion

	private checkOlder(
		first: ParsedNotification | undefined,
		new_notification: ParsedNotification
	): boolean {
		if (first) {
			const _old = new Date(first.created_at);
			const _new = new Date(new_notification.created_at);
			const older = _old.valueOf() > _new.valueOf();
			if (older) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Compare created_at and insert `ParsedNotification` in the right index
	 *
	 * @param content `IndexedArray<ParsedNotification>`
	 * @param notification `ParsedNotification`
	 * @returns `true` if `status` has been inserted
	 */
	protected reorder(
		content: IndexedArray<ParsedNotification>,
		notification: ParsedNotification
	): void {
		let i = 0;
		for (const old of content.get()) {
			if (this.checkOlder(old, notification)) {
				i++;
				continue;
			}

			content.setAt(notification, i);
			return;
		}

		content.setAt(notification, i);
	}

	/**
	 * next content
	 */
	// #region

	async findNextContent(
		findFunct: FindFunct<Entity.Notification, NotificationsOptional>
	): Promise<void> {
		if (this._refreshing) {
			return;
		}
		this._refreshing = true;

		const id = this.first_content_id;
		if (!id) {
			this.processQueuedEvents();
			return;
		}

		const response = await findFunct({
			limit: this._fetch_limit,
			types: this.types,
		});
		if (!response) {
			errorModal('Failed to get next notifications.', undefined, true);
			this.processQueuedEvents();
			return;
		}

		const data = this.getResponse(ColumnLinkType.Top, response);

		const group = get(this.params.options).group;

		// Previous content button only created for supported columns
		if (!this._links) {
			const end = get(this._content)
				.slice(0, this._fetch_limit)
				.reverse()
				.find((x) => data.find((y) => x.id === y.id));
			if (!end) {
				this._content.update((content) => {
					content.setTop(this.previousContentBtn());

					if (group) {
						this.groupNotifications(content);
					}

					return content;
				});
			}
		}

		data.reverse();

		while (data.length) {
			const notifications = data.splice(0, 3);
			this._content.update((content) => {
				const array = content.get();

				notifications.forEach((notification) => {
					if (group && this.groupSkipNotification(array, notification)) {
						return;
					}

					const parsed = ContentManager.Notification.store(this, notification);
					content.setTop(parsed);

					if (group) {
						this.groupNotifications(content);
					}
				});

				return content;
			});
			await sleep(500);
		}

		this.processQueuedEvents();
	}

	abstract nextContent(): Promise<void>;

	// #endregion

	/**
	 * previous content
	 */
	// #region

	protected previousContentBtn(): ParsedNotification {
		this._previous_btn_uuid = window.crypto.randomUUID();
		return {
			accounts: [this._account.entity],
			animateIn: false,
			animateOut: false,
			created_at: new Date().toLocaleDateString(),
			id: this._previous_btn_uuid,
			notify: false,
			status: null,
			type: 'TootDeck:btn:previous',
			uuid: window.crypto.randomUUID(),
		};
	}

	protected async findImmediatelyPreviousContent(
		findFunct: FindFunct<Entity.Notification, NotificationsOptional>
	): Promise<void> {
		const content = get(this._content);
		const before_btn_index = (content.findIndex(this._previous_btn_uuid) ?? 0) - 1;
		const id = content.get()[before_btn_index]?.id;
		if (!id) {
			return;
		}

		const response = await findFunct({
			max_id: id,
			limit: this._fetch_limit,
			types: this.types,
		});
		if (!response) {
			errorModal('Failed to get previous notifications.', undefined, true);
			return;
		}

		const data = this.getResponse(ColumnLinkType.Middle, response);
		const group = get(this.params.options).group;

		this._content.update((content) => {
			const array = content.get();

			data.forEach((notification) => {
				const parsed = ContentManager.Notification.store(this, notification);
				const index = content.findIndex(parsed.id);
				const btn_index = content.findIndex(this._previous_btn_uuid);
				if (index === -1) {
					content.setAt(parsed, btn_index - 1);
					return;
				}

				if (index !== -1 && btn_index !== -1) {
					const btn = array[btn_index];
					this._previous_btn_uuid = '';
					content.delete(btn.id, btn_index);
					content.setAt(parsed, btn_index);
				}
			});

			if (group) {
				this.groupNotifications(content);
			}

			return content;
		});
	}

	abstract immediatelyPreviousContent(): Promise<void>;

	protected async findPreviousContent(
		findFunct: FindFunct<Entity.Notification, NotificationsOptional>
	): Promise<NextContent> {
		const id = this.last_content_id;
		if (!id) {
			return true;
		}

		const response = await findFunct({
			max_id: id,
			limit: this._fetch_limit,
			types: this.types,
		});
		if (!response) {
			errorModal('Failed to get previous notifications.', undefined, true);
			return 'DEAD';
		}

		const data = this.getResponse(ColumnLinkType.Bottom, response);
		const group = get(this.params.options).group;

		this._content.update((content) => {
			data.forEach((notification) => {
				const parsed = ContentManager.Notification.store(this, notification);
				content.setBottom(parsed);
			});

			if (group) {
				this.groupNotifications(content);
			}

			return content;
		});

		return data.length !== this._fetch_limit;
	}

	abstract previousContent(): Promise<NextContent>;

	// #endregion

	removeOwnership(content: ParsedNotification) {
		if (content.status) {
			// prettier-ignore
			debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${content.status.id}`);
			ContentManager.Status.removeOwner(this as any, content.status.id);
		}

		// prettier-ignore
		debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${content.id}`);
		ContentManager.Notification.removeOwner(this, content.id);
	}
}
