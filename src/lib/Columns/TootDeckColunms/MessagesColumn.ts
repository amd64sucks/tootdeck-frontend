// import Conversation from '../../../components/Message/Conversation.svelte';

import { Account } from '../../../stores/Account/Accounts';

import { MessagesColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';

// TODO
export class MessagesColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, MessagesColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Messages + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Message:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
	}

	private event(content: IndexedArray<ParsedStatus>, payload: any): void {
		// if (index === -1) {
		// 	this.playSound();
		// }
	}

	// override component(): ConstructorOfATypedSvelteComponent {
	// 	return Conversation;
	// }

	async nextContent(): Promise<void> {}

	async immediatelyPreviousContent(): Promise<void> {}

	async previousContent(): Promise<NextContent> {
		return 'DEAD';
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		super.destroy();
	}
}
