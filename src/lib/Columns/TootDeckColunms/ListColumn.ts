import { Account } from '../../../stores/Account/Accounts';
import type { LayoutColumnList } from '../../../stores/Columns/Layout';

import { ListsColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEventList } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';
import type { IndexedArray } from '../../IndexedArray';
import type { OptionalFull } from '../../APIs/MirrorAPI/types';

import type { StandardColumnListParams, StandardColumnListParamsStore } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';

export class ListColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnListParams, id: number) {
		const account = Account.get(args.account);

		super(account, ListsColumnType, id, args);

		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.List + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (
					!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI) ||
					!payload.list_id ||
					payload.list_id !== this.list.id
				) {
					return;
				}

				switch (source) {
					case TootDeckEvents.List:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.List, {
			list_id: this.list.id,
		});
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEventList): void {
		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			const index = content.findIndex(status.id);
			if (index === -1) {
				if (status.reblog) {
					ContentManager.Status.addOwner(this, status.reblog.id);
				}
				ContentManager.Status.addOwner(this, status.id);

				this.playSound();
			}

			content.update(status, index);
		});
	}

	private get list() {
		return this.params.list;
	}

	override get params(): StandardColumnListParamsStore {
		return this._params as any;
	}

	override get name() {
		return this.list.title;
	}

	override get props(): LayoutColumnList<ListColumn, ParsedStatus> {
		const props = super.props;
		return { ...props, instance: this, list: this.list };
	}

	async nextContent(): Promise<void> {
		return this.findNextContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getList(this.list.id, params)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getList(this.list.id, params)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getList(this.list.id, params)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.List, {
			list_id: this.list.id,
		});
		super.destroy();
	}
}
