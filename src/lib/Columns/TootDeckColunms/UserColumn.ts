import { get } from 'svelte/store';

import { Account } from '../../../stores/Account/Accounts';

import { UserColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';
import { getHandleFromURL } from '../../Utils/handle/getHandleFromURL';
import type { OptionalFull } from '../../APIs/MirrorAPI/types';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnUserParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';

export class UserColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnUserParams, id: number) {
		const account = Account.get(args.account);

		super(account, UserColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.User + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Status:
					case TootDeckEvents.User:
					case TootDeckEvents.ColumnUser:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.User);
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>): void {
		if (!statuses.data.length) {
			return;
		}

		const params = get(this.params.options) as any as Omit<StandardColumnUserParams, 'options'>;

		statuses.data.forEach((status) => {
			if (params?.user) {
				const status_handle = getHandleFromURL(status.account.url);
				const column_handle = getHandleFromURL(params.user.url);

				if (
					status_handle.username !== column_handle.username &&
					status_handle.domain !== column_handle.domain
				) {
					return;
				}
			} else if (status.account.id !== this._account.entity.id) {
				return;
			}

			const index = content.findIndex(status.id);
			if (index !== -1) {
				content.update(status, index);
				return;
			}

			if (status.reblog) {
				ContentManager.Status.addOwner(this, status.reblog.id);
			}
			ContentManager.Status.addOwner(this, status.id);

			this.reorder(content, status);
		});
	}

	async nextContent(): Promise<void> {
		return this.findNextContent((params: OptionalFull) =>
			this._account.mirrorAPI.Accounts.getStatuses(this._account.entity.id, params)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Accounts.getStatuses(this._account.entity.id, params)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Accounts.getStatuses(this._account.entity.id, params)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.User);
		super.destroy();
	}
}
