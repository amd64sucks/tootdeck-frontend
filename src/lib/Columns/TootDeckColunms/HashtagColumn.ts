import { get } from 'svelte/store';

import { Account } from '../../../stores/Account/Accounts';

import { TootDeckColumnIdentifier, HashtagColumnType } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';
import type { OptionalFull } from '../../APIs/MirrorAPI/types';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnHashtagParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';

export class HashtagColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;
	private readonly hashtag: string;

	constructor(args: StandardColumnHashtagParams, id: number) {
		const account = Account.get(args.account);

		super(account, HashtagColumnType, id, args);

		// Override column title only if not already overrided
		const params = get(this._params.options);
		if (!params.override_title.toggle && !params.override_title.value) {
			params.override_title.toggle = true;
			params.override_title.value = {
				title: args.hashtag,
				handle: account.mirrorAPI.handle_string,
			};
		}

		this.hashtag = args.hashtag;
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Hashtag + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Status:
					case TootDeckEvents.Hashtag:
					case TootDeckEvents.HashtagColumn:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.Hashtag, {
			hashtag: args.hashtag,
		});
	}

	get bindToHashtag() {
		return this.hashtag;
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>): void {
		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			if (!status.tags.find((tag) => tag.name === this.hashtag)) {
				return;
			}

			const index = content.findIndex(status.id);
			if (index !== -1) {
				content.update(status, index);
				return;
			}

			if (status.reblog) {
				ContentManager.Status.addOwner(this, status.reblog.id);
			}
			ContentManager.Status.addOwner(this, status.id);

			this.reorder(content, status);
		});
	}

	async nextContent(): Promise<void> {
		return this.findNextContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getTag(this.hashtag, params)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getTag(this.hashtag, params)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent((params: OptionalFull) =>
			this._account.mirrorAPI.Timeline.getTag(this.hashtag, params)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.Hashtag, {
			hashtag: this.hashtag,
		});
		super.destroy();
	}
}
