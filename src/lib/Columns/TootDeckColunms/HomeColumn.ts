import { Account } from '../../../stores/Account/Accounts';

import { HomeColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import type { IndexedArray } from '../../IndexedArray';
import { WebsocketSubscriptionsManager } from '../../Websocket/WebsocketManager';

import type { StandardColumnParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';
import { AvailableSubscription } from '../../Websocket/types';

export class HomeColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, HomeColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Home + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Status:
						this.queueEvent((content) => this.statusEvent(content, payload));
						return;
					case TootDeckEvents.User:
					case TootDeckEvents.ColumnHome:
						this.queueEvent((content) => this.homeEvent(content, payload));
						return;
					default:
						return;
				}
			}
		);
		WebsocketSubscriptionsManager.subscribeColumn(this, AvailableSubscription.User);
	}

	private statusEvent(
		content: IndexedArray<ParsedStatus>,
		statuses: PayloadEvent<ParsedStatus>
	): void {
		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			const index = status.reblog
				? content.findIndex(status.reblog.id)
				: content.findIndex(status.id);
			if (index === -1) {
				return;
			}

			content.update(status.reblog ?? status, index);

			// if (status.reblog && status.account.id === this._account.entity.id) {
			// 	this.appendToContent(content, status);
			// }
		});
	}

	private homeEvent(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>) {
		statuses.data.forEach((status) => {
			this.appendToContent(content, status);
		});
	}

	private appendToContent(content: IndexedArray<ParsedStatus>, status: ParsedStatus) {
		const index = content.findIndex(status.id);
		if (index === -1) {
			if (status.reblog) {
				ContentManager.Status.addOwner(this, status.reblog.id);
			}
			ContentManager.Status.addOwner(this, status.id);

			this.playSound();
		}

		if (index !== -1) {
			content.update(status, index);
			return;
		}

		this.reorder(content, status);
	}

	async nextContent(): Promise<void> {
		return this.findNextContent(
			this._account.mirrorAPI.Timeline.getHome.bind(this._account.mirrorAPI)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent(
			this._account.mirrorAPI.Timeline.getHome.bind(this._account.mirrorAPI)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent(
			this._account.mirrorAPI.Timeline.getHome.bind(this._account.mirrorAPI)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		WebsocketSubscriptionsManager.unsubscribeColumn(this, AvailableSubscription.User);
		super.destroy();
	}
}
