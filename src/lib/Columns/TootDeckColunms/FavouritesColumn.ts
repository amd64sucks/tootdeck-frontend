import { Account } from '../../../stores/Account/Accounts';

import { StandardStatusesColumn } from '../StandardStatusesColumn';
import { FavouritesColumnType, TootDeckColumnIdentifier } from '../TootDeckColumnTypes';
import { EventsManager, type SubscriptionHandle } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import type { PayloadEvent } from '../../Events/types';
import { ContentManager } from '../../Managers/ContentManager';
import type { IndexedArray } from '../../IndexedArray';

import type { StandardColumnParams } from '../types/options';
import type { ParsedStatus } from '../../../types/contentParsed';
import type { NextContent } from '../types/return';

export class FavouritesColumn extends StandardStatusesColumn {
	private readonly mutation_event: SubscriptionHandle;

	constructor(args: StandardColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, FavouritesColumnType, id, args);
		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Favourites + ':' + id + ':' + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Favourites:
					case TootDeckEvents.Status:
						this.queueEvent((content) => this.event(content, payload));
						return;
					default:
						return;
				}
			}
		);

		this._using_links = true;
	}

	private event(content: IndexedArray<ParsedStatus>, statuses: PayloadEvent<ParsedStatus>): void {
		if (statuses.links) {
			if (!this._links) {
				this._links = {
					top: statuses.links,
					middle: statuses.links,
					bottom: statuses.links,
				};
			} else {
				this._links = { ...this._links, top: statuses.links };
			}
		}

		if (!statuses.data.length) {
			return;
		}

		statuses.data.forEach((status) => {
			const index = content.findIndex(status.id);

			if (status.favourited) {
				if (index === -1) {
					ContentManager.Status.addOwner(this, status.id);
				}
				content.update(status, index);
				return;
			}

			if (!status.favourited && index !== -1) {
				ContentManager.Status.removeOwner(this, status.id);
				content.delete(status.id, index);
			}
		});
	}

	async nextContent(): Promise<void> {
		return this.findNextContent(
			this._account.mirrorAPI.Accounts.getFavourites.bind(this._account.mirrorAPI)
		);
	}

	async immediatelyPreviousContent(): Promise<void> {
		return this.findImmediatelyPreviousContent(
			this._account.mirrorAPI.Accounts.getFavourites.bind(this._account.mirrorAPI)
		);
	}

	async previousContent(): Promise<NextContent> {
		return this.findPreviousContent(
			this._account.mirrorAPI.Accounts.getFavourites.bind(this._account.mirrorAPI)
		);
	}

	destroy(): void {
		EventsManager.unsubscribe(this.mutation_event);
		super.destroy();
	}
}
