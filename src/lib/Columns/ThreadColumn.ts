import { get, writable, type Writable, type Readable } from 'svelte/store';

import { Account } from '../../stores/Account/Accounts';
import { rdOptionsBehaviorColumnThreadInsertStatusBeforeSelected } from '../../stores/Options';

import {
	ThreadColumnType as ThreadColumnType,
	TootDeckColumnIdentifier,
} from './TootDeckColumnTypes';
import { StandardColumn } from './StandardColumn';
import { ContentManager } from '../Managers/ContentManager';
import { EventsManager, type SubscriptionHandle } from '../Events/EventsManager';
import { TootDeckEvents } from '../Events/TootDeckEvents';
import type { PayloadEvent } from '../Events/types';
import { deleteNotify, linkStatuses } from '../ComponentFunctions/thread/generateThreadContent';
import { updateObject } from '../Utils/js/updateObject';
import { debugLogger } from '../debugLogger';
import type { OptionalFull } from '../APIs/MirrorAPI/types';

import type { ThreadColumnParams } from './types/options';
import type { PartialThreadStatus, ThreadStatus } from '../../types/thread';
import type { ParsedStatus } from '../../types/contentParsed';
import type { NextContent } from './types/return';
import type { IColumn } from './types/interfaces';

export class ThreadColumn extends StandardColumn<ThreadStatus> {
	private readonly mutation_event: SubscriptionHandle;

	private thread_status_id: number = 0;
	private readonly status_id: string | undefined;
	private readonly parent_column: IColumn | undefined;
	private readonly loading_context: Writable<boolean> = writable(false);

	constructor(args: ThreadColumnParams, id: number) {
		const account = Account.get(args.account);

		super(account, ThreadColumnType, id, args);

		this.status_id = args.status_id;
		this.parent_column = args.main_column;

		this.mutation_event = EventsManager.subscribe(
			TootDeckColumnIdentifier.Thread + ':' + id + account.mirrorAPI.handle_string,
			(source, payload) => {
				if (!Object.is(this._account.mirrorAPI, payload.account.mirrorAPI)) {
					return;
				}

				switch (source) {
					case TootDeckEvents.Status:
					case TootDeckEvents.User:
						this.event(payload);
						return;
					default:
						return;
				}
			}
		);
	}

	get bindToStatusID() {
		return this.status_id ?? undefined;
	}

	get parentColumn() {
		return this.parent_column;
	}

	get loading(): Readable<boolean> {
		return this.loading_context;
	}

	set loading(v: boolean) {
		this.loading_context.set(v);
	}

	private event(statuses: PayloadEvent<ParsedStatus>) {
		const content = get(this._content);

		statuses.data.forEach((status) => {
			const index = content.get().findIndex((x) => x.status.id === status.id);
			let replying_to_index: number = -1;

			if (
				// Not in thread
				index === -1 &&
				// Replying to a status
				status.in_reply_to_id &&
				// Index of the replied status
				(replying_to_index = content
					.get()
					.findIndex((x) => x.status.id === status.in_reply_to_id)) !== -1
			) {
				this._content.update((v) => {
					const value: ThreadStatus = {
						id: (this.thread_status_id++).toString(),
						selected: false,
						status: status,
						lines: {
							up: true,
							down: false,
						},
					};

					if (
						get(rdOptionsBehaviorColumnThreadInsertStatusBeforeSelected) ||
						v.get().findIndex((x) => x.selected) <= replying_to_index
					) {
						ContentManager.Status.addOwner(this as any, status.id);
						v.setAt(value, replying_to_index + 1);
					}

					const to_update = v.get();
					linkStatuses(to_update);

					return v;
				});

				return;
			}

			if (index !== -1) {
				this._content.update((v) => {
					v.getFromIndex(status.id, index).status = status;
					return v;
				});
			}
		});
	}

	private updateThread(partial: PartialThreadStatus<ParsedStatus>, selected: boolean = false) {
		let entity = get(this._content)
			.get()
			.find((x) => x.status.id === partial.status.id);
		if (entity) {
			updateObject(entity.status, partial);
		} else {
			entity = {
				id: (this.thread_status_id++).toString(),
				selected,
				...partial,
			};
		}

		return entity;
	}

	override removeContent(content_id: string) {
		this._content.update((v) => {
			const content = v.get().find((x) => x.status.id === content_id);
			if (content) {
				const status = content.status;
				const del = deleteNotify(status.account, status);
				// @ts-ignore
				content.status = del.status;
				// v.set(content_id, content);
			}
			return v;
		});
	}

	append(partial: Array<PartialThreadStatus<ParsedStatus>>): void;
	append(partial: PartialThreadStatus<ParsedStatus>, selected: boolean): void;
	append(
		partial: PartialThreadStatus<ParsedStatus> | Array<PartialThreadStatus<ParsedStatus>>,
		selected: boolean = false
	): void {
		this._content.update((v) => {
			if (Array.isArray(partial)) {
				partial.forEach((p) => {
					const updated = this.updateThread(p, false);
					v.setBottom(updated);
				});
			} else {
				const updated = this.updateThread(partial, selected);
				v.setBottom(updated);
			}

			return v;
		});
	}

	/**
	 * Unused in this column
	 */
	// #region

	/**
	 * @deprecated function unused
	 */
	override setTop(value: boolean): void {}

	/**
	 * @deprecated function unused
	 */
	limitContent(): boolean {
		return false;
	}

	// next content

	/**
	 * @deprecated function unused
	 */
	findNextContent(findFunct: (params: OptionalFull) => Promise<any | null>): Promise<void> {
		return undefined as any;
	}

	/**
	 * @deprecated function unused
	 */
	nextContent(): Promise<void> {
		return undefined as any;
	}

	// previous content

	/**
	 * @deprecated function unused
	 */
	previousContentBtn(): ThreadStatus {
		return undefined as any;
	}

	/**
	 * @deprecated function unused
	 */
	protected findImmediatelyPreviousContent(
		findFunct: (params: OptionalFull) => Promise<any | null>
	): Promise<void> {
		return undefined as any;
	}

	/**
	 * @deprecated function unused
	 */
	immediatelyPreviousContent(): Promise<void> {
		return undefined as any;
	}

	/**
	 * @deprecated function unused
	 */
	protected findPreviousContent(
		findFunct: (params: OptionalFull) => Promise<any | null>
	): Promise<NextContent> {
		return undefined as any;
	}

	/**
	 * @deprecated function unused
	 */
	previousContent(): Promise<NextContent> {
		return undefined as any;
	}

	// #endregion

	removeOwnership(content: ThreadStatus) {
		const status = content.status;

		if (status.reblog) {
			// prettier-ignore
			debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${status.reblog.id}`);
			ContentManager.Status.removeOwner(this, status.reblog.id);
		}

		// prettier-ignore
		debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${status.id}`);
		ContentManager.Status.removeOwner(this, status.id);
	}

	clear(remove_ownership: boolean = true) {
		if (remove_ownership) {
			get(this._content).forEach((content) => {
				const status = content.status;
				// prettier-ignore
				debugLogger(`${this.identifier}:${this._account.entity.username} Removing content ${status.id}`);
				ContentManager.Status.removeOwner(this as any, status.id);
			});
		}

		this._content.update((v) => {
			v.clear();
			return v;
		});
	}

	destroy(): void {
		debugLogger(`${this.identifier}:${this._account.entity.username} is destroying..`);

		this.clear();
		EventsManager.unsubscribe(this.mutation_event);

		debugLogger(`${this.identifier}:${this._account.entity.username} COMPLETE`);
	}
}
