import { writable, get, type Writable } from 'svelte/store';

import Column from '../../components/Column/Column.svelte';

import { type TootDeckAccount } from '../../stores/Account/Accounts';
import { stLayout, type LayoutColumn } from '../../stores/Columns/Layout';
import { rdOptionsBehaviorColumnMaxStatuses } from '../../stores/Options';
import { NotificationModalType, notificationModal } from '../../stores/NotificationsModal';
import { stInitialized } from '../../stores/loading';

import type { StandardColumnType } from './TootDeckColumnTypes/StandardColumnType';
import { ContentManager } from '../Managers/ContentManager';
import { IndexedArray } from '../IndexedArray';
import { debugLogger } from '../debugLogger';
import { SoundsLibrary } from '../SoundsLibrary';
import { TootDeckConfig } from '../TootDeckConfig';
import { type Links, type ResponseLinks } from '../Utils/parseResponseLInk';
import { CustomAudio } from '../Utils/js/audio';

import type { IColumn } from './types/interfaces';
import type { StandardColumnParams, StandardColumnParamsStore } from './types/options';
import type { NextContent } from './types/return';
import type { FindFunct } from './types/content';

export enum ColumnLinkType {
	Top = 1,
	Middle = 2,
	Bottom = 3,
}

export abstract class IStandardColumn implements IColumn {
	protected readonly _id: number;
	protected readonly _name: string;
	protected readonly _icon: string;
	protected readonly _params: StandardColumnParamsStore;
	protected readonly _identifier: string;
	protected readonly _initialized: Writable<boolean> = writable(false);

	protected _sound: CustomAudio | undefined;

	constructor(kind: StandardColumnType, id: number, params: StandardColumnParams) {
		this._id = id;
		this._name = kind.name;
		this._icon = kind.icon;
		this._identifier = kind.identifier;
		this._params = {
			...params,
			options: writable(params.options),
		};

		if (params.options.sound_uuid) {
			this.sound = params.options.sound_uuid;
		}

		this._params.options.subscribe((v) => {
			if (get(stInitialized) && get(this._initialized)) {
				TootDeckConfig.saveLayout();
			}
		});
	}

	get id() {
		return this._id;
	}

	get name(): string {
		return this._name;
	}

	get icon() {
		return this._icon;
	}

	get identifier() {
		return this._identifier;
	}

	get params() {
		return this._params;
	}

	get component(): ConstructorOfATypedSvelteComponent {
		return Column;
	}

	abstract get props(): LayoutColumn<IStandardColumn, any>;

	get initialized() {
		return this._initialized;
	}

	/**
	 * Sound
	 */
	// #region

	get sound() {
		return get(this._params.options).sound_uuid;
	}

	set sound(uuid: string | null) {
		this._params.options.update((options) => {
			options.sound_uuid = uuid;
			return options;
		});
		const initialized = get(this._initialized);

		if (!uuid) {
			this._sound = undefined;

			if (initialized) {
				TootDeckConfig.save();
			}
			return;
		}

		const sound = SoundsLibrary.get(uuid);
		if (!sound) {
			notificationModal.create(
				NotificationModalType.Warn,
				`Unable to find sound for column ${this._name}:${this._params.account.username}@${this._params.account.domain}.`,
				true
			);
			return;
		}

		this._sound = new CustomAudio(sound.value);
		this._sound.volume = 0.1;

		if (initialized) {
			TootDeckConfig.save();
		}
	}

	async playSound() {
		if (get(this._initialized) && get(this._params.options).sound_uuid) {
			return this._sound!.play();
		}
	}

	// #endregion

	abstract clear(): void;

	abstract destroy(): void;
}

export abstract class StandardColumn<T> extends IStandardColumn {
	protected readonly _account: TootDeckAccount;
	protected readonly _content = writable(new IndexedArray<T>());
	protected readonly _event_queue: Array<(content: IndexedArray<any>) => void> = [];
	protected _quote_ownership = new Map<string, number>();
	protected _top: boolean = true;
	protected _scroll_timeout: NodeJS.Timeout = setTimeout(() => {});
	// prettier-ignore
	protected _links: { top?: Links; middle?: Links; bottom?: Links; } | null = null;
	protected _using_links: boolean = false;
	protected _refreshing: boolean = false;
	protected _previous_btn_uuid: string = '';
	protected _size_limit: number = get(rdOptionsBehaviorColumnMaxStatuses);
	protected _fetch_limit = 20;

	// Performance mode
	private _count: number = 0;
	protected _performance_mode = writable(false);
	private _performance_timeout: NodeJS.Timeout | null = null;
	private readonly _interval = setInterval(() => {
		const performance_mode = get(this._performance_mode);

		if (this._count > 20) {
			debugLogger(
				this._identifier,
				'is refreshing to often,',
				this._count,
				'statuses in the last 10s'
			);

			if (!performance_mode && !this._performance_timeout) {
				this._performance_mode.set(true);
			}
			this._count = 0;

			return;
		}

		if (performance_mode && !this._performance_timeout) {
			this._performance_timeout = setTimeout(() => {
				this._performance_mode.set(false);

				this._performance_timeout = null;
			}, 60000);
		}
		this._count = 0;
	}, 10000);

	constructor(
		account: TootDeckAccount,
		kind: StandardColumnType,
		id: number,
		params: StandardColumnParams
	) {
		if (params.options.collapsed === undefined) {
			params.options.collapsed = false;
		}
		if (params.options.sound_uuid === undefined) {
			params.options.sound_uuid = null;
		}
		if (params.options.override_title === undefined) {
			params.options.override_title = {
				toggle: false,
				value: null,
			};
		}

		super(kind, id, params);
		this._account = account;
		rdOptionsBehaviorColumnMaxStatuses.subscribe((v) => (this._size_limit = v));
	}

	/**
	 * Getters
	 */
	// #region

	get props(): LayoutColumn<StandardColumn<T>, T> {
		return {
			icon: this.icon,
			title: this.name,
			identifier: this.identifier,
			options: this.params.options,
			using_links: this.using_links,
			account: this.account,
			content: this.content,
			performance_mode: this._performance_mode,
			instance: this,
		};
	}

	get account() {
		return this._account;
	}

	get content(): Writable<IndexedArray<T>> {
		return this._content;
	}

	get using_links() {
		return this._using_links;
	}

	// #endregion

	updateContent() {
		this._content.update((v) => v);
	}

	removeContent(content_ids: string | Array<string>) {
		this._content.update((v) => {
			if (Array.isArray(content_ids)) {
				content_ids.forEach((id) => v.delete(id));
			} else {
				v.delete(content_ids);
			}

			return v;
		});
	}

	setTop(value: boolean) {
		this._top = value;

		clearTimeout(this._scroll_timeout);

		if (this._top) {
			this._scroll_timeout = setTimeout(() => {
				this._content.update((content) => {
					this.limitContent(content);

					return content;
				});
			}, 15 * 1000);
		}
	}

	/**
	 * store payload if column is refreshing due to websocket reconnection
	 *
	 * otherwise just call the callback
	 */
	protected queueEvent(callback: (content: IndexedArray<T>) => void) {
		if (this._refreshing) {
			debugLogger('queueEvent: Column is refreshing, caching payload');
			this._event_queue.push(callback);
		} else {
			this._content.update((content) => {
				callback(content);
				this.limitContent(content);
				this._count++;

				return content;
			});
		}

		if (!get(this._initialized)) {
			this._initialized.set(true);
		}
	}

	/**
	 * Execute stored payloads
	 */
	protected processQueuedEvents() {
		debugLogger('queueEvent: Column is done refreshing, processing cached payloads');

		this._content.update((content) => {
			this._event_queue.forEach((callback) => {
				callback(content);
			});
			this.limitContent(content);

			return content;
		});

		this._refreshing = false;

		debugLogger('queueEvent: processing done, restoring normal behavior');
	}

	/**
	 * Remove excess content when `_content` size is bigger than `_size_limit`
	 * return `true` when content has been removed
	 */

	abstract limitContent(content: IndexedArray<T>): boolean;

	/**
	 * Content utils
	 */
	// #region

	protected get first_content_id() {
		if (this._using_links) {
			return this._links?.top?.prev?.min_id;
		} else {
			return get(this._content).first()?.id;
		}
	}

	protected get immediatly_last_content_id() {
		if (this._using_links) {
			return this._links?.middle?.next?.max_id;
		} else {
			const content = get(this._content);
			const before_btn_index = (content.findIndex(this._previous_btn_uuid) ?? 0) - 1;
			return content.get()[before_btn_index]?.id;
		}
	}

	protected get last_content_id() {
		if (this._using_links) {
			return this._links?.bottom?.next?.max_id;
		} else {
			return get(this._content).last()?.id;
		}
	}

	protected getResponse<K>(type: ColumnLinkType, response: Array<K> | ResponseLinks<Array<K>>) {
		if (Array.isArray(response)) {
			return response.map((c) => ({ ...c, animateIn: false, animateOut: true }));
		} else {
			if (response.links !== 'NONE') {
				switch (type) {
					case ColumnLinkType.Top:
						this._links = { ...this._links, top: response.links };
						break;
					case ColumnLinkType.Middle:
						this._links = { ...this._links, middle: response.links };
						break;
					case ColumnLinkType.Bottom:
						this._links = { ...this._links, bottom: response.links };
						break;
				}
			} else {
				switch (type) {
					case ColumnLinkType.Top:
						this._links = { ...this._links, top: undefined };
						break;
					case ColumnLinkType.Middle:
						this._links = { ...this._links, middle: undefined };
						break;
					case ColumnLinkType.Bottom:
						this._links = { ...this._links, bottom: undefined };
						break;
				}
			}

			return response.data.map((c) => ({ ...c, animateIn: false, animateOut: true }));
		}
	}

	// #endregion

	/**
	 * next content
	 */
	// #region

	abstract findNextContent(findFunct: FindFunct<any, any>): Promise<void>;

	abstract nextContent(): Promise<void>;

	// #endregion

	/**
	 * previous content
	 */
	// #region

	protected abstract previousContentBtn(): T;

	protected abstract findImmediatelyPreviousContent(
		findFunct: FindFunct<any, any>
	): Promise<void>;

	abstract immediatelyPreviousContent(): Promise<void>;

	protected abstract findPreviousContent(findFunct: FindFunct<any, any>): Promise<NextContent>;

	abstract previousContent(): Promise<NextContent>;

	// #endregion

	abstract removeOwnership(content: T): void;

	addQuoteOwership(status_id: string) {
		let get = this._quote_ownership.get(status_id);
		if (get) {
			this._quote_ownership.set(status_id, ++get);
			return;
		}

		ContentManager.Status.addOwner(this, status_id);
		this._quote_ownership.set(status_id, 1);
	}

	removeQuoteOwership(status_id: string) {
		let get = this._quote_ownership.get(status_id);
		if (!get) {
			return;
		}

		if (--get !== 0) {
			this._quote_ownership.set(status_id, get);
			return;
		}

		ContentManager.Status.removeOwner(this, status_id);
		this._quote_ownership.delete(status_id);
	}

	removeQuote(status_id: string) {
		let get = this._quote_ownership.get(status_id);
		if (!get) {
			return;
		}

		ContentManager.Status.removeOwner(this, status_id);
		this._quote_ownership.delete(status_id);
	}

	clear(): void {
		this._content.update((v) => {
			v.forEach((content) => {
				this.removeOwnership(content);
			});

			this._quote_ownership.forEach((_, id) => this.removeQuote(id));

			v.clear();
			return v;
		});
	}

	destroy(): void {
		debugLogger(`${this.identifier}:${this._account.entity.username} is destroying..`);

		this.clear();

		stLayout.update((v) => v.filter((x) => x.id !== this.id));

		debugLogger(`${this.identifier}:${this._account.entity.username} COMPLETE`);
	}
}
