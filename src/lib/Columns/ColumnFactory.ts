import { loadingUpdateState } from '../../stores/loading';

import type { IStandardColumn, StandardColumn } from './StandardColumn';
import {
	TootDeckColumnIdentifier,
	BookmarksColumnType,
	ThreadColumnType,
	FavouritesColumnType,
	FederatedTimelineColumnType,
	HomeColumnType,
	ListsColumnType,
	LocalTimelineColumnType,
	// MentionsColumnType,
	// MessagesColumnType,
	NotificationsColumnType,
	UserColumnType,
	ProfileColumnType,
	HashtagColumnType,
} from './TootDeckColumnTypes';
import { ListColumn } from './TootDeckColunms/ListColumn';
import { ThreadColumn } from './ThreadColumn';
import { ProfileColumn } from './TootDeckColunms/ProfileColumn';
import type { StandardNotificationsColumn } from './StandardNotificationColumn';
import type {
	StandardColumnProfileParms,
	StandardColumnListParams,
	StandardColumnParams,
	StandardNotificationColumnParams,
	ThreadColumnParams,
	StandardColumnHashtagParams,
} from './types/options';
import { HashtagColumn } from './TootDeckColunms/HashtagColumn';
import { HomeColumn } from './TootDeckColunms/HomeColumn';
// import { MessagesColumn } from './TootDeckColunms/MessagesColumn';
import { LocalTimelineColumn } from './TootDeckColunms/LocalTimelineColumn';
import { FederatedTimelineColumn } from './TootDeckColunms/FederatedTimelineColumn';
import { UserColumn } from './TootDeckColunms/UserColumn';
import { NotificationsColumn } from './TootDeckColunms/NotificationsColumn';
import { FavouritesColumn } from './TootDeckColunms/FavouritesColumn';
// import { MentionsColumn } from './TootDeckColunms/MentionsColumn';
import { BookmarksColumn } from './TootDeckColunms/BookmarksColumn';
import { errorModal } from '../errorModal';

interface ColumnConstructor {
	new (args: any, uid: number): IStandardColumn;
}

class ColumnFactory {
	static instance: ColumnFactory = new ColumnFactory();
	private column_id: number = 0;
	private readonly _constructors = new Map<String, ColumnConstructor>();

	create(
		column_identifier: TootDeckColumnIdentifier.Hashtag,
		data: StandardColumnHashtagParams
	): HashtagColumn;
	create(
		column_identifier: TootDeckColumnIdentifier.Profile,
		data: StandardColumnProfileParms
	): ProfileColumn;
	create(
		column_identifier: TootDeckColumnIdentifier.Thread,
		data: ThreadColumnParams
	): ThreadColumn;
	create(
		column_identifier: TootDeckColumnIdentifier.List,
		data: StandardColumnListParams
	): ListColumn;
	create(
		column_identifier: TootDeckColumnIdentifier.Notifications,
		data: StandardNotificationColumnParams
	): StandardNotificationsColumn;
	create(column_identifier: string, data: StandardColumnParams): StandardColumn<any>;
	create(column_identifier: string, data: any): any {
		const constr = this._constructors.get(column_identifier);
		if (!constr) {
			throw 'Unknown column identifier: ' + column_identifier;
		}

		return new constr(data, this.column_id++);
	}

	register(column_identifier: string, constr: ColumnConstructor) {
		if (this._constructors.has(column_identifier)) {
			throw 'Column identifier already in use: ' + column_identifier;
		}

		this._constructors.set(column_identifier, constr);
	}
}

export const columnFactory = ColumnFactory.instance;

let registered: boolean = false;

export function init() {
	if (registered) {
		return true;
	}

	loadingUpdateState('Registering columns');

	try {
		columnFactory.register(FederatedTimelineColumnType.identifier, FederatedTimelineColumn);
		columnFactory.register(LocalTimelineColumnType.identifier, LocalTimelineColumn);
		columnFactory.register(HomeColumnType.identifier, HomeColumn);
		columnFactory.register(UserColumnType.identifier, UserColumn);
		columnFactory.register(NotificationsColumnType.identifier, NotificationsColumn);
		columnFactory.register(ListsColumnType.identifier, ListColumn);
		columnFactory.register(FavouritesColumnType.identifier, FavouritesColumn);
		// columnFactory.register(MessagesColumnType.identifier, MessagesColumn);
		// columnFactory.register(MentionsColumnType.identifier, MentionsColumn);
		columnFactory.register(BookmarksColumnType.identifier, BookmarksColumn);
		columnFactory.register(ThreadColumnType.identifier, ThreadColumn);
		columnFactory.register(ProfileColumnType.identifier, ProfileColumn);
		columnFactory.register(HashtagColumnType.identifier, HashtagColumn);
		registered = true;
	} catch (e: any) {
		errorModal(`Unable to initialize columns`, ['columnFactory: ', e], true);
		return false;
	}

	return true;
}
