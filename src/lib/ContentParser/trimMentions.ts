import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { ParsedNode } from './flattenHTML';
import { ContentManager } from '../Managers/ContentManager';

import type { Entity } from '../../types/mastodonEntities';

function getReplyingAccounts(
	account: TootDeckAccount,
	status: Entity.Status
): Array<Entity.Mention> {
	const previous = status.in_reply_to_id
		? ContentManager.Status.get(account, status.in_reply_to_id)
		: null;
	let replying_to: Array<Entity.Mention> = [];
	if (previous) {
		// Back tracking method, accurate
		replying_to = status.mentions.filter(
			(current) =>
				current.id === previous.account.id ||
				previous.mentions.find((prev) => prev.id === current.id)
		);
	} else {
		// Fallback method, estimation
		const found = status.mentions.find((x) => x.id === status.in_reply_to_account_id);
		if (found) {
			replying_to.push(found);
		}
	}

	return replying_to;
}

function trimByReplyingAccounts(flat: Array<ParsedNode>, replying_to: Array<Entity.Mention>) {
	if (!replying_to.length) {
		return;
	}

	replying_to.sort((a, b) => {
		if (flat.find((node) => a.url === node.attributes?.href)) {
			return 1;
		}

		if (flat.find((node) => b.url === node.attributes?.href)) {
			return -1;
		}

		return 0;
	});

	let trim_index: number = 0;
	for (let index = 0; index < flat.length; index++) {
		const node = flat[index];
		if (node.tag !== 'a') {
			break;
		}

		if (node.tag === 'a' && node.attributes?.class?.includes('mention')) {
			const href = node.attributes?.href;
			const mention_index = replying_to.findIndex((mention) => href === mention.url);
			if (mention_index === -1) {
				break;
			}

			trim_index++;

			const next_node = flat[index + 1];
			if (next_node && next_node.text?.length) {
				if (
					next_node.text?.length === 1 &&
					next_node.text[0].is_raw &&
					next_node.text[0].content === ' '
				) {
					trim_index++;
					index++;
				} else if (next_node.text[0].is_raw && next_node.text[0].content === ' ') {
					next_node.text.splice(0, 1);
					break;
				}
			}
		}
	}

	flat.splice(0, trim_index);
}

export function trimMentions(
	flat: Array<ParsedNode>,
	account: TootDeckAccount,
	status: Entity.Status
) {
	if (!status.in_reply_to_account_id) {
		return;
	}

	const replying_to = getReplyingAccounts(account, status);

	// Trim mentions based on accounts replied
	if (replying_to.length) {
		trimByReplyingAccounts(flat, replying_to);
	}

	// Remove spaces after mentions
	const first = flat[0]?.text?.[0];
	if (first?.is_raw) {
		first.content = (first.content as string).trimStart();
	}
}
