import type { Entity } from '../../types/mastodonEntities';

type EmojiMatch = {
	index_in_text: number;
	index_in_emoji_set: number;
};

function addColons(shortcode: string) {
	return ':' + shortcode + ':';
}

function matchEmoji(
	text: string,
	emoji_data: Entity.Emoji,
	index_in_emoji_set: number
): Array<EmojiMatch> {
	const short_code = addColons(emoji_data.shortcode);
	const matches = text.matchAll(new RegExp(short_code, 'g'));

	let ret: Array<EmojiMatch> = [];

	for (const match of matches) {
		ret.push({
			index_in_text: match.index!,
			index_in_emoji_set,
		});
	}

	return ret;
}

function getEmojiMatches(text: string, emoji_data_set: Array<Entity.Emoji>): Array<EmojiMatch> {
	// Let's imagine this input text:
	// 'This is a string. :A: :A: :B: :A:'
	// And this emoji data set:
	// [
	//      { shortCode: 'A', url: 'example.com/A' },
	//      { shortCode: 'B', url: 'example.com/B' },
	// ]
	//
	// This function would return:
	// [
	//      { index_in_text: 19, index_in_emoji_set: 0 },
	//      { index_in_text: 23, index_in_emoji_set: 0 },
	//      { index_in_text: 27, index_in_emoji_set: 1 },
	//      { index_in_text: 31, index_in_emoji_set: 0 },
	// ]

	let ret: Array<EmojiMatch> = [];

	for (const [i, emoji_data] of emoji_data_set.entries()) {
		ret = ret.concat(matchEmoji(text, emoji_data, i));
	}

	ret.sort((a, b) => a.index_in_text - b.index_in_text);

	return ret;
}

export type TextChunk = {
	is_raw: boolean; // 'true' if the chunk is raw text. 'false' if it's an emoji
	content: string | [string, string]; // raw text is 'is_raw' is true. emoji url/static_url otherwise
	short_code?: string;
};

export function isolateEmojis(text: string, emoji_data_set: Array<Entity.Emoji>): Array<TextChunk> {
	const matches = getEmojiMatches(text, emoji_data_set);

	if (matches.length === 0) {
		return [{ is_raw: true, content: text }];
	}

	let ret: Array<TextChunk> = [];

	let index = 0;
	let match_iterator = matches.values();

	let match_iterator_result = match_iterator.next();
	while (index < text.length) {
		const match: EmojiMatch | undefined = match_iterator_result.value;
		const is_raw = match_iterator_result.done || index !== match!.index_in_text;

		if (is_raw) {
			const content = text.substring(index, match?.index_in_text);
			ret.push({ is_raw, content });

			if (!match?.index_in_text) {
				break;
			}

			index = match.index_in_text;
			continue;
		}

		const emoji_data = emoji_data_set[match!.index_in_emoji_set];

		ret.push({
			is_raw,
			content: [emoji_data.url, emoji_data.static_url],
			short_code: addColons(emoji_data.shortcode),
		});

		// The short code stored here doesn't have the colons at the start and end.
		// So we add 2.
		index += emoji_data.shortcode.length + 2;
		match_iterator_result = match_iterator.next();
	}

	return ret;
}
