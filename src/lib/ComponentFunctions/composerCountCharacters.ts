import { isURL } from '../Utils/url/isURL';

export function composerCountCharacters(
	limit: number,
	link_size: number,
	text: string,
	cw: string
) {
	if (!text) {
		if (cw) {
			return limit - cw.length;
		}

		return limit;
	}

	let value = text.trim();
	let links_char_size: number = 0;
	let at_char_size: number = 0;

	const array = value.split(' ');
	// Remove at and calculating size
	const filtered_out_at = array.filter((v) => {
		const compare = v.charAt(0) === '@' && v.charAt(1);
		if (compare) {
			// Mastodon
			// const split = v.split('@');
			// const at_count = split.length === 2 ? 1 : 2;
			// const char_count = split[1].length;
			// at_char_size += char_count + at_count;

			// Pleroma
			at_char_size += v.length;
		}

		return !compare;
	});
	// Remove links and calculating size
	const filtered_out_link = filtered_out_at.filter((v) => {
		const compare = isURL(v, false);
		if (compare) {
			links_char_size += link_size;
		}
		return !compare;
	});

	value = filtered_out_link.join(' ');

	let remain_char = limit - (cw?.length || 0) - value.length - links_char_size - at_char_size;
	if (remain_char > limit) {
		remain_char = limit;
	}
	return remain_char;
}
