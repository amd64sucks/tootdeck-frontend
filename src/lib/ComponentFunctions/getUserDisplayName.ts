import type { ParsedAccount } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

export function getUserDisplayName(account: ParsedAccount | Entity.Account) {
	if (
		!account.display_name ||
		(typeof account.display_name === 'object' && !account.display_name.length)
	) {
		return account.username;
	}

	return account.display_name || account.username;
}
