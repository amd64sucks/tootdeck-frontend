import type { TootDeckAccount } from '../../stores/Account/Accounts';

import type { ReplyAccount } from './getReplyingAccounts';
import { getHandleFromURL } from '../Utils/handle/getHandleFromURL';

import type { ParsedStatus } from '../../types/contentParsed';
import type { InstanceHandle } from '../../types/handle';

function formatHandle(account: TootDeckAccount, handle: InstanceHandle) {
	if (account.mirrorAPI.domain === handle.domain) {
		return '@' + handle.username;
	}

	return '@' + handle.username + '@' + handle.domain;
}

export function getTaggedAccounts(
	account: TootDeckAccount,
	status: ParsedStatus | undefined,
	replying_to: Array<ReplyAccount>
) {
	if (!status) {
		return '';
	}

	const tagged: Array<string> = [];

	for (const mention of replying_to) {
		const handle = getHandleFromURL(mention.url);
		const tag = formatHandle(account, handle);
		if (!tagged.includes(tag) && mention.url !== account.entity.url && mention.enable_reply) {
			tagged.push(tag);
		}
	}

	return tagged.join(' ') + ' ';
}
