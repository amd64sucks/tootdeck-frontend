import { get } from 'svelte/store';

import { type TootDeckAccount } from '../../../stores/Account/Accounts';
import { Layout, stLayout } from '../../../stores/Columns/Layout';

import { columnFactory } from '../../Columns/ColumnFactory';
import type { ThreadColumn } from '../../Columns/ThreadColumn';
import { TootDeckColumnIdentifier } from '../../Columns/TootDeckColumnTypes';
import { EventsManager } from '../../Events/EventsManager';
import { TootDeckEvents } from '../../Events/TootDeckEvents';
import { getRebloggedStatus } from '../getRebloggedStatus';
import { getThreadContext } from './generateThreadContent';

import type { ParsedStatus } from '../../../types/contentParsed';

async function appendThreadToColumn(column: ThreadColumn, p: ParsedStatus): Promise<boolean> {
	const status = getRebloggedStatus(p);

	const thread = await getThreadContext(column.account, status, column);
	if (!thread) {
		return false;
	}

	// Get up to date status version
	EventsManager.sendEvent(TootDeckEvents.Status, {
		account: column.account,
		data: [
			await column.account.mirrorAPI.Statuses.get(status.id).then((status) => {
				if (status) {
					return { ...status, animateIn: false, animateOut: true };
				}
			}),
		],
	});

	return true;
}

function generateThreadColumn(account: TootDeckAccount, status_id: string): ThreadColumn {
	return columnFactory.create(TootDeckColumnIdentifier.Thread, {
		account: {
			username: account.mirrorAPI.handle.username,
			domain: account.mirrorAPI.handle.domain,
		},
		options: {
			collapsed: false,
			sound_uuid: null,
			override_title: {
				toggle: false,
				value: null,
			},
		},
		status_id,
	});
}

export async function createAndInsertThreadColumn(
	account: TootDeckAccount,
	status: ParsedStatus,
	index: number
) {
	// Check for duplicate
	for (const instance of get(stLayout)) {
		if (
			instance.identifier === TootDeckColumnIdentifier.Thread &&
			(instance as ThreadColumn).bindToStatusID === status.id
		) {
			return;
		}
	}

	const column = generateThreadColumn(account, status.id);

	column.loading = true;
	column.append(
		{
			lines: {
				up: false,
				down: false,
			},
			status,
		},
		true
	);

	Layout.add(column, index);

	const thread = await appendThreadToColumn(column, status);
	if (!thread) {
		Layout.delete(column.id);
	}

	column.loading = false;
}

export function createThreadColumn(account: TootDeckAccount, status: ParsedStatus): ThreadColumn {
	const column = generateThreadColumn(account, status.id);

	setTimeout(async () => {
		column.loading = true;
		column.append(
			{
				lines: {
					up: false,
					down: false,
				},
				status,
			},
			true
		);

		await appendThreadToColumn(column, status);

		column.loading = false;
	});

	return column;
}
