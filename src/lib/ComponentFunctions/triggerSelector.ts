function searchDelimiter(find_in: string, delimiter: string, threshold: number) {
	if (find_in.includes(' ') || find_in.includes('\n')) {
		return false;
	}

	// delimiter
	const has_delimiter = find_in.charAt(0) === delimiter;
	// Searched string
	const value = find_in.replace(delimiter, '');

	return !!value && value.length >= threshold && !!has_delimiter;
}

export function delimitChunk(textarea: HTMLTextAreaElement) {
	const cursor = textarea.selectionStart;

	/**
	 * jigrdidjgigdigrj @Nellen kfdjfkfjsdjlf
	 *                        /\
	 *                      cursor
	 *
	 * nelleN@ jrgidgigjdidrgij
	 *       /\
	 *     start
	 *
	 * jigrdidjgigdigrj @Nellen kfdjfkfjsdjlf
	 *                /\
	 *              start
	 *
	 * jigrdidjgigdigrj @Nellen kfdjfkfjsdjlf
	 *                        /\
	 *                       end
	 */

	const found_space_at = textarea.value
		.substring(0, cursor)
		.split('')
		.reverse()
		.findIndex((c) => c === ' ' || c === '\n');

	let start = Math.max(0, cursor - found_space_at);
	if (found_space_at === -1) {
		start = 0;
	}

	const end = cursor;

	return { start, end };
}

export function triggerSelector(
	textarea: HTMLTextAreaElement,
	delimiter: string,
	threshold: number,
	cb: (r: boolean, value: string) => void
) {
	if (!textarea || !textarea.value || textarea.selectionStart !== textarea.selectionEnd) {
		cb(false, '');
		return;
	}

	const { start, end } = delimitChunk(textarea);

	const find_in = textarea.value.substring(start, end);

	if (searchDelimiter(find_in, delimiter, threshold)) {
		cb(true, find_in.replace(delimiter, ''));
		return;
	}

	// const split = textarea.value.split(/\ |\n/gm);

	// for (const str of split) {
	// 	if (searchDelimiter(str, delimiter, threshold)) {
	// 		cb(true, str.replace(delimiter, ''));
	// 		return;
	// 	}
	// }

	cb(false, '');
}
