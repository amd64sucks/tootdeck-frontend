import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { accountParser, statusParser } from '../ContentParser/ContentParser';
import { randomInt } from '../Utils/js/randomInt';

import type { ParsedStatus } from '../../types/contentParsed';
import { Entity } from '../../types/mastodonEntities';
import { Twitter } from '../../types/twitter';

function convertMediaType(type: Twitter.MediaType): Entity.AttachmentType {
	switch (type) {
		case Twitter.MediaType.Image:
			return Entity.AttachmentType.Image;
		case Twitter.MediaType.Gif:
			return Entity.AttachmentType.Gifv;
		case Twitter.MediaType.Video:
			return Entity.AttachmentType.Video;
	}
}

export function convertTwitterEmbed(account: TootDeckAccount, res: Twitter.Response): ParsedStatus {
	const status_account: Entity.Account = {
		acct: res.user_screen_name + '@twitter.com',
		avatar: res.user_profile_image_url,
		avatar_static: '',
		bot: false,
		created_at: new Date().toString(),
		discoverable: false,
		display_name: res.user_name,
		emojis: [],
		fields: [],
		followers_count: 0,
		following_count: 0,
		header: '',
		header_static: '',
		group: null,
		id: randomInt(1000000000000000, 8999999999999999).toString(),
		limited: true,
		locked: false,
		moved: null,
		mute_expires_at: undefined,
		noindex: true,
		note: '',
		role: undefined,
		source: undefined,
		statuses_count: 0,
		suspended: false,
		url: 'https://twitter.com/' + res.user_screen_name,
		username: res.user_name,
	};

	const status: Entity.Status = {
		account: status_account,
		animateIn: false,
		animateOut: false,
		application: {
			name: 'Twitter',
			website: 'https://twitter.com',
			vapid_key: null,
		},
		bookmarked: false,
		card: null,
		content: res.text,
		created_at: res.date,
		edited_at: null,
		emoji_reactions: [],
		emojis: [],
		favourited: false,
		favourites_count: res.likes,
		id: res.tweetID,
		in_reply_to_account_id: null,
		in_reply_to_id: null,
		language: null,
		media_attachments: res.media_extended.map((m, id) => ({
			blurhash: null,
			description: m.altText,
			id: id.toString(),
			meta: {
				width: m.size.width,
				height: m.size.height,
			},
			preview_url: m.thumbnail_url,
			remote_url: m.url,
			text_url: null,
			url: m.url,
			type: convertMediaType(m.type),
		})),
		mentions: [],
		muted: false,
		pinned: false,
		plain_content: res.text,
		poll: null,
		quote: false,
		reblog: null,
		reblogged: false,
		reblogs_count: res.retweets,
		replies_count: res.replies,
		sensitive: res.possibly_sensitive,
		spoiler_text: '',
		tags: res.hashtags.map((h) => ({
			name: h,
			url: 'https://twitter.com/hashtag/' + h,
		})),
		uri: res.tweetURL,
		url: res.tweetURL,
		visibility: Entity.Visibility.Public,
	};

	const parsed_account = accountParser(account, status_account);
	const parsed = statusParser(account, status);

	parsed.account = parsed_account;
	parsed.tootdeck.is_twitter_embed = true;

	return parsed;
}
