import type { TootDeckAccount } from '../../stores/Account/Accounts';

import { Entity } from '../../types/mastodonEntities';

export function getVisibility(account: TootDeckAccount) {
	return (account.entity.source?.privacy as Entity.Visibility) ?? Entity.Visibility.Public;
}
