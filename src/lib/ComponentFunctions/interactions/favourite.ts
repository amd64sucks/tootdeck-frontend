import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { TootDeckEvents } from '../../Events/TootDeckEvents';
import { EventsManager } from '../../Events/EventsManager';
import { errorModal } from '../../errorModal';
import { RequestsManager } from '../../Managers/RequestsManager';

import type { ParsedStatus } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export async function favourite(account: TootDeckAccount, status: ParsedStatus | Entity.Status) {
	return await RequestsManager.await(
		`Favourite:${account.mirrorAPI.handle_string}:${status.id}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.favourite(status.id);
			if (!response) {
				errorModal('Failed to favourite status.', undefined, true);
				return false;
			}

			// if (status.favourites_count === response.favourites_count) {
			// 	response.favourites_count++;
			// }

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return true;
		}
	);
}

export async function unfavourite(account: TootDeckAccount, status: ParsedStatus | Entity.Status) {
	return await RequestsManager.await(
		`Unfavourite:${account.mirrorAPI.handle_string}:${status.id}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.unfavourite(status.id);
			if (!response) {
				errorModal('Failed to undo favourite.', undefined, true);
				return false;
			}

			// if (status.favourites_count === response.favourites_count) {
			// 	response.favourites_count--;
			// }

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return true;
		}
	);
}
