import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { TootDeckEvents } from '../../Events/TootDeckEvents';
import { EventsManager } from '../../Events/EventsManager';
import { errorModal } from '../../errorModal';
import { RequestsManager } from '../../Managers/RequestsManager';

import type { ParsedStatus } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export async function bookmark(account: TootDeckAccount, status: ParsedStatus | Entity.Status) {
	return await RequestsManager.await(
		`Bookmark:${account.mirrorAPI.handle_string}:${status.id}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.bookmark(status.id);
			if (!response) {
				errorModal('Failed to bookmark status.', undefined, true);
				return false;
			}

			response.bookmarked = true;
			status.bookmarked = response.bookmarked;

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return true;
		}
	);
}

export async function unbookmark(account: TootDeckAccount, status: ParsedStatus | Entity.Status) {
	return await RequestsManager.await(
		`Unbookmark:${account.mirrorAPI.handle_string}:${status.id}`,
		async () => {
			const response = await account.mirrorAPI.Statuses.unbookmark(status.id);
			if (!response) {
				errorModal('Failed to undo bookmark.', undefined, true);
				return false;
			}

			response.bookmarked = false;
			status.bookmarked = response.bookmarked;

			EventsManager.sendEvent(TootDeckEvents.Status, {
				account,
				data: [response],
			});

			return true;
		}
	);
}
