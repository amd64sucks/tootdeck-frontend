import { column_width_class, ColumnWidthName } from './columnSize';

export function getCSSVariables(
	background_image: string,
	background_opacity: number,
	accent_scrollbar: boolean,
	custom_css: string,
	custom_column_size: string,
	speed_factor: number
) {
	background_opacity !== 1
		? document.body.classList.add('transparency')
		: document.body.classList.remove('transparency');

	accent_scrollbar
		? document.body.classList.add('accent-scrollbar')
		: document.body.classList.remove('accent-scrollbar');

	const column_size = document.body.classList.contains(column_width_class[ColumnWidthName.Custom])
		? `--column-width: ${custom_column_size};`
		: '';

	return `
	:root {
		--background-image:url("${background_image}");
		--background-opacity:${background_opacity};
		${column_size}
		--transition-speed: ${speed_factor};
	}
	${custom_css}
	`;
}
