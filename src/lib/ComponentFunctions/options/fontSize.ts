import { stOptions } from '../../../stores/Options';

import { bodyChangeClass } from '../../Utils/bodyChangeClass';

export enum FontSizeName {
	Smallest = 'Smallest',
	Small = 'Small',
	Medium = 'Medium',
	Large = 'Large',
	Largest = 'Largest',
}

export enum FontSize {
	Smallest = '12px',
	Small = '13px',
	Medium = '14px',
	Large = '15px',
	Largest = '16px',
}

export const font_sizes_class: Record<FontSizeName, string> = {
	[FontSizeName.Smallest]: 'font-size-smallest',
	[FontSizeName.Small]: 'font-size-small',
	[FontSizeName.Medium]: 'font-size-medium',
	[FontSizeName.Large]: 'font-size-large',
	[FontSizeName.Largest]: 'font-size-largest',
};

function updateStore(value: string) {
	stOptions.update((v) => {
		v.appearance.fontSize = value;
		return v;
	});
}

export function setfontSize(selected: string) {
	const class_names = Object.values(font_sizes_class);

	switch (selected) {
		case FontSizeName.Smallest:
		case FontSize.Smallest:
			bodyChangeClass(class_names, font_sizes_class[FontSizeName.Smallest]);
			updateStore(FontSize.Smallest);
			break;
		case FontSizeName.Small:
		case FontSize.Small:
			bodyChangeClass(class_names, font_sizes_class[FontSizeName.Small]);
			updateStore(FontSize.Small);
			break;
		case FontSizeName.Medium:
		case FontSize.Medium:
			bodyChangeClass(class_names, font_sizes_class[FontSizeName.Medium]);
			updateStore(FontSize.Medium);
			break;
		case FontSizeName.Large:
		case FontSize.Large:
			bodyChangeClass(class_names, font_sizes_class[FontSizeName.Large]);
			updateStore(FontSize.Large);
			break;
		case FontSizeName.Largest:
		case FontSize.Largest:
			bodyChangeClass(class_names, font_sizes_class[FontSizeName.Largest]);
			updateStore(FontSize.Largest);
			break;
	}
}
