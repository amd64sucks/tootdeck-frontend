import { stOptions } from '../../../stores/Options';

import { bodyChangeClass } from '../../Utils/bodyChangeClass';

export enum ThemeName {
	Mastodon = 'Mastodon',
	Dark = 'Dark',
	Light = 'Light',
}

export enum ThemeAvailable {
	Mastodon = 'theme-mastodon',
	Dark = 'theme-dark',
	Light = 'theme-light',
}

let user_selected: boolean = false;

function killTransition(theme: ThemeAvailable): ThemeAvailable {
	// Remove all transitions
	document.body.classList.add('disable-all-transition');

	bodyChangeClass(Object.values(ThemeAvailable), theme);

	// Restore all transitions
	setTimeout(() => document.body.classList.remove('disable-all-transition'), 1000);

	return theme;
}

export function matchDarkOrLight(matches?: boolean) {
	if (matches === undefined) {
		matches = window.matchMedia('(prefers-color-scheme: dark)').matches;
	}

	if (matches) {
		return ThemeAvailable.Dark;
	}

	return ThemeAvailable.Light;
}

const listener = ({ matches }: MediaQueryListEvent | MediaQueryList) => {
	if (user_selected) {
		return;
	}

	return killTransition(matchDarkOrLight(matches));
};

export function changeTheme(theme?: ThemeAvailable) {
	if (!theme) {
		window.matchMedia('(prefers-color-scheme: dark)').onchange = listener;
		const match = window.matchMedia('(prefers-color-scheme: dark)');
		stOptions.update((v) => {
			v.appearance.theme = listener(match);
			return v;
		});
		return;
	}

	user_selected = true;

	// Remove listener
	// Doesn't work
	window.matchMedia('(prefers-color-scheme: dark)').onchange = () => {};

	killTransition(theme);
}

export function init() {
	changeTheme();
}
