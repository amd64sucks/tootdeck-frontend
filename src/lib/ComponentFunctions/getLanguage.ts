import type { TootDeckAccount } from '../../stores/Account/Accounts';

export function getLanguage(account: TootDeckAccount) {
	return account.entity.source?.language ?? navigator.language.split('-')[0];
}
