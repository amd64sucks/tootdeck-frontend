import { get, type Writable } from 'svelte/store';

import { stLayout } from '../../../stores/Columns/Layout';

export const getColumnInstance = (content: Writable<Map<String, any>>) =>
	get(stLayout).find((c) => Object.is(c.props.content, content))!;
