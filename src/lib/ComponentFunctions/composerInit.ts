import { derived, get, writable, type Writable } from 'svelte/store';
import type { Readable } from 'svelte/motion';

import {
	Account,
	stAccountDefault,
	stAccounts,
	type TootDeckAccount,
} from '../../stores/Account/Accounts';
import { rdOptionsBehaviorInstance } from '../../stores/Options';

import { getLanguage } from './getLanguage';
import { getTaggedAccounts } from './getTaggedAccounts';
import type { ReplyAccount } from './getReplyingAccounts';
import { composerCountCharacters } from './composerCountCharacters';
import { getVisibility } from './getVisibility';
import { type StatusOptionalPoll } from '../APIs/MirrorAPI/types';

import type { ParsedStatus } from '../../types/contentParsed';
import type { Composer, ComposerData } from '../../types/composer';
import type { Entity } from '../../types/mastodonEntities';

function countCharacters(
	account: TootDeckAccount,
	replying_to_status: ParsedStatus | undefined,
	replying_to_accounts: Array<ReplyAccount>,
	text: string,
	content_warning: string
) {
	const characters_reserved_per_url =
		account.instance.configuration.statuses.characters_reserved_per_url ?? 0;
	const max_characters = account.instance.configuration.statuses.max_characters ?? 500;

	return composerCountCharacters(
		max_characters,
		characters_reserved_per_url,
		getTaggedAccounts(account, replying_to_status, replying_to_accounts) + text,
		content_warning
	);
}

function validatePoll(poll: StatusOptionalPoll): boolean {
	if (!poll) {
		return true;
	}

	if (poll.expires_in <= 0) {
		return false;
	}

	const duplicate_entry =
		poll.options
			.map((a, ai) => poll.options.find((b, bi) => ai !== bi && a === b))
			.filter((x) => x).length !== 0;
	if (duplicate_entry) {
		return false;
	}

	const empty = Object.values(poll.options).length !== poll.options.length;
	if (poll.options.length < 2 || empty) {
		return false;
	}

	return true;
}

export function composerInit(args?: Partial<ComposerData>): Composer {
	const raw_account = args?.account ?? Account.get(get(stAccountDefault));
	const account = writable(raw_account);

	const replying_to_status: Writable<ParsedStatus | undefined> = writable(
		args?.replying_to_status ?? undefined
	);
	const replying_to_accounts: Writable<Array<ReplyAccount>> = writable(
		args?.replying_to_accounts ?? []
	);

	const content_warning: Writable<string> = writable(args?.content_warning ?? '');
	const text: Writable<string> = writable(args?.text ?? '');

	const remaining_char: Readable<number> = derived(
		[account, replying_to_status, replying_to_accounts, text, content_warning],
		([$account, $replying_to_status, $replying_to_accounts, $text, $content_warning], set) =>
			set(
				countCharacters(
					$account,
					$replying_to_status,
					$replying_to_accounts,
					$text,
					$content_warning
				)
			)
	);

	const poll = writable({
		options: [],
		expires_in: 60 * 60 * 24,
		hide_totals: false,
		multiple: false,
	});

	const language = writable(args?.language ?? getLanguage(raw_account));
	const visibility = writable(
		(args?.visibility as Entity.Visibility) ?? getVisibility(raw_account)
	);
	const media = writable([]);

	const show_content_warning = writable(!!args?.content_warning);
	const show_poll = writable(false);

	const loading = writable(false);
	const failed_remote_fetch = writable(false);

	const lite = args?.lite ?? false;

	const invalid: Readable<boolean> = derived(
		[
			text,
			content_warning,
			poll,
			show_poll,
			remaining_char,
			media,
			failed_remote_fetch,
			account,
			rdOptionsBehaviorInstance,
		],
		(
			[
				$text,
				$content_warning,
				$poll,
				$show_poll,
				$remaining_char,
				$media,
				$failed_remote_fetch,
				$account,
				$rdOptionsBehaviorInstance,
			],
			set
		) => {
			if ($failed_remote_fetch) {
				set(true);
				return;
			}

			/**
			 * Invalid
			 * - No text and no media
			 * - poll_allow_media
			 * 			? skip this check
			 * 			: No text and valid poll
			 * - Has text and invalid poll
			 * - Too many char
			 */

			const instance_overrides = $rdOptionsBehaviorInstance[$account.mirrorAPI.domain];
			const poll_allow_media = instance_overrides.poll_allow_media;

			const poll_validation = validatePoll($poll);

			const has_text = !!$text;
			const has_media = !!$media.length;

			const no_text_no_media = !has_text && !has_media;
			const no_text_valid_poll = !has_text && $show_poll && poll_validation;
			const has_text_invalid_poll = has_text && $show_poll && !poll_validation;
			const has_too_many_char = $remaining_char < 0;

			set(
				no_text_no_media ||
					(poll_allow_media ? false : no_text_valid_poll) ||
					has_text_invalid_poll ||
					has_too_many_char
			);
		}
	);

	const clear: Readable<boolean> = derived(
		[
			account,
			text,
			show_content_warning,
			show_poll,
			media,
			replying_to_status,
			replying_to_accounts,
			language,
			visibility,
			loading,
		],
		(
			[
				$account,
				$text,
				$show_content_warning,
				$show_poll,
				$media,
				$replying_to_status,
				$replying_to_accounts,
				$language,
				$visibility,
				$loading,
			],
			set
		) => {
			if ($loading) {
				set(false);
				return;
			}

			if (lite) {
				set(!!$text.length || $show_content_warning || $show_poll || !!$media.length);
			} else {
				set(
					!!$text.length ||
						$show_content_warning ||
						$show_poll ||
						!!$media.length ||
						!!$replying_to_status ||
						!!$replying_to_accounts.length ||
						$language !== getLanguage($account) ||
						$visibility !== getVisibility($account)
				);
			}
		}
	);

	const uuid = crypto.randomUUID();

	return {
		replying_to_status,
		replying_to_accounts,
		content_warning,
		sensitive: writable(!!args?.content_warning),
		poll,
		language,
		visibility,
		text,
		media,

		// -------------------
		uuid,
		account,
		enabled_accounts: writable(
			Array.from(get(stAccounts)).map(([handle, entity]) => [handle, entity, true])
		),
		remaining_char,
		edition: writable(args?.edition ?? false),
		quote: writable(args?.quote ?? false),
		redraft: writable(args?.redraft),
		lite,
		invalid,
		publish_button_text: writable(''),
		sending: writable(false),
		sending_media: writable([]),
		loading,
		picked_emoji: writable(''),
		failed_remote_fetch,
		added_media: writable(args?.added_media ?? []),
		toggle: {
			poll: show_poll,
			content_warning: show_content_warning,
			clear,
		},

		// -------------------
		selector: {
			tag: writable(false),
			emoji: writable(false),
			visibility: writable(false),
			hashtag: writable(false),
		},
		search: {
			tag: writable(''),
			emoji: writable(''),
			hashtag: writable(''),
		},
		found: {
			tag: writable(''),
			emoji: writable(''),
			hashtag: writable(''),
		},
	};
}
