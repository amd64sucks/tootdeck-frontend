import type { ParsedStatus } from '../../types/contentParsed';
import type { Entity } from '../../types/mastodonEntities';

export interface ReplyAccount extends Entity.Mention {
	enable_reply: boolean;
}

export function getReplyingAccounts(status: ParsedStatus | Entity.Status): Array<ReplyAccount> {
	const reply = [
		...status.mentions.map((mention) => ({
			...mention,
			enable_reply: true,
		})),
	];

	if (!reply.find((x) => x.id === status.account.id)) {
		reply.push({
			id: status.account.id,
			username: status.account.username,
			acct: status.account.acct,
			url: status.account.url,
			enable_reply: true,
		});
	}

	if (reply.length && reply.length !== 1) {
		// Find and reorder array based on the replied account
		const index = reply.findIndex((x) => x.id === status.in_reply_to_account_id);
		if (index !== -1) {
			const first_account = reply.splice(index, 1)[0];
			reply.unshift(first_account);
		}
	}

	return reply;
}
