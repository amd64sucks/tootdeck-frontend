import { fetchAPI } from '../../APIs/fetchAPI';
import { getStatusID } from '../../ContentParser/anchorsParser';

export async function getPleromaStatusID(href: string): Promise<string | undefined> {
	return fetchAPI('GET', href, { hide_error: true, cache_time: 60 * 60 * 1000 })
		.then((r) => (r?.headers['redirect'] ? getStatusID(r.headers['redirect']) : undefined))
		.catch(async (e) => {
			console.error(e);
			return undefined;
		});
}
