import type { TootDeckAccount } from '../../../stores/Account/Accounts';

import { accountParser } from '../../ContentParser/ContentParser';
import { getHandleFromAccount } from '../../Utils/handle/getHandleFromAccount';

import type { ParsedAccount } from '../../../types/contentParsed';
import type { Entity } from '../../../types/mastodonEntities';

export function parseRemoteAccounts(
	account: TootDeckAccount,
	status_accounts: Array<Entity.Account>
): Array<ParsedAccount> {
	return status_accounts
		.map((a) => accountParser(account, a))
		.map((a) => {
			const handle = getHandleFromAccount(a, false);
			if (handle.domain === account.mirrorAPI.domain) {
				a.acct = handle.username;
			} else {
				a.acct = `${handle.username}@${handle.domain}`;
			}

			return a;
		})
		.sort((a, b) => {
			const handle_a = getHandleFromAccount(a, true);
			if (account.relationship.get(handle_a)?.following) {
				return -1;
			}

			const handle_b = getHandleFromAccount(b, true);
			if (account.relationship.get(handle_b)?.following) {
				return -1;
			}

			return 0;
		});
}
