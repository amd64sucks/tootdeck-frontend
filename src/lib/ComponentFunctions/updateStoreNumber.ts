export function updateStoreNumber(
	value: number,
	transform: ((v: number) => number) | null,
	updater: (v: number) => void
) {
	let v = +value;
	if (isNaN(+value)) {
		return;
	}

	if (transform) {
		v = transform(v);
	}

	updater(v);
	value = v;
}
