import type { Entity } from '../../types/mastodonEntities';

export function trimFirstMentions(mentions: Array<Entity.Mention>, text: string) {
	let skip: boolean = false;
	return text
		.split(' ')
		.filter((x) => {
			if (skip || x.charAt(0) !== '@') {
				skip = true;
				return skip;
			}

			return !mentions.find((mention) => mention.acct === x.substring(1));
		})
		.join(' ');
}
