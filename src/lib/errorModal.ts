import { NotificationModalType, notificationModal } from '../stores/NotificationsModal';

export function errorModal(message: string, details: Array<any> = [], dismissable: boolean) {
	console.error(message, ...details);
	notificationModal.create(NotificationModalType.Error, message, dismissable);
	return message;
}
