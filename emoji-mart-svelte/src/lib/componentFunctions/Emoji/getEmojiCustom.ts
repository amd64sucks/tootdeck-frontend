import type { EmojiCustom, EmojiIndexed } from '../../../types/emoji.types';

export function getEmojiCustom(emoji: EmojiIndexed) {
	const cast = emoji as any as EmojiCustom;
	return cast.skins[0].src;
}
