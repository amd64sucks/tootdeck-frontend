export function getEmojiPicture(style: string, version: string, emoji_unified: string) {
	return `https://cdn.jsdelivr.net/npm/emoji-datasource-${style}@${version}/img/${style}/64/${emoji_unified}.png`;
}
