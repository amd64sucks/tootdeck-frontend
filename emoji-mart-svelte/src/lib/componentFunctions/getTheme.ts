import type { PickerProps } from '../../types/picker.types';

export function getTheme(Props: PickerProps) {
	const match = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';

	return Props.theme === 'auto' ? match : Props.theme;
}
