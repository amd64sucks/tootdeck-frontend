export function getEmojisAtlas(style: string, version: number) {
	return `https://cdn.jsdelivr.net/npm/emoji-datasource-${style}@${version}/img/${style}/sheets-128/64.png`;
}
