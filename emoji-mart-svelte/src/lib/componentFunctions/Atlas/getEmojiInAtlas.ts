import { getEmojiPositionInAtlas } from './getEmojiPositionInAtlas';
import { getEmojisAtlas } from './getEmojisAtlas';

import type { EmojiSkin } from '../../../types/emoji.types';
import type { PickerProps } from '../../../types/picker.types';

export function getEmojiInAtlas(Props: PickerProps, skin: EmojiSkin) {
	const atlas = getEmojisAtlas(Props.style, Props.version);
	const position = getEmojiPositionInAtlas(Props.dataset, skin);

	return `background-image: url(${atlas}); ${position}`;
}
