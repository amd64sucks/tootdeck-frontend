export function getBackgroundSize(Data: any) {
	const columns = 100 * Data.sheet.cols;
	const rows = 100 * Data.sheet.rows;

	return `${columns}% ${rows}%`;
}

export function getBackgroundPosition(Data: any, skin: any) {
	// const x = (100 / (Data.sheet.cols - 1)) * skin.x;
	// const y = (100 / (Data.sheet.rows - 1)) * skin.y;
	const x = (100 / Data.sheet.cols) * skin.x;
	const y = (100 / Data.sheet.rows) * skin.y;

	return `${x}% ${y}%`;
}

export function getEmojiPositionInAtlas(Data: any, skin: any) {
	const size = `background-size: ${getBackgroundSize(Data)};`;
	const position = `background-position: ${getBackgroundPosition(Data, skin)};`;

	return size + ' ' + position;
}
