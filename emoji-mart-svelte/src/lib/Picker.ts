import { derived, writable, type Writable } from 'svelte/store';
import type { Readable } from 'svelte/motion';

import PickerComponent from '../components/Picker.svelte';
import { getSkinToneIndex } from './componentFunctions/getSkinToneIndex';
import { getEmojiSkin } from './componentFunctions/Emoji/getEmojiSkin';
import { removeDuplicates } from './utils/removeDuplicates';
import { createDictionary, reCreateDictionary } from './utils/createDictionary';
import * as NativeSupport from './NativeSupport';
import * as localStorage from './localStorage';
import { updateObject } from './utils/updateObject';
import { parseNode } from './parseNode';
import { getEmojisAtlas } from './componentFunctions/Atlas/getEmojisAtlas';
import { getBackgroundSize } from './componentFunctions/Atlas/getEmojiPositionInAtlas';

import { DefaultFrequent } from '../config/defaultFrequent';
import { safeFlags } from '../config/safeFlags';
import { getLanguages, i18n } from '../config/i18n';

import type { Data, DataCategoryCustom, DataIndexed } from '../types/data.types';
import type { EmojiData, EmojiIndexed, EmojiIndexedSkin, EmojiSkin } from '../types/emoji.types';
import {
	SkinToneDic,
	type PickerProps,
	type PickerSettings,
	theme,
	emojiStyles,
	// versionsSupported,
	topRow,
	type Theme,
	type SkinTone,
	type EmojiStyle,
	type Dictionary,
	type PickerCallbacks,
	type i18n as AvaliableLang,
	type TopRow,
	type RGB,
	// type VersionSupported,
} from '../types/picker.types';

const VERSION = 15;

export class PickerInstance {
	private readonly picker_component: typeof PickerComponent = PickerComponent;
	private readonly stUpdate: Writable<number> = writable(0);
	private readonly rdUpdate: Readable<number> = derived(this.stUpdate, ($stUpdate) => $stUpdate);
	// @ts-expect-error
	// Defined if PickerSettings.dataset is
	// or when the promise in fetchDataset is resolved
	private dataset: DataIndexed;

	// @ts-expect-error Defined in setSettings()
	private settings: PickerSettings;
	private props: PickerProps;
	private callbacks: PickerCallbacks;

	private setSettings(settings: PickerSettings, build_indexes: boolean) {
		this.checkParams(settings);
		this.settings = structuredClone(settings);

		if (build_indexes) {
			if (!this.settings.dataset) {
				this.fetchDataset();
			} else {
				this.dataset = this.generateIndexes(this.settings.dataset as any);
			}
		}

		localStorage.save(
			this.formatKey(this.settings, 'skin'),
			SkinToneDic[this.settings.skinTone!]
		);
		localStorage.save(this.formatKey(this.settings, 'style'), this.settings.style);
	}

	constructor(settings: PickerSettings, callbacks: PickerCallbacks) {
		this.setSettings(settings, true);

		this.callbacks = {
			scrollTo:
				callbacks.scrollTo ??
				((node: Element, position: number, duration: number) => {
					node.scrollTo({
						behavior: 'auto',
						top: position,
					});
				}),
			onEmojiClick: callbacks.onEmojiClick,
			onSkinToneChange: callbacks.onSkinToneChange,
			onStyleChange: callbacks.onStyleChange,
			onThemeChange: callbacks.onThemeChange,
		};

		this.props = this.getProps();
	}

	/**
	 * Init
	 */
	// #region

	/**
	 * Utility method to transform hexadecimal color to `RGB` for accent color
	 */
	hexToRGB(hex: string): RGB {
		const normal = hex.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i);
		if (normal) {
			const [r, g, b] = normal.slice(1).map((e) => parseInt(e, 16));
			return `${r}, ${g}, ${b}`;
		}

		const shorthand = hex.match(/^#([0-9a-f])([0-9a-f])([0-9a-f])$/i);
		if (shorthand) {
			const [r, g, b] = shorthand.slice(1).map((e) => 0x11 * parseInt(e, 16));
			return `${r}, ${g}, ${b}`;
		}

		throw new Error(`${hex} is not a valid hexadecimal color`);
	}

	private checkTheme(settings: PickerSettings) {
		if (settings.theme === undefined) {
			settings.theme = 'auto';
		} else if (!theme.includes(settings.theme)) {
			throw new Error(
				`Invalid theme provided: ${settings.theme}, supported values are: ${theme.join(
					', '
				)}`
			);
		}
	}

	private checkStyle(settings: PickerSettings) {
		if (settings.styleSelector) {
			const local = localStorage.load<EmojiStyle>(this.formatKey(settings, 'style'));
			if (local && emojiStyles.includes(local)) {
				settings.style = local;
				return;
			}
		}

		if (settings.style === undefined) {
			settings.style = 'native';
		} else if (!emojiStyles.includes(settings.style)) {
			throw new Error(
				`Invalid style provided: ${
					settings.style
				}, supported emoji styles are: ${emojiStyles.join(', ')}`
			);
		}
	}

	// private checkVersion(settings: PickerSettings) {
	// 	if (settings.version === undefined) {
	// 		settings.version = 15;
	// 	} else if (!versionsSupported.includes(settings.version)) {
	// 		throw new Error(
	// 			`Invalid version provided: ${
	// 				settings.version
	// 			}, supported emoji versions are: ${versionsSupported.join(', ')}`
	// 		);
	// 	}
	// }

	private checkSkinTone(settings: PickerSettings) {
		const load = localStorage.load<number>(this.formatKey(settings, 'skin'));
		const found = load ? getSkinToneIndex(+load) : undefined;
		if (found && SkinToneDic[found] !== undefined) {
			settings.skinTone = found;
		}

		if (settings.skinTone === undefined) {
			settings.skinTone = 'Default';
		} else if (SkinToneDic[settings.skinTone] === undefined) {
			throw new Error(
				`Invalid skin tone provided: ${
					settings.skinTone
				}, supported emoji skin tones are: ${Object.keys(SkinToneDic).join(', ')}`
			);
		}
	}

	private checkTopRow(settings: PickerSettings) {
		if (settings.topRow === undefined) {
			settings.topRow = 'recently';
		} else if (!topRow.includes(settings.topRow)) {
			throw new Error(
				`Invalid parameter for the top row provided: ${
					settings.topRow
				}, supported values are: ${topRow.join(', ')}`
			);
		}
	}

	private checkLanguage(settings: PickerSettings) {
		const languages = getLanguages();

		if (settings.language === undefined) {
			settings.language = 'en';
		} else if (!languages.includes(settings.language)) {
			throw new Error(
				`Invalid language provided: ${
					settings.language
				}, supported languages are: ${languages.join(', ')}`
			);
		}
	}

	private checkParams(settings: PickerSettings): void {
		if (settings.localStorageKeyPrefix === undefined) {
			settings.localStorageKeyPrefix = '';
		}

		if (settings.accentColor === undefined) {
			settings.accentColor = '58, 130, 247';
		}

		if (settings.maxTopRows === undefined) {
			settings.maxTopRows = 4;
		}

		if (settings.noCountryFlags === undefined) {
			settings.noCountryFlags = false;
		}

		if (settings.exceptEmojis === undefined) {
			settings.exceptEmojis = [];
		}

		if (settings.styleSelector === undefined) {
			settings.styleSelector = false;
		}

		if (settings.toggleWideEmoji === undefined) {
			settings.toggleWideEmoji = false;
		}

		this.checkTheme(settings);
		this.checkStyle(settings);
		// this.checkVersion(settings);
		this.checkSkinTone(settings);
		this.checkTopRow(settings);
		this.checkLanguage(settings);
	}

	/**
	 * From https://github.com/missive/emoji-mart/blob/d29728f7b4e295e46f9b64aa80335aa4a3c15b8e/packages/emoji-mart/src/config.ts#L122
	 */
	private parseCustomData(data: DataIndexed, custom: Array<DataCategoryCustom>) {
		for (let i in custom) {
			const index = +i;
			const category = custom[index];

			if (!category.emojis || !category.emojis.length) {
				continue;
			}

			category.id || (category.id = `custom_${index + 1}`);
			category.name || (category.name = i18n[this.props.language].categories.custom);

			data.customCategories.push({
				id: category.id,
				name: category.name,
				emojis: category.emojis.map((x) => x.id),
			});

			for (const emoji of category.emojis) {
				data.emojis[emoji.id] = { ...emoji, custom: true } as any;
			}
		}
	}

	/**
	 * From https://github.com/missive/emoji-mart/blob/d29728f7b4e295e46f9b64aa80335aa4a3c15b8e/packages/emoji-mart/src/config.ts#L122
	 */
	private readonly perLine = 9;

	private parseEmojis(
		data: DataIndexed,
		key: 'categories' | 'customCategories',
		latestVersionSupport: number | null,
		noCountryFlags: boolean
	) {
		let categoryIndex = data[key].length;
		while (categoryIndex--) {
			const category = data[key][categoryIndex];

			if (category.id == 'frequent') {
				category.emojis = DefaultFrequent.slice(
					0,
					this.perLine * this.settings.maxTopRows!
				);
			}

			if (!category.emojis || !category.emojis.length) {
				data[key].splice(categoryIndex, 1);
				continue;
			}

			let emojiIndex = category.emojis.length;
			while (emojiIndex--) {
				const emojiId = category.emojis[emojiIndex];
				const emoji = data.emojis[emojiId] as EmojiIndexed;

				const ignore = () => {
					category.emojis.splice(emojiIndex, 1);
				};

				if (
					!emoji ||
					(this.settings.exceptEmojis && this.settings.exceptEmojis.includes(emoji.id))
				) {
					ignore();
					continue;
				}

				if (latestVersionSupport && emoji.version > latestVersionSupport) {
					ignore();
					continue;
				}

				if (noCountryFlags && category.id == 'flags') {
					if (!safeFlags.includes(emoji.id)) {
						ignore();
						continue;
					}
				}

				if (!emoji.search) {
					emoji.search =
						',' +
						[
							[emoji.id, false],
							[emoji.name, true],
							[emoji.keywords, false],
							[emoji.emoticons, false],
						]
							.map(([strings, split]) => {
								if (!strings) {
									return;
								}

								const array = Array.isArray(strings)
									? strings
									: [strings as string];
								return array
									.map((string) =>
										(split ? string.split(/[-|_|\s]+/) : [string]).map((s) =>
											s.toLowerCase()
										)
									)
									.flat();
							})
							.flat()
							.filter((a) => a && a.trim())
							.join(',');

					if (emoji.emoticons) {
						for (const emoticon of emoji.emoticons) {
							if (data.emoticons[emoticon]) {
								continue;
							}
							data.emoticons[emoticon] = emoji.id;
						}
					}

					let skinIndex = 0;
					for (const skin of emoji.skins) {
						if (!skin) {
							continue;
						}
						skinIndex++;

						const { native } = skin as EmojiIndexedSkin;
						if (native) {
							data.natives[native] = emoji.id;
							emoji.search += `,${native}`;
						}

						const skinShortcodes = skinIndex == 1 ? '' : `:skin-tone-${skinIndex}:`;
						skin.shortcodes = `:${emoji.id}:${skinShortcodes}`;
					}
				}
			}
		}
	}

	/**
	 * From https://github.com/missive/emoji-mart/blob/d29728f7b4e295e46f9b64aa80335aa4a3c15b8e/packages/emoji-mart/src/config.ts#L122
	 */
	private generateIndexes(data: DataIndexed) {
		data = structuredClone(data);

		data.customCategories = [];

		if (this.settings.customDataset) {
			this.parseCustomData(data, this.settings.customDataset);
		}

		data.emoticons = {};
		data.natives = {};

		let latestVersionSupport: number | null = null;
		let noCountryFlags: boolean = false;
		if (this.settings.style == 'native') {
			latestVersionSupport = NativeSupport.latestVersion();
			noCountryFlags = this.settings.noCountryFlags! || NativeSupport.noCountryFlags();
		}

		this.parseEmojis(data, 'categories', latestVersionSupport, noCountryFlags);
		if (this.settings.customDataset) {
			this.parseEmojis(data, 'customCategories', latestVersionSupport, noCountryFlags);
		}

		return data;
	}

	private dataset_promise: Promise<void> | null = null;

	private async fetchDataset(): Promise<void> {
		// const url = `https://cdn.jsdelivr.net/npm/@emoji-mart/data@latest/sets/${VERSION}/${this.settings.style}.json`;
		const url = `emoji-dataset/${this.settings.style}.json`;
		this.dataset_promise = fetch(url)
			.then(async (r) => {
				const json = (await r.json()) as any;
				this.dataset = this.generateIndexes(json);
				this.updateProps();
			})
			.catch((e) => {
				console.error(e);
			});
	}

	// #endregion

	private formatKey(
		settings: PickerSettings,
		key: 'frequently' | 'recently' | 'last' | 'skin' | 'style'
	) {
		const prefix = settings.localStorageKeyPrefix ? '_' + settings.localStorageKeyPrefix : '';
		return `emoji-mart${prefix}.${key}`;
	}

	private updateProps() {
		updateObject(this.props, this.getProps());
		this.stUpdate.update((v) => ++v);
	}

	getProps(): PickerProps {
		console.log(this.settings.localStorageKeyPrefix);
		return {
			accentColor: this.settings.accentColor!,
			dataset: this.dataset,
			frequently: this.loadFrequently(),
			instance: this,
			i18n: i18n[this.settings.language!],
			language: this.settings.language!,
			recently: this.loadRecently(),
			update: this.rdUpdate,
			scrollTo: this.callbacks.scrollTo!,
			style: this.settings.style!,
			skinTone: SkinToneDic[this.settings.skinTone!],
			styleSelector: this.settings.styleSelector!,
			theme: this.settings.theme!,
			topRow: this.settings.topRow!,
			toggleWideEmoji: this.settings.toggleWideEmoji!,
			version: VERSION,
		};
	}

	getPickerNProps(): {
		Picker: typeof PickerComponent;
		Props: PickerProps;
	} {
		return {
			Picker: this.picker_component,
			Props: this.props,
		};
	}

	getEmojiFromID(id: string): EmojiIndexed | undefined {
		return this.dataset.emojis[id];
	}

	getEmojiDataFromNative(native: string): EmojiData | undefined {
		const results = this.search(native, 1);
		if (!results || !results.length) {
			return;
		}

		const emoji = results[0];
		let skinIndex = 0;

		if (emoji.skins.length !== 1) {
			for (let skin of emoji.skins) {
				if ((skin as EmojiSkin).native === native) {
					break;
				}

				skinIndex++;
			}
		}

		const skin = getEmojiSkin(emoji, skinIndex);
		const emoji_data = {
			id: emoji.id,
			name: emoji.name,
			keywords: emoji.keywords,
			skin,
		};

		return emoji_data;
	}

	/**
	 * Set values after constructor for dynamic updates
	 * Call update() after calling of these methods
	 */
	// #region

	private updated_settings: PickerSettings = {};

	setCallback(callback: PickerCallbacks): this {
		if (callback.onEmojiClick) {
			this.callbacks.onEmojiClick = callback.onEmojiClick;
		}

		if (callback.scrollTo) {
			this.callbacks.scrollTo = callback.scrollTo;
		}

		if (callback.onSkinToneChange) {
			this.callbacks.onSkinToneChange = callback.onSkinToneChange;
		}

		if (callback.onSkinToneChange) {
			this.callbacks.onSkinToneChange = callback.onSkinToneChange;
		}

		if (callback.onThemeChange) {
			this.callbacks.onThemeChange = callback.onThemeChange;
		}

		return this;
	}

	setTheme(theme: Theme): this {
		this.updated_settings.theme = theme;
		return this;
	}

	setAccentColor(accentColor: RGB): this {
		this.updated_settings.accentColor = accentColor;
		return this;
	}

	setStyle(style: EmojiStyle): this {
		this.updated_settings.style = style;
		return this;
	}

	// setVersion(version: VersionSupported): this {
	// 	this.updated_settings.version = version;
	// 	return this;
	// }

	setSkinTone(skinTone: SkinTone): this {
		this.updated_settings.skinTone = skinTone;
		return this;
	}

	setTopRow(topRow: TopRow): this {
		this.updated_settings.topRow = topRow;
		return this;
	}

	setMaxTopRows(maxTopRows: number): this {
		this.updated_settings.maxTopRows = maxTopRows;
		return this;
	}

	setNoCountryFlags(noCountryFlags: boolean): this {
		this.updated_settings.noCountryFlags = noCountryFlags;
		return this;
	}

	setLocalStorageKeyPrefix(localStorageKeyPrefix: string): this {
		this.updated_settings.localStorageKeyPrefix = localStorageKeyPrefix;
		return this;
	}

	setExceptEmojis(exceptEmojis: Array<string>): this {
		this.updated_settings.exceptEmojis = exceptEmojis;
		return this;
	}

	setDataset(data: { dataset?: Data; custom?: Array<DataCategoryCustom> }): this {
		if (data.dataset) {
			this.updated_settings.dataset = data.dataset;
		}

		if (data.custom) {
			this.updated_settings.customDataset = data.custom;
		}

		return this;
	}

	setLanguage(language: AvaliableLang): this {
		this.updated_settings.language = language;
		return this;
	}

	setStyleSelector(styleSelector: boolean): this {
		this.updated_settings.styleSelector = styleSelector;
		return this;
	}

	setToggleWideEmoji(toggleWideEmoji: boolean): this {
		this.updated_settings.toggleWideEmoji = toggleWideEmoji;
		return this;
	}

	update() {
		if (Object.keys(this.updated_settings).length) {
			const build_indexes =
				!!this.updated_settings.dataset ||
				!!this.updated_settings.customDataset ||
				!!(
					this.updated_settings.style &&
					this.settings.style !== this.updated_settings.style
				);
			this.setSettings({ ...this.settings, ...this.updated_settings }, build_indexes);
			this.updated_settings = {};
		}

		this.updateProps();
	}

	// #endregion

	/**
	 * Callback
	 */
	// #region

	onThemeChange(theme: Theme) {
		this.checkTheme({ theme });
		this.settings.theme = theme;
		this.updateProps();

		this.callbacks.onThemeChange?.(theme);
	}

	onSkinToneChange(skinTone: SkinTone) {
		this.checkSkinTone({ skinTone });
		this.settings.skinTone = skinTone;
		this.updateProps();

		this.callbacks.onSkinToneChange?.(skinTone);

		localStorage.save(this.formatKey(this.settings, 'skin'), SkinToneDic[skinTone]);
	}

	onStyleChange(style: EmojiStyle) {
		this.checkStyle({ style });
		this.settings.style = style;
		this.updateProps();

		this.callbacks.onStyleChange?.(style);

		localStorage.save(this.formatKey(this.settings, 'style'), style);
	}

	onEmojiClick(emoji: EmojiIndexed) {
		const skin = getEmojiSkin(emoji, this.props.skinTone);

		this.callbacks.onEmojiClick?.({ ...emoji, skin });

		this.updateRecently(emoji);
		this.updateFrequently(emoji);

		localStorage.save(this.formatKey(this.settings, 'last'), emoji.id);
	}

	// #endregion

	/**
	 * Recently
	 */
	// #region

	private loadRecently(): Array<string> {
		const local = localStorage.load<Array<string>>(this.formatKey(this.settings, 'recently'));

		return local && local.length
			? local.filter((key) => !!this.dataset?.emojis[key])
			: DefaultFrequent;
	}

	updateRecently(emoji: EmojiIndexed) {
		const local = this.loadRecently();

		local.unshift(emoji.id);
		const recently = removeDuplicates(local).slice(0, this.perLine * this.settings.maxTopRows!);

		localStorage.save(this.formatKey(this.settings, 'recently'), recently);
	}

	// #endregion

	/**
	 * Frequently
	 */
	// #region

	private loadFrequently(): Dictionary<number> {
		const local = localStorage.load<Dictionary<number>>(
			this.formatKey(this.settings, 'frequently')
		);

		if (typeof local === 'object') {
			const entries = Object.entries(local);
			if (entries.length) {
				// Remove undefined emojis
				return reCreateDictionary(entries.filter(([key]) => !!this.dataset?.emojis[key]));
			}
		}

		// has no values, create a default dictionary
		return createDictionary(DefaultFrequent, 0);
	}

	updateFrequently(emoji: EmojiIndexed) {
		const frequently = this.loadFrequently();

		if (frequently[emoji.id]) {
			frequently[emoji.id]++;
		} else {
			frequently[emoji.id] = 1;
		}

		const sorted = Object.entries(frequently).sort(([Akey, Avalue], [Bkey, Bvalue]) => {
			if (Avalue < Bvalue) {
				return 1;
			}

			if (Avalue > Bvalue) {
				return -1;
			}

			return 0;
		});

		const trunct = sorted.slice(0, this.perLine * this.settings.maxTopRows!);
		const updated = reCreateDictionary(trunct);

		localStorage.save(this.formatKey(this.settings, 'frequently'), updated);
	}

	// #endregion

	/**
	 * Search
	 */
	// #region

	/**
	 * From https://github.com/missive/emoji-mart/blob/d29728f7b4e295e46f9b64aa80335aa4a3c15b8e/packages/emoji-mart/src/helpers/search-index.ts#L23
	 */
	private pool: Array<EmojiIndexed> | null = null;

	search(value: string, maxResults?: number): Array<EmojiIndexed> {
		if (!value || !value.trim().length) {
			return [];
		}
		maxResults || (maxResults = Infinity);

		const values = value
			.toLowerCase()
			.replace(/(\w)-/, '$1 ')
			.split(/[\s|,]+/)
			.filter((word, i, words) => word.trim() && words.indexOf(word) == i);

		if (!values.length) {
			return [];
		}

		let pool = this.pool || (this.pool = Object.values(this.dataset.emojis));
		let results: Array<EmojiIndexed> = [];
		let scores: Dictionary<number> = {};

		for (const value of values) {
			if (!pool.length) {
				break;
			}

			results = [];
			scores = {};

			for (const emoji of pool) {
				if (!emoji.search) {
					continue;
				}

				const score = emoji.search.indexOf(`,${value}`);
				if (score == -1) {
					continue;
				}

				results.push(emoji);
				scores[emoji.id] || (scores[emoji.id] = 0);
				scores[emoji.id] += emoji.id == value ? 0 : score + 1;
			}

			pool = results;
		}

		if (results.length < 2) {
			return results;
		}

		results.sort((a, b) => {
			const aScore = scores[a.id];
			const bScore = scores[b.id];

			if (aScore == bScore) {
				return a.id.localeCompare(b.id);
			}

			return aScore - bScore;
		});

		if (results.length > maxResults) {
			results = results.slice(0, maxResults);
		}

		return results;
	}

	// #endregion

	/**
	 * HTML Node parser
	 * Needs the dataset to be loaded
	 * So, await the promise with the method await() before using it
	 *
	 * Does nothing if the style is set to "native"
	 */
	// #region

	async await() {
		await this.dataset_promise;
	}

	parse(node: HTMLElement) {
		if (this.settings.style === 'native') {
			return;
		}

		if (!this.dataset) {
			throw new Error(
				'Needs the dataset to be loaded, await the promise with the method await() before using it'
			);
		}

		/**
		 * Atlas method
		 */
		// const atlasURL = getEmojisAtlas(this.settings.style!, VERSION);
		// const backgroundSize = getBackgroundSize(this.dataset);

		// parseNode(
		// 	node,
		// 	{
		// 		Data: this.dataset as Data,
		// 		atlasURL,
		// 		backgroundSize,
		// 	},
		// 	this.getEmojiDataFromNative.bind(this)
		// );

		parseNode(node, this.settings.style!, this.getEmojiDataFromNative.bind(this));
	}

	// #endregion
}
