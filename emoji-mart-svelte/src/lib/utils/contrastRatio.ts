/**
 * From https://gist.github.com/paulcollett/5533b0f3b9be16a068f58f9e76ad6289#file-color-contrast-ratio-js
 */

// There seems to be a few algorithms floating around for brightness/luminance detection.
// One uses NTSC `(299*R + 587*G + 114*B)` which is incorrect for web colors (sRGB) see
// `rgbLuminance` for "better" coefficients (seems subjective but agreed apon). To be more
// accurate you need to also convert RGB from sRGB color space (which gives more spectrum to lighter colors)
// to linear rgb which normalizes colors across the spectrum - better for processing.
// see https://stackoverflow.com/questions/596216/formula-to-determine-perceived-brightness-of-rgb-color

// convert sRGB to linear RGB values
// - channel ** 2.218 same as Math.pow((channel + 0.055) / 1.055, 2.4)
// - i've seen this simplified to be just `(channel / 255) ** 2.21`
function convertSRGBtoLinearRGBRatios(rgb: [number, number, number]): [number, number, number] {
	return rgb
		.map((channel) => channel / 255)
		.map((channel) => (channel <= 0.04045 ? channel / 12.92 : channel ** 2.218)) as any;
}

// algorithm which considers the human eye sensitivity to different light wavelengths.
// converts RGB to luminance (aka. Y) 0 - 1
function rgbRatiosToLuminanceRatio([rLin, gLin, bLin]: [number, number, number]): number {
	return rLin * 0.2126 + gLin * 0.7152 + bLin * 0.0722;

	// divide brighter color with darker color. division by 21 gives us a ratio
}
// According to the Web Content Accessibility Guidelines 2, a minimum contrast of 0.33 is recommended or 0.22 for < 14px non-bold text
// note: 0.05 is required by WCAG 2.0 to prevent the contrast ratio from becoming infinite
function luminanceContrastRatio(fgLuminanceRatio: number, bgLuminanceRatio: number): number {
	return (
		(Math.max(fgLuminanceRatio, bgLuminanceRatio) + 0.05) /
		(Math.min(fgLuminanceRatio, bgLuminanceRatio) + 0.05) /
		21
	);
}

// - I've seen this roughly simplified to just `Math.pow(luminanceRatio, 0.33)`
function perceivedLightnessRatioFromLuminance(luminanceRatio: number): number {
	return luminanceRatio <= 0.008856
		? luminanceRatio * 903.3
		: Math.pow(luminanceRatio, 0.333) * 116 - 16;
}

// usage:

// expects full hex like 'fff000' without hash
function hexToRGB(c: string): [number, number, number] {
	return [0, 0, 0].map((_, i) => {
		const index = i * 2;
		return parseInt(c.substring(index, index + 2), 16);
	}) as any;
}

export function perceivedLightnessRatio(hex: string) {
	const rgb = hexToRGB(hex);
	const linear = convertSRGBtoLinearRGBRatios(rgb);
	const luminance = rgbRatiosToLuminanceRatio(linear);

	return perceivedLightnessRatioFromLuminance(luminance);
}

export function contrastRatio(hexA: string, hexB: string) {
	const [rgbA, rgbB] = [hexA, hexB].map((hex) => {
		const rgb = hexToRGB(hex);
		const linear = convertSRGBtoLinearRGBRatios(rgb);

		return rgbRatiosToLuminanceRatio(linear);
	});

	return luminanceContrastRatio(rgbA, rgbB);
}
