import type { Dictionary } from '../../types/picker.types';

export function createDictionary<T>(array: Array<string>, default_value: T): Dictionary<T> {
	const dictionary: Dictionary<T> = {};

	for (const name of array) {
		dictionary[name] = default_value;
	}

	return dictionary;
}

export function reCreateDictionary<T>(entries: Array<[string, T]>): Dictionary<T> {
	const dictionary: Dictionary<T> = {};

	for (const [key, value] of entries) {
		dictionary[key] = value;
	}

	return dictionary;
}
