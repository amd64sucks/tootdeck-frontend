import type { Readable } from 'svelte/motion';

import type { PickerInstance } from '../lib/Picker';

import type { Data, DataCategoryCustom, DataIndexed } from './data.types';
import type { ClickedEmoji } from './emoji.types';
import type { i18nEntry } from '../config/i18n';

export type Dictionary<T> = { [key: string]: T };

export type RGB = `${string}, ${string}, ${string}`;

export type EmojiStyle = 'apple' | 'facebook' | 'google' | 'twitter' | 'native';
export const emojiStyles = ['apple', 'facebook', 'google', 'twitter', 'native'];

export type SkinTone = 'Default' | 'Light' | 'Medium-Light' | 'Medium' | 'Medium-Dark' | 'Dark';
export const SkinToneDic: Dictionary<number> = {
	Default: 0,
	Light: 1,
	'Medium-Light': 2,
	Medium: 3,
	'Medium-Dark': 4,
	Dark: 5,
};

export type TopRow = 'frequently' | 'recently';
export const topRow = ['frequently', 'recently'];

export type Theme = 'auto' | 'light' | 'dark';
export const theme = ['auto', 'light', 'dark'];

export type IconStyle = 'outline' | 'solid';

export type i18n =
	| 'ar'
	| 'be'
	| 'cs'
	| 'de'
	| 'en'
	| 'es'
	| 'fa'
	| 'fi'
	| 'fr'
	| 'hi'
	| 'it'
	| 'ja'
	| 'kr'
	| 'nl'
	| 'pl'
	| 'pt'
	| 'ru'
	| 'sa'
	| 'tr'
	| 'uk'
	| 'vi'
	| 'zh';

export interface PickerSettings {
	/** @default "auto" */
	theme?: Theme;
	/** @default RGB "58, 130, 247" */
	accentColor?: RGB;
	/** @default "native" */
	style?: EmojiStyle;
	/** @default "Default" */
	skinTone?: SkinTone;
	/** @default "recently" */
	topRow?: TopRow;
	/** @default 4 */
	maxTopRows?: number;
	/** @default false */
	noCountryFlags?: boolean;
	/** @default undefined */
	localStorageKeyPrefix?: string;
	/** @default [] */
	exceptEmojis?: Array<string>;
	/** @default undefined */
	dataset?: Data;
	/** @default undefined */
	customDataset?: Array<DataCategoryCustom>;
	/** @default "en" */
	language?: i18n;
	/** @default false */
	styleSelector?: boolean;
	/** @default false */
	toggleWideEmoji?: boolean;
}

export interface PickerCallbacks {
	scrollTo?: (node: Element, position: number, duration: number) => void;
	onEmojiClick?: (emoji: ClickedEmoji) => void;
	onThemeChange?: (theme: Theme) => void;
	onStyleChange?: (style: EmojiStyle) => void;
	onSkinToneChange?: (skinTone: SkinTone) => void;
}

export interface PickerProps {
	accentColor: RGB;
	dataset: DataIndexed;
	frequently: Dictionary<number>;
	instance: PickerInstance;
	i18n: i18nEntry;
	language: i18n;
	recently: Array<string>;
	update: Readable<number>;
	scrollTo: (node: Element, position: number, duration: number) => void;
	skinTone: number;
	style: EmojiStyle;
	styleSelector: boolean;
	theme: Theme;
	topRow: TopRow;
	toggleWideEmoji: boolean;
	version: number;
}
