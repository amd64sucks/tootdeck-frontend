import type { Emoji, EmojiCustom, EmojiIndexed } from './emoji.types';
import type { Dictionary } from './picker.types';

export interface DataCategory {
	id: string;
	name: string;
	emojis: Array<string>;
}

export interface DataCategoryCustom {
	id: string;
	name: string;
	emojis: Array<EmojiCustom>;
}

/** Atlas size */
export interface DataSheet {
	cols: number;
	rows: number;
}

export interface Data {
	aliases: Dictionary<string>;
	categories: Array<DataCategory>;
	emojis: Dictionary<Emoji>;
	sheet: DataSheet;
}

// @ts-expect-error
// Linter isn't happy with `EmojiIndexed`
// because it doesn't know if the property `skins`
// is `EmojiIndexedSkin` or `EmojiIndexedSkinCustom`
// it can ignored because the check can only be done at runtime
// (and it's checked at runtime)
export interface DataIndexed extends Data {
	customCategories: Array<DataCategory>;
	emojis: Dictionary<EmojiIndexed>;
	emoticons: Dictionary<string>;
	natives: Dictionary<string>;
}
